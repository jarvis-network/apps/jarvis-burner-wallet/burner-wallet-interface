import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';
import { AddressOn } from '@jarvis-network/core-utils/dist/eth/address';
import { SupportedNetworkName } from '@jarvis-network/synthereum-config/src/supported/networks';

export interface IRedeemParams<
  Net extends SupportedNetworkName = SupportedNetworkName
> {
  numTokens: FPN | undefined;
  minCollateral: FPN | undefined;
  feePercentage: FPN | undefined;
  recipient: AddressOn<Net> | undefined;
  expiration: number | undefined;
}
