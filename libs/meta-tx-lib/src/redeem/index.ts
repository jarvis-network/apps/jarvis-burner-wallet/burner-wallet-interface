import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';
import { AddressOn } from '@jarvis-network/core-utils/dist/eth/address';
import { SupportedNetworkName } from '@jarvis-network/synthereum-config/src/supported/networks';

import { RequestDTO } from '../request';

import { IRedeemParams } from './interface';

export class Redeem<Net extends SupportedNetworkName = SupportedNetworkName>
  implements IRedeemParams {
  numTokens: FPN | undefined;

  minCollateral: FPN | undefined;

  feePercentage: FPN | undefined;

  recipient: AddressOn<Net> | undefined;

  expiration = 0;

  r: string | undefined;

  s: string | undefined;

  v: number | undefined;

  static of(params: RequestDTO): Redeem {
    const redeem = new Redeem();
    redeem.expiration = params.deadline as number;
    redeem.numTokens = FPN.fromWei(params.permit_value);
    redeem.minCollateral = FPN.fromWei(params.min_collateral);
    redeem.feePercentage = FPN.fromWei(params.fee_percentage);
    redeem.recipient = params.from as any;
    redeem.r = params.r;
    redeem.s = params.s;
    redeem.v = params.v;
    Object.assign(redeem, params);

    return redeem;
  }
}
