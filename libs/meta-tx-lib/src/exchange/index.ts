import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';
import { AddressOn } from '@jarvis-network/core-utils/dist/eth/address';
import { SupportedNetworkName } from '@jarvis-network/synthereum-config/src/supported/networks';

import { RequestDTO } from '../request';

import { IExchangeParams } from './interface';

export class Exchange<Net extends SupportedNetworkName = SupportedNetworkName>
  implements IExchangeParams {
  minDestNumTokens: FPN | undefined;

  numTokens: FPN | undefined;

  minCollateral: FPN | undefined;

  feePercentage: FPN | undefined;

  recipient: FPN | undefined;

  expiration = 0;

  opType: 'REDEEM' | 'MINT' | 'EXCHANGE' = 'REDEEM';

  from: AddressOn<Net> | undefined;

  to: AddressOn<Net> | undefined;

  pool: AddressOn<Net> | undefined;

  r: string | undefined;

  s: string | undefined;

  v: number | undefined;

  static of(params: Partial<RequestDTO>): IExchangeParams {
    const exchange = new Exchange();
    exchange.expiration = new Date(Date.now() + 30 * 60000).getTime();
    Object.assign(exchange, params);
    return exchange;
  }
}
