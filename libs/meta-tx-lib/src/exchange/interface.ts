import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';

import { IBaseRequest } from '../interfaces';

export interface ExchangeRequestDTO extends IBaseRequest {
  minDestNumTokens: string;
  numTokens: string;
  minCollateral: string;
  feePercentage: string;
  recipient: string;
}

export interface IExchangeParams extends IBaseRequest {
  minDestNumTokens: FPN | undefined;
  numTokens: FPN | undefined;
  minCollateral: FPN | undefined;
  feePercentage: FPN | undefined;
  recipient: FPN | undefined;
  expiration: number | undefined;
}
