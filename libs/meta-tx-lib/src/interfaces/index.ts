import { AddressOn } from '@jarvis-network/core-utils/dist/eth/address';
import { SupportedNetworkName } from '@jarvis-network/synthereum-config/src/supported/networks';

export enum BASE_OPERATION {
  'REDEEM',
  'MINT',
  'EXCHANGE',
}
export interface IBaseRequest<
  Net extends SupportedNetworkName = SupportedNetworkName
> {
  opType: keyof typeof BASE_OPERATION;
  from: AddressOn<Net> | undefined;
  to: AddressOn<Net> | undefined;
  pool: AddressOn<Net> | undefined;
  r: string | undefined;
  s: string | undefined;
  v: number | undefined;
}
