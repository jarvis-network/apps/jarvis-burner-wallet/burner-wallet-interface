import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';

import { IBaseRequest } from '../interfaces';

export interface MintRequestDTO extends IBaseRequest {
  numTokens: string;
  minCollateral: string;
  feePercentage: string;
  recipient: string;
}

export interface IMintParams extends IBaseRequest {
  numTokens: FPN | undefined;
  minCollateral: FPN | undefined;
  feePercentage: FPN | undefined;
  recipient: FPN | undefined;
  expiration: number | undefined;
  r: string | undefined;
  s: string | undefined;
  v: number | undefined;
}
