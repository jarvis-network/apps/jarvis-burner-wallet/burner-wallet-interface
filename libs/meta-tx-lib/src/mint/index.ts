import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';
import { AddressOn } from '@jarvis-network/core-utils/dist/eth/address';
import { SupportedNetworkName } from '@jarvis-network/synthereum-config/src/supported/networks';

import { RequestDTO } from '../request';

import { IMintParams } from './interface';

export class Mint<Net extends SupportedNetworkName = SupportedNetworkName>
  implements IMintParams {
  numTokens: FPN | undefined;

  minCollateral: FPN | undefined;

  feePercentage: FPN | undefined;

  recipient: FPN | undefined;

  expiration = 0;

  opType: 'REDEEM' | 'MINT' | 'EXCHANGE' = 'REDEEM';

  from: AddressOn<Net> | undefined;

  to: AddressOn<Net> | undefined;

  pool: AddressOn<Net> | undefined;

  r: string | undefined;

  s: string | undefined;

  v: number | undefined;

  static of(params: Partial<RequestDTO>): IMintParams {
    const mint = new Mint();
    mint.expiration = new Date(Date.now() + 30 * 60000).getTime();
    Object.assign(mint, params);
    return mint;
  }
}
