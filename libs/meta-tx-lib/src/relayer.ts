/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { parseInteger } from '@jarvis-network/core-utils/dist/base/asserts';
import { maxUint256 } from '@jarvis-network/core-utils/dist/base/big-number';
import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';
import { AddressOn } from '@jarvis-network/core-utils/dist/eth/address';
import {
  scaleTokenAmountToWei,
  weiToTokenAmount,
} from '@jarvis-network/core-utils/dist/eth/contracts/erc20';
import { NonPayableTransactionObject } from '@jarvis-network/core-utils/dist/eth/contracts/typechain/types';
import { SupportedNetworkName } from '@jarvis-network/synthereum-config/src/supported/networks';
import { DefenderRelaySigner } from 'defender-relay-client/lib/ethers';
import { ethers } from 'ethers';

import { Exchange } from './exchange';

import JARVIS_TOKEN_ABI from './jarvis_token_abi.json';
import { Mint } from './mint';
import JARVIS_POOL_ABI from './pool_abi.json';
import { Redeem } from './redeem';
import { RequestDTO } from './request';

export class MetaTxRelayer<
  Net extends SupportedNetworkName = SupportedNetworkName
> {
  private readonly tokenInstance: ethers.Contract;

  private readonly poolInstance: ethers.Contract;

  private readonly gasPrice: FPN;

  constructor(
    gasPrice: FPN,
    public readonly tokenAddress: AddressOn<Net>,
    public readonly poolAddress: AddressOn<Net>,
    public readonly signer: DefenderRelaySigner,
  ) {
    this.gasPrice = gasPrice;
    this.tokenInstance = new ethers.Contract(
      tokenAddress,
      JARVIS_TOKEN_ABI,
      signer as any,
    );
    this.poolInstance = new ethers.Contract(
      poolAddress,
      JARVIS_POOL_ABI,
      signer as any,
    );
  }

  public async getTokenInfo(): Promise<{
    symbol: string;
    decimals: number;
    derivative: AddressOn<Net>;
  }> {
    console.log(this.poolInstance);
    const symbol = await this.tokenInstance.methods.symbol().call();
    const decimals = parseInteger(await this.tokenInstance.decimals().call());
    const [derivative] = await this.poolInstance.getAllDerivatives();
    return {
      symbol,
      decimals,
      derivative: derivative as AddressOn<Net>,
    };
  }

  public async isSufficientAllowance(
    owner: AddressOn<Net>,
    spender: AddressOn<Net>,
    amount: FPN,
  ): Promise<boolean> {
    const allowanceAllowed = FPN.fromWei(
      await this.tokenInstance.allowance(owner, spender),
    );
    return allowanceAllowed.gte(amount);
  }

  public approveAllowance(
    spender: AddressOn<Net>,
    decimals: number,
  ): NonPayableTransactionObject<any> {
    const max = scaleTokenAmountToWei({
      amount: maxUint256,
      decimals,
    });

    const amountInWei = weiToTokenAmount({ wei: max, decimals });
    return this.tokenInstance.approve(spender, amountInWei);
  }

  public async increaseAllowance(
    spender: AddressOn<Net>,
    amount: FPN,
  ): Promise<NonPayableTransactionObject<any>> {
    const { decimals } = await this.getTokenInfo();
    const max = scaleTokenAmountToWei({
      amount: amount.bn,
      decimals,
    });

    const amountInWei = weiToTokenAmount({ wei: max, decimals });
    return this.tokenInstance.increaseAllowance(spender, amountInWei);
  }

  async permit(requestData: RequestDTO) {
    const estimatePermitTxGas = await this.tokenInstance.estimateGas.permit(
      requestData.owner,
      requestData.spender,
      new FPN(maxUint256).toString(),
      requestData.deadline,
      requestData.v,
      requestData.r,
      requestData.s,
    );

    const permitTx = await this.tokenInstance.permit(
      requestData.owner,
      requestData.spender,
      new FPN(maxUint256).toString(),
      requestData.deadline,
      requestData.v,
      requestData.r,
      requestData.s,
      {
        gasPrice: this.gasPrice,
        gasLimit: estimatePermitTxGas,
      },
    );
    console.log(permitTx);
    await permitTx.wait();
  }

  async redeem(params: Redeem) {
    const { derivative } = await this.getTokenInfo();
    const estimateRedeemTxGas = await this.poolInstance.redeem(
      [
        derivative,
        params.numTokens?.toString(),
        params.minCollateral?.toString(),
        params.feePercentage?.toString(),
        params.expiration,
        params.recipient,
      ],
      {
        gasPrice: this.gasPrice,
        gasLimit: 2000000,
      },
    );
    console.log(estimateRedeemTxGas);
    console.log(await estimateRedeemTxGas.wait());
  }

  async mint(params: Mint) {
    const { derivative } = await this.getTokenInfo();
    const estimateMintTx = await this.poolInstance.mint(
      [
        derivative,
        params.numTokens?.toString(),
        params.minCollateral?.toString(),
        params.feePercentage?.toString(),
        params.expiration,
        params.from,
      ],
      {
        gasPrice: this.gasPrice,
        gasLimit: 2000000,
      },
    );
    console.log(estimateMintTx);
    console.log(await estimateMintTx.wait());
  }

  async exchange(params: Exchange) {
    const { derivative } = await this.getTokenInfo();
    // TODO: get derivative , dest pool , destDerivative
    const estimageExchangeTx = await this.poolInstance.redeem(
      [
        derivative,
        params.numTokens?.toString(),
        params.minCollateral?.toString(),
        params.feePercentage?.toString(),
        params.expiration,
        params.from,
      ],
      {
        gasPrice: this.gasPrice,
        gasLimit: 2000000,
      },
    );
    console.log(estimageExchangeTx);
    console.log(await estimageExchangeTx.wait());
  }
}
