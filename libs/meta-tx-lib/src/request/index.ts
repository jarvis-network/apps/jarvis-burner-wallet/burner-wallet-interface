import { BASE_OPERATION } from '../interfaces';

/* eslint-disable camelcase */
export interface RequestDTO {
  owner: string;
  spender: string;
  from: string;
  permit_value: string;
  min_collateral: string;
  fee_percentage: string;
  value: string;
  deadline: number;
  op_type: keyof typeof BASE_OPERATION;
  to: string;
  pool: string;
  v: number;
  r: string;
  s: string;
}
