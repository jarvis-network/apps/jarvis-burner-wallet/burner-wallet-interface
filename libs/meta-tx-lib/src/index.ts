/* eslint-disable camelcase */
/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable require-await */
/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';
import {
  DefenderRelayProvider,
  DefenderRelaySigner,
} from 'defender-relay-client/lib/ethers';

import { Exchange } from './exchange';
import { Mint } from './mint';
import { Redeem } from './redeem';
import { RequestDTO } from './request';

export async function handler(event: any) {
  const requestData = event.request.body as RequestDTO;
  const credentials = { ...event };

  const provider = new DefenderRelayProvider(credentials);
  const signer = new DefenderRelaySigner(credentials, provider as any, {
    speed: 'fast',
  });
  const poolAddress = requestData.pool; // Pool
  const tokenAddress = requestData.to; // Derivative
  if (requestData.op_type === 'REDEEM') {
    const redeem = Redeem.of(requestData);
    console.log(redeem);
  }
  if (requestData.op_type === 'MINT') {
    const mint = Mint.of(requestData);
    console.log(mint);
  }
  if (requestData.op_type === 'EXCHANGE') {
    const exchange = Exchange.of(requestData);
    console.log(exchange);
  }
}
// Sample typescript type definitions
type EnvInfo = {
  API_KEY: string;
  API_SECRET: string;
};

// To run locally (this code will not be executed in Autotasks)
if (require.main === module) {
  require('dotenv').config();
  const { API_KEY: apiKey, API_SECRET: apiSecret } = process.env as EnvInfo;
  handler({
    apiKey,
    apiSecret,
    request: {
      body: {
        owner: '0x61b5a06ce0fcda6445fb454244ce84ed64c41aca',
        spender: '0x59a266d00d20b0edf7a8f12f468ce7211d8f0de2',
        from: '0x61b5a06ce0fcda6445fb454244ce84ed64c41aca',
        permit_value: new FPN(1000000000000).toString(),
        value: new FPN(10).toString(),
        deadline: 1668765976,
        op_type: 'REDEEM',
        to: '0x02CBE6055F8aad745321f70d6aDD4711455c7F45',
        pool: '0x06ED7844293F6433791A22D426Fb7Ad782Ed82f1',
        r: '0xa4ce705a6050218ab48f8f4212641e766263c7a18cfd831c7dafd7eb2198fd7d',
        s: '0x3b78bae3df2b9fb3102a12a03760eeaaaa28bf7cf48777f53a72380a4176486f',
        v: 28,
      },
    },
  })
    .then(() => process.exit(0))
    .catch((error: Error) => {
      console.error(error);
      process.exit(1);
    });
}
