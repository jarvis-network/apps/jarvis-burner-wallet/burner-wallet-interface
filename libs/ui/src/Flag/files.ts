import WBTC from './icons/BTC.svg';
import BUSD from './icons/BUSD.svg';
import jAUD from './icons/jAUD.svg';
import jBRL from './icons/jBRL.svg';
import jCAD from './icons/jCAD.svg';
import jCHF from './icons/jCHF.svg';
import jCOP from './icons/jCOP.svg';
import jEUR from './icons/jEUR.svg';
import jGBP from './icons/jGBP.svg';
import jJPY from './icons/jJPY.svg';
import jNGN from './icons/jNGN.svg';
import jPHP from './icons/jPHP.svg';
import jSEK from './icons/jSEK.svg';
import jSGD from './icons/jSGD.svg';
import jZAR from './icons/jZAR.svg';
import UMA from './icons/UMA.svg';
import USDC from './icons/USDC.svg';

export const files = {
  WBTC,
  BTCB: WBTC,
  jAUD,
  jCAD,
  jCHF,
  jCOP,
  jEUR,
  jGBP,
  jJPY,
  jPHP,
  jSEK,
  jSGD,
  UMA,
  USDC,
  BUSD,
  jZAR,
  jNGN,
  jBRL,
};

export type FlagKeys = keyof typeof files;

export { default as questionMark } from './icons/question-mark.svg';
