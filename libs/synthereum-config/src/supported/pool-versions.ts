export const poolVersions = ['v5'] as const;
export type PoolVersions = typeof poolVersions;
export type PoolVersion = PoolVersions[number];
