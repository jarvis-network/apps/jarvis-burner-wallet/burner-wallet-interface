import { typeCheck } from '@jarvis-network/core-utils/dist/base/meta';
import { ToNetworkId } from '@jarvis-network/core-utils/dist/eth/networks';

import {
  PairLike,
  PairToExactPair,
  PairToExactPairBUSD,
  PairToSynth,
  SynthereumCollateralSymbol,
  SynthereumPair,
} from '../types/all-price-feed-symbols';

import {
  PerNetwork,
  SupportedNetworkId,
  SupportedNetworkName,
} from './networks';

export type NetworkPairs<
  PairType extends PairLike<string, string, ''>
> = PerNetwork<PairType[]>;

export const supportedSynthereumPairs = typeCheck<
  NetworkPairs<SynthereumPair>
>()({
  1: [] as SynthereumPair[],
  42: [] as SynthereumPair[],
  137: [] as SynthereumPair[],
  80001: [] as SynthereumPair[],
  56: [] as SynthereumPair[],
} as const);

export type SupportedSynthereumPairs = typeof supportedSynthereumPairs;

export type SupportedSynthereumPair<
  Net extends SupportedNetworkName = SupportedNetworkName
> = SupportedSynthereumPairs[ToNetworkId<Net>][number];

export type SupportedSynthereumPairExact<
  Net extends SupportedNetworkName = SupportedNetworkName
> =
  | PairToExactPair<SupportedSynthereumPair<Net>>
  | PairToExactPairBUSD<SupportedSynthereumPair<Net>>;
export type xxd = SupportedSynthereumPairExact;
// jEUR
export type SupportedSynthereumSymbol<
  Net extends SupportedNetworkName = SupportedNetworkName
> = PairToSynth<SupportedSynthereumPair<Net>>;

// jEUR
export type SupportedSynthereumSymbolExact<
  Net extends SupportedNetworkName = SupportedNetworkName
> = keyof {
  [N in Net]: {
    [X in SupportedSynthereumSymbol<N>]: unknown;
  };
}[Net];
// EURUSD : config
export type PerPair<
  Config,
  Net extends SupportedNetworkName = SupportedNetworkName
> = {
  [Pair in SupportedSynthereumPairs[ToNetworkId<Net>][number]]: Config;
};

export type ExchangeSynthereumToken =
  | SupportedSynthereumSymbol
  | SynthereumCollateralSymbol;

// jEUR: config
export type PerSynthereumPair<
  Config,
  Net extends SupportedNetworkName = SupportedNetworkName
> = {
  [Pair in PairToSynth<
    SupportedSynthereumPairs[ToNetworkId<Net>][number]
  >]: Config;
};

// "jEUR/USDC": config;
export type PerSynthereumPairExact<
  Config,
  Net extends SupportedNetworkName = SupportedNetworkName
> = {
  [Pair in PairToExactPair<
    SupportedSynthereumPairs[ToNetworkId<Net>][number]
  >]?: Config;
};
export type PerSynthereumCollateralPair<Config> = {
  [N in SupportedNetworkId]: {
    [Pair in SynthereumCollateralSymbol]: Config;
  };
};
export type PerSynthereumPairExact2<
  Config,
  Net extends SupportedNetworkName = SupportedNetworkName
> = {
  [N in SupportedNetworkId]:
    | PerSynthereumPairExact<Config, Net>
    | PerSynthereumCollateralPair<Config>;
};

export type xx = PerSynthereumPairExact<string>;
