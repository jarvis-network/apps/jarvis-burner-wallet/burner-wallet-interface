import { StringAmount } from '@jarvis-network/core-utils/dist/base/big-number';
import { AddressOn } from '@jarvis-network/core-utils/dist/eth/address';

import {
  ExchangeSynthereumToken,
  SupportedSynthereumPair,
  SupportedSynthereumPairExact,
  SupportedSynthereumSymbol,
} from '../supported/all-synthereum-pairs';
import {
  SupportedNetworkId,
  SupportedNetworkName,
} from '../supported/networks';

export type ChainlinkPair = SupportedSynthereumPair;

export type PerPair<Config> = {
  [Pair in ChainlinkPair]?: Config;
};
export type x = SupportedSynthereumPair;
export type ChainLinkPriceAggregators = {
  [Net in SupportedNetworkId]: PerPair<AddressOn<Net>>;
};

// export type x = ChainLinkPriceAggregators;

export interface Fees<Net extends SupportedNetworkName> {
  feePercentage: StringAmount; // Example: weiString(0.002),
  feeRecipients: AddressOn<Net>[]; // Example: ["0xCc3528125499d168ADFB5Ef99895c98a7C430ed4"]
  feeProportions: number[]; // Example: [50, 50]
}

export interface Roles<Net extends SupportedNetworkName> {
  admin: AddressOn<Net>;
  maintainer: AddressOn<Net>;
  liquidityProvider: AddressOn<Net>;
  validator: AddressOn<Net>;
}

export interface BurnerWalletTokenConfig {
  name?: string; /// Example: "Jarvis Synthetic Euro",
  symbol?: ExchangeSynthereumToken; /// Example: "jEUR",
  // chainLinkPriceFeed?: SupportedSynthereumPairExact;
  chainLinkFeedIdentifier?: SupportedSynthereumPairExact;
  nonceFN?: string;
  network?: {
    [Net in SupportedNetworkId]: Partial<AddressOn<Net>>;
  }; // We can load Address from the registry
}
export type BurnerTokens<
  Net extends SupportedNetworkName = SupportedNetworkName
> = {
  [SynthSymbol in SupportedSynthereumSymbol<Net>]?: BurnerWalletTokenConfig;
};
export type xd = BurnerTokens;

export interface FixedPointNumber {
  rawValue: StringAmount;
}
