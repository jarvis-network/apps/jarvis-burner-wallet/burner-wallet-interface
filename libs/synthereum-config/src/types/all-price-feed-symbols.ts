import {
  PerTupleElement,
  typeCheck,
} from '@jarvis-network/core-utils/dist/base/meta';

// primary definitions:

export const allCollateralSymbols = ['USDC', 'BUSD'] as const;
export const synthereumCollateralSymbols = typeCheck<CollateralSymbol[]>()([
  ...allCollateralSymbols,
]);

export const assetSymbols = [
  'EUR',
  'GBP',
  'CHF',
  'ZAR',
  'CAD',
  'KRW',
  'PHP',
  'JPY',
  'NGN',
  'XAU',
  'SPX',
  'XTI',
  'XAG',
  'AUD',
  'COP',
  'SGD',
  'SEK',
  'BRL',
] as const;

export const nonSyntheticAssetSymbols = ['BTC', 'ETH'] as const;

export type PairLike<
  AssetType extends string,
  CollateralType extends string,
  Separator extends string = '/'
> = `${AssetType}${Separator}${CollateralType}`;

export type CollateralSymbol = typeof allCollateralSymbols[number];
export type SynthereumCollateralSymbol = typeof synthereumCollateralSymbols[number];
export type AssetSymbol = typeof assetSymbols[number];
export type NonAssetSymbol = typeof nonSyntheticAssetSymbols[number];
export type SyntheticSymbol = `j${AssetSymbol}` | `W${NonAssetSymbol}` | 'BTCB';

export type AnySynthereumPair = `${SyntheticSymbol}/${SynthereumCollateralSymbol}`;

export type SynthereumPair =
  | SyntheticToForexPair<AnySynthereumPair, 'USDC', 'USD', 'j' | 'W'>
  | 'BTCBUSD';

export type PairToSynth<Pair extends string> = Pair extends `${infer Asset}${
  | 'USD'
  | SynthereumCollateralSymbol}`
  ? `j${Asset}` extends SyntheticSymbol
    ? `j${Asset}`
    : `W${Asset}` extends SyntheticSymbol
    ? `W${Asset}`
    : never
  : never;

export type SyntheticToForexPair<
  Pair extends string,
  Collateral extends string,
  CollateralReplacement extends string = Collateral,
  SyntheticPrefix extends string = 'j'
> = Pair extends `${SyntheticPrefix}${infer Asset}/${Collateral}`
  ? Asset extends Collateral
    ? never
    : `${Asset}${CollateralReplacement}`
  : never;

export type PerAsset<Config> = PerTupleElement<
  typeof allSyntheticSymbols,
  Config
> &
  PerTupleElement<['USDC', 'BUSD'], Config>;

type AdjustUsdc<Sym extends string> = Sym extends 'USD' ? 'USDC' : Sym;

type AdjustBusd<Sym extends string> = Sym extends 'USD' ? 'BUSD' : Sym;

// "EURBUSD" | "EURUSD", "GBPUSD" | "GBPUMA" -> "jEUR/USDC" | "jEUR/BUSD" | "jGBP/USDC" | "jGBP/BUSD"
export type PairToExactPair<
  Pair extends string
> = Pair extends `${infer Asset}${SynthereumCollateralSymbol | 'USD'}`
  ? Pair extends `${Asset}${infer Collateral}`
    ? `j${Asset}` extends SyntheticSymbol
      ? `j${Asset}/${AdjustUsdc<Collateral>}`
      : `W${Asset}` extends SyntheticSymbol
      ? `W${Asset}/${AdjustUsdc<Collateral>}`
      : never
    : never
  : never;
export type PairToExactPairBUSD<
  Pair extends string
> = Pair extends `${infer Asset}${SynthereumCollateralSymbol | 'USD'}`
  ? Pair extends `${Asset}${infer Collateral}`
    ? `j${Asset}` extends SyntheticSymbol
      ? `j${Asset}/${AdjustBusd<Collateral>}`
      : `W${Asset}` extends SyntheticSymbol
      ? `W${Asset}/${AdjustBusd<Collateral>}`
      : `BTCB/${AdjustBusd<Collateral>}`
    : never
  : never;

export type PriceFeed = PerAsset<string>;

export type AssetFromSyntheticSymbol<
  ExactPair extends string
> = ExactPair extends `j${infer Asset}` ? Asset : never;

export type CollateralOf<
  ExactPair extends string
> = ExactPair extends `j${AssetSymbol}/${infer Collateral}`
  ? Collateral
  : never;

export type BurnerWalletPair = SyntheticToForexPair<
  AnySynthereumPair,
  'USDC',
  'USD',
  'j'
>;
// derived definitions:

export const primaryCollateralSymbol = synthereumCollateralSymbols[0];

export const allSyntheticSymbols = typeCheck<SyntheticSymbol[]>()([
  'jEUR',
  'jGBP',
  'jCHF',
  'jZAR',
  'jCAD',
  'jKRW',
  'jPHP',
  'jJPY',
  'jNGN',
  'jXAU',
  'jSPX',
  'jXTI',
  'jXAG',
  'jAUD',
  'jCOP',
  'jSGD',
  'jSEK',
  'jBRL',
  'WBTC',
  'WETH',
  'BTCB',
] as const);
