import { valuesOf } from '@jarvis-network/core-utils/dist/base/meta';
import {
  ExchangeSynthereumToken as EST,
  PoolVersion,
  SupportedNetworkName,
  SupportedSynthereumSymbol,
  SynthereumCollateralSymbol,
} from '@jarvis-network/synthereum-config';
import { burnerWalletConfig } from '@jarvis-network/synthereum-config/dist/data';
import { ExchangeSynthereumToken } from '@jarvis-network/synthereum-config/src/supported/all-synthereum-pairs';

import { PoolsForVersion } from './types/pools';

export function isSupportedSynth<
  Version extends PoolVersion,
  Net extends SupportedNetworkName
>(
  activePools: PoolsForVersion<Version, Net>,
  token: EST,
): token is SupportedSynthereumSymbol {
  return token in activePools;
}

export function isSupportedCollateral<
  Version extends PoolVersion,
  Net extends SupportedNetworkName
>(
  activePools: PoolsForVersion<Version, Net>,
  token: EST,
): token is SynthereumCollateralSymbol {
  // TODO: optimize by caching a table of all known collateral symbols
  return valuesOf(activePools).some(
    pool => pool?.collateralToken.symbol === token,
  );
}

export function isSupportedExtra(
  token: ExchangeSynthereumToken,
): token is ExchangeSynthereumToken {
  // TODO: optimize by caching a table of all known collateral symbols
  return valuesOf(burnerWalletConfig).some(pool => pool?.symbol === token);
}
type TxType =
  | 'mint'
  | 'exchange'
  | 'redeem'
  | 'unsupported'
  | 'atomicSwapMint'
  | 'atomicSwapRedeem';

export function determineSide<
  Version extends PoolVersion,
  Net extends SupportedNetworkName
>(activePools: PoolsForVersion<Version, Net>, input: EST, output: EST): TxType {
  const synthInput = isSupportedSynth(activePools, input);
  const synthOutput = isSupportedSynth(activePools, output);
  const collateralInput = isSupportedCollateral(activePools, input);
  const collateralOutput = isSupportedCollateral(activePools, output);
  const extraInput = isSupportedExtra(input);
  const extraOutput = isSupportedExtra(output);

  if (collateralInput && synthOutput) return 'mint';

  if (synthInput && collateralOutput) return 'redeem';

  if (synthInput && synthOutput) return 'exchange';

  if (extraInput && synthOutput) return 'atomicSwapMint';
  if (synthInput && extraOutput) return 'atomicSwapRedeem';
  if ((output as any) === 'BTCB') return 'atomicSwapRedeem';
  if ((input as any) === 'BTCB') return 'atomicSwapMint';
  return 'unsupported';
}
