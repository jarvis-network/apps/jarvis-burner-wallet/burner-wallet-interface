import { parseInteger } from '@jarvis-network/core-utils/dist/base/asserts';
import { AddressOn } from '@jarvis-network/core-utils/dist/eth/address';
import { getContract } from '@jarvis-network/core-utils/dist/eth/contracts/get-contract';
import type {
  NetworkName,
  Web3On,
} from '@jarvis-network/core-utils/dist/eth/web3-instance';
import {
  ERC20Permit_Abi,
  SynthereumTrustedForwarder_Abi,
  UChildERC20_Abi,
} from '@jarvisnetwork/synthereum-contracts/build/abi';

import {
  TokenInstancePermit,
  TokenInstanceTrustForwarder,
  TokenInstanceUChildERC20,
} from '../types/pools';

export async function getTokenInfo<
  TokenSymbol extends string,
  Net extends NetworkName
>(
  web3: Web3On<Net>,
  address: AddressOn<Net>,
): Promise<TokenInstancePermit<Net, TokenSymbol>> {
  const { instance, connect } = getContract(web3, ERC20Permit_Abi, address);
  const symbol = (await instance.methods.symbol().call()) as TokenSymbol;
  const decimals = parseInteger(await instance.methods.decimals().call());
  return {
    address,
    connect,
    instance,
    symbol,
    decimals,
  };
}

export async function get3rdPartyTokenInfo<
  TokenSymbol extends string,
  Net extends NetworkName
>(
  web3: Web3On<Net>,
  address: AddressOn<Net>,
): Promise<TokenInstanceUChildERC20<Net, TokenSymbol>> {
  const { instance, connect } = getContract(web3, UChildERC20_Abi, address);
  const symbol = (await instance.methods.symbol().call()) as TokenSymbol;
  const decimals = parseInteger(await instance.methods.decimals().call());
  return {
    address,
    connect,
    instance,
    symbol,
    decimals,
  };
}

export function getGenericTokenInfo<Net extends NetworkName>(
  web3: Web3On<Net>,
  address: AddressOn<Net>,
  abi: any,
): any {
  const { instance, connect } = getContract(web3, abi, address);

  return {
    address,
    connect,
    instance,
    symbol: '',
    decimals: 0,
  };
}

export async function getTrustForwarder<
  TokenSymbol extends string,
  Net extends NetworkName
>(
  web3: Web3On<Net>,
  address: AddressOn<Net>,
): Promise<TokenInstanceTrustForwarder<Net, TokenSymbol>> {
  const { instance, connect } = await getContract(
    web3,
    SynthereumTrustedForwarder_Abi,
    address,
  );
  return {
    address,
    connect,
    instance,
    symbol: 'TF0' as TokenSymbol,
    decimals: 0,
  };
}
export const AppEvents = [
  'networkSwitch',
  'addressSwitch',
  'transaction/metaMaskError',
  'transaction/cancel',
  'transaction/reset',
  'transaction/confirmed',
  'transaction/send',
  'transaction/validate',
  'transaction/metaMaskConfirmation',
  'approveTransaction/cancel',
  'approveTransaction/confirmed',
  'approveTransaction/send',
  'approveTransaction/metaMaskConfirmation',
];
