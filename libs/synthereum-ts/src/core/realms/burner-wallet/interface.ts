import { StringAmount } from '@jarvis-network/core-utils/dist/base/big-number';
import { TxOptions } from '@jarvis-network/core-utils/dist/eth/contracts/send-tx';
import { ExchangeSynthereumToken } from '@jarvis-network/synthereum-config/dist/supported/all-synthereum-pairs';

export interface ContractParams {
  inputToken: ExchangeSynthereumToken | 'BTCB';
  outputToken: ExchangeSynthereumToken;
  inputAmountWei: StringAmount;
  outputAmountWei: StringAmount;
  recipientAddress?: string;
  txOptions?: TxOptions;
}
