import { catchError, filter, map, of, switchMap, takeUntil } from 'rxjs';

import { Context, Epic, ReduxAction } from '../../../epics/types';
import { AppEvents } from '../common-realm';

import { genericTx } from './generic';
import { ContractParams } from './interface';

type CamelCase<
  S extends string
> = S extends `${infer P1}_${infer P2}${infer P3}`
  ? `${Lowercase<P1>}${Uppercase<P2>}${CamelCase<P3>}`
  : Lowercase<S>;

type RealmFunctions = 'MINT' | 'APPROVE' | 'EXCHANGE' | 'SEND';
type RealmOp = `CALL_${RealmFunctions}`;
type TxOptions = {
  [Op in RealmOp]: [CamelCase<RealmFunctions>];
};

const txOptions: TxOptions = {
  CALL_MINT: ['mint'],
  CALL_APPROVE: ['approve'],
  CALL_EXCHANGE: ['exchange'],
  CALL_SEND: ['send'],
};

export function createBurnerRealmAgentEpic(
  actionType: RealmOp,
): Epic<ReduxAction, ReduxAction> {
  return (action$, _state$, { context$ }) =>
    action$.pipe(
      filter(action => action.type === actionType),
      switchMap(action =>
        context$!.pipe(
          map(context => ({
            context,
            payload: action.payload as ContractParams,
          })),
          takeUntil(action$.pipe(filter(a => AppEvents.indexOf(a.type) > -1))),
        ),
      ),
      switchMap(
        ({ payload, context }: { payload: ContractParams; context: Context }) =>
          genericTx(context, ...txOptions[actionType], payload),
      ),
      catchError(err => {
        console.log(err);
        return of({
          type: `metaMaskConfirmation`,
        });
      }),
    );
}

export function loadTransactionEpic(): Epic<ReduxAction, ReduxAction> {
  return (action$, _state$, { context$ }) =>
    action$.pipe(
      filter(action => action.type === 'LOAD_TRANSACTION'),
      switchMap(action =>
        context$!.pipe(
          map(context => ({
            context,
            payload: action.payload as string,
          })),
          takeUntil(action$.pipe(filter(a => AppEvents.indexOf(a.type) > -1))),
        ),
      ),
      switchMap(({ payload, context }: { payload: string; context: Context }) =>
        context.burnerRealmAgent!.getEvents(payload),
      ),
      catchError(err => {
        console.log(err);
        return of({
          type: `metaMaskConfirmation`,
        });
      }),
    );
}
