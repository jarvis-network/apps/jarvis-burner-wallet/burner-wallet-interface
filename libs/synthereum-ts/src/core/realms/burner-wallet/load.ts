import { last } from '@jarvis-network/core-utils/dist/base/array-fp-utils';
import { throwError } from '@jarvis-network/core-utils/dist/base/asserts';
import { t } from '@jarvis-network/core-utils/dist/base/meta';
import {
  AddressOn,
  assertIsAddress,
  isAddress,
} from '@jarvis-network/core-utils/dist/eth/address';
import { getContract } from '@jarvis-network/core-utils/dist/eth/contracts/get-contract';
import type {
  NetworkId,
  ToNetworkId,
} from '@jarvis-network/core-utils/dist/eth/networks';
import type { Web3On } from '@jarvis-network/core-utils/dist/eth/web3-instance';
import { SupportedNetworkName } from '@jarvis-network/synthereum-config';
import { burnerWalletSynthereumConfig } from '@jarvis-network/synthereum-config/dist/data';
import { SupportedSynthereumSymbol } from '@jarvis-network/synthereum-config/dist/supported/all-synthereum-pairs';
import { ISynthereumRegistry_Abi } from '@jarvisnetwork/synthereum-contracts/build/abi';
import { ISynthereumRegistry } from '@jarvisnetwork/synthereum-contracts/build/typechain/ISynthereumRegistry';

import { loadPool } from '../../pool-utils';
import type {
  PoolsForVersion,
  PoolVersion,
  SynthereumPool,
} from '../../types/pools';
import type { SynthereumRealmWithWeb3 } from '../../types/realm';
import { getTokenInfo } from '../common-realm';

export type BurnerSupportedNetwork = Extract<NetworkId, 137>;
export type PoolVersionsToLoad<Net extends SupportedNetworkName> = {
  [Version in PoolVersion]?: PoolsForVersion<Version, Net> | null;
};
export function loadRealm<Net extends SupportedNetworkName>(
  web3: Web3On<Net>,
  netId: ToNetworkId<Net>,
  versionsToLoad?: PoolVersionsToLoad<Net>,
): Promise<SynthereumRealmWithWeb3<Net>> {
  const config = burnerWalletSynthereumConfig[netId as BurnerSupportedNetwork];
  return loadCustomRealm(web3, netId, config, versionsToLoad);
}

export async function loadCustomRealm<Net extends SupportedNetworkName>(
  web3: Web3On<Net>,
  netId: ToNetworkId<Net>,
  config: any,
  versionsToLoad: PoolVersionsToLoad<Net> = { v5: null },
): Promise<SynthereumRealmWithWeb3<Net>> {
  const poolRegistry = getContract(
    web3,
    ISynthereumRegistry_Abi,
    config.poolRegistry,
  );

  const collateralAddress = await poolRegistry.instance.methods
    .getCollaterals()
    .call();
  const getSyntheticTokens = await poolRegistry.instance.methods
    .getSyntheticTokens()
    .call();

  const loadAllPools = async <Version extends PoolVersion>(
    version: Version,
  ) => {
    const pairs = await Promise.all(
      getSyntheticTokens.map(async (symbol: any) => {
        const info = await loadPoolInfo(
          web3,
          netId,
          poolRegistry.instance,
          last(collateralAddress) as AddressOn<Net>,
          version,
          symbol,
        );

        return t(symbol, info);
      }),
    );
    return Object.fromEntries(pairs.filter(x => !!x[1]));
  };

  const pools: SynthereumRealmWithWeb3<Net>['pools'] = {};
  for (const i in versionsToLoad) {
    if (!Object.prototype.hasOwnProperty.call(versionsToLoad, i)) continue;
    const version = i as PoolVersion;
    const poolsForVersion = versionsToLoad[version];

    pools[version] =
      typeof poolsForVersion === 'object' && poolsForVersion !== null
        ? poolsForVersion
        : // eslint-disable-next-line no-await-in-loop
          await loadAllPools(version);
  }

  return {
    web3,
    netId,
    poolRegistry,
    pools,
  };
}
function poolVersionId(version: PoolVersion) {
  return version === 'v5'
    ? 5
    : throwError(`'${version}' is not a supported pool version`);
}

export async function loadPoolInfo<
  Version extends PoolVersion,
  SynthSymbol extends SupportedSynthereumSymbol<Net>,
  Net extends SupportedNetworkName
>(
  web3: Web3On<Net>,
  netId: ToNetworkId<Net>,
  poolRegistry: ISynthereumRegistry,
  collateralAddress: AddressOn<Net>,
  version: Version,
  symbol: SynthSymbol,
): Promise<SynthereumPool<Version, Net, SynthSymbol> | null> {
  const versionId = poolVersionId(version);
  const poolAddresses = await poolRegistry.methods
    .getElements(symbol, collateralAddress, versionId)
    .call();

  // Assume the last address in the array is the one we should interact with
  const lastPoolAddress = last(poolAddresses);

  if (!isAddress(lastPoolAddress)) {
    return null;
  }

  const poolAddress = assertIsAddress(lastPoolAddress) as AddressOn<Net>;

  const { result: poolInstance } = await loadPool(web3, version, poolAddress);
  const collateralTokenAddress = assertIsAddress(
    await poolInstance.instance.methods.collateralToken().call(),
  ) as AddressOn<Net>;

  if (
    collateralTokenAddress.toLowerCase() !== collateralAddress.toLowerCase()
  ) {
    throwError(
      `Collateral token mismatch - expected: '${collateralAddress}', ` +
        `got: '${collateralTokenAddress}'`,
    );
  }

  const syntheticTokenAddress = assertIsAddress(
    await poolInstance.instance.methods.syntheticToken().call(),
  ) as AddressOn<Net>;

  return {
    versionId: version,
    networkId: netId,

    symbol,
    ...poolInstance,
    syntheticToken: await getTokenInfo(web3, syntheticTokenAddress),
    collateralToken: await getTokenInfo(web3, collateralTokenAddress),
    derivative: {
      address: '0x0000000000000000000000000000000000000000' as AddressOn<Net>,
      // TODO:Important debug this later
      // address: assertIsAddress(
      //   (await poolInstance.instance.methods.getAllDerivatives().call())[0],
      // ) as AddressOn<Net>,
      connect: () => throwError('Unsupported function'),
    },
  };
}

export async function loadERC20PoolInfo<
  Version extends PoolVersion,
  SynthSymbol extends SupportedSynthereumSymbol<Net>,
  Net extends SupportedNetworkName
>(
  web3: Web3On<Net>,
  netId: ToNetworkId<Net>,
  poolRegistry: ISynthereumRegistry,
  collateralAddress: AddressOn<Net>,
  version: Version,
  symbol: SynthSymbol,
): Promise<SynthereumPool<Version, Net, SynthSymbol> | null> {
  const versionId = poolVersionId(version);
  const poolAddresses = await poolRegistry.methods
    .getElements(symbol, collateralAddress, versionId)
    .call();

  // Assume the last address in the array is the one we should interact with
  const lastPoolAddress = last(poolAddresses);

  if (!isAddress(lastPoolAddress)) {
    return null;
  }

  const poolAddress = assertIsAddress(lastPoolAddress) as AddressOn<Net>;

  const { result: poolInstance } = await loadPool(web3, version, poolAddress);

  const collateralTokenAddress = assertIsAddress(
    await poolInstance.instance.methods.collateralToken().call(),
  ) as AddressOn<Net>;

  if (
    collateralTokenAddress.toLowerCase() !== collateralAddress.toLowerCase()
  ) {
    throwError(
      `Collateral token mismatch - expected: '${collateralAddress}', ` +
        `got: '${collateralTokenAddress}'`,
    );
  }

  const syntheticTokenAddress = assertIsAddress(
    await poolInstance.instance.methods.syntheticToken().call(),
  ) as AddressOn<Net>;

  return {
    versionId: version,
    networkId: netId,

    symbol,
    ...poolInstance,
    syntheticToken: await getTokenInfo(web3, syntheticTokenAddress),
    collateralToken: await getTokenInfo(web3, collateralTokenAddress),
    derivative: {
      address: '0x000000000000000000' as AddressOn<Net>,
      // address: assertIsAddress(
      //   (await poolInstance.instance.methods.getAllDerivatives().call())[0],
      // ) as AddressOn<Net>,
      connect: () => throwError('Unsupported function'),
    },
  };
}
