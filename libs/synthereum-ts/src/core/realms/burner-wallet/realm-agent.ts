/* eslint-disable no-await-in-loop */
/* eslint-disable class-methods-use-this */
/* eslint-disable camelcase */
import {
  assert,
  assertNotNull,
} from '@jarvis-network/core-utils/dist/base/asserts';
import { Amount, wei } from '@jarvis-network/core-utils/dist/base/big-number';
import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';
import { ascendingRange } from '@jarvis-network/core-utils/dist/base/iterables';
import { t } from '@jarvis-network/core-utils/dist/base/meta';
import { AddressOn } from '@jarvis-network/core-utils/dist/eth/address';
import {
  getTokenBalance,
  scaleTokenAmountToWei,
  weiToTokenAmount,
} from '@jarvis-network/core-utils/dist/eth/contracts/erc20';
import {
  FullTxOptions,
  TxOptions,
} from '@jarvis-network/core-utils/dist/eth/contracts/send-tx';
import { TokenInstance } from '@jarvis-network/core-utils/dist/eth/contracts/types';
import {
  ExchangeSynthereumToken,
  SupportedNetworkId,
  SupportedNetworkName,
  SupportedSynthereumSymbol,
} from '@jarvis-network/synthereum-config';
import {
  burnerWalletConfig,
  BurnerWalletExtraTokens,
} from '@jarvis-network/synthereum-config/dist/data';
import { OnChainLiquidityRouterV2_Abi } from '@jarvisnetwork/atomic-swap/build/abi';
import { ERC20Permit } from '@jarvisnetwork/synthereum-contracts/build/typechain';
import BN from 'bn.js';
import * as util from 'eth-sig-util';
import { ethers } from 'ethers';
import { Observable } from 'rxjs';

import { ReduxAction } from '../../../epics/types';
import { mapPools } from '../../pool-utils';
import { determineSide, isSupportedCollateral } from '../../realm-utils';
import { PoolsForVersion, PoolVersion } from '../../types/pools';
import { SynthereumRealmWithWeb3 } from '../../types/realm';
import {
  get3rdPartyTokenInfo,
  getGenericTokenInfo,
  getTokenInfo,
  getTrustForwarder,
} from '../common-realm';

import { decodeSignature } from './decodeSignature';
import { getForwardRequestTypedData } from './message/forwardRequest';
import { getMetaTransactionTypedData } from './message/metaTransaction';
import { getTypedData } from './message/permit';
import { cartesian } from './util';

interface BaseTxParams {
  collateral: Amount;
  txOptions?: TxOptions;
}

export interface MintParams<Net extends SupportedNetworkName>
  extends BaseTxParams {
  outputSynth: SupportedSynthereumSymbol<Net>;
  outputAmount: Amount;
}
export const MAX_ALLOWANCE_IN_ETH = 1000000000000;

export const RELAYER = '0xca777b46b9eb34a862307f15301eb3811f4ad7a4';
const onChainLiquidationRouter = '0x49e88c9036e4f888069a354465f1738b372df03b';
export class RealmAgent<
  Net extends SupportedNetworkName = SupportedNetworkName
> {
  public readonly activePools: PoolsForVersion<PoolVersion, Net>;

  public readonly collateralToken: TokenInstance<SupportedNetworkName>;

  private readonly defaultTxOptions: FullTxOptions<Net>;

  constructor(
    public readonly realm: SynthereumRealmWithWeb3<Net>,
    public readonly agentAddress: AddressOn<Net>,
    public readonly privateKey: string,
    public readonly poolVersion: PoolVersion,
  ) {
    this.activePools = assertNotNull(
      realm.pools![poolVersion],
      `realm.pools[${poolVersion}] is null`,
    );

    // Assume all pools use the same collateral
    const pools = this.activePools as PoolsForVersion<
      PoolVersion,
      SupportedNetworkName
    >;
    this.collateralToken = assertNotNull(pools.jEUR?.collateralToken);
    this.defaultTxOptions = {
      from: this.agentAddress,
      web3: this.realm.web3,
      confirmations: 1,
    };
  }

  collateralBalance(): Promise<Amount> {
    return getTokenBalance(this.collateralToken, this.agentAddress);
  }

  syntheticTokenBalanceOf(
    synthetic: SupportedSynthereumSymbol<Net>,
  ): Promise<Amount> {
    const asset = this.activePools[synthetic]!.syntheticToken;
    return getTokenBalance(asset, this.agentAddress);
  }

  getAllBalances(): Promise<[ExchangeSynthereumToken, Amount][]> {
    return Promise.all([
      (async (): Promise<[ExchangeSynthereumToken, Amount]> =>
        t(
          this.collateralToken.symbol as ExchangeSynthereumToken,
          await getTokenBalance(this.collateralToken, this.agentAddress),
        ))(),
      ...mapPools(this.realm, this.poolVersion, async p =>
        t(
          p.symbol as ExchangeSynthereumToken,
          await getTokenBalance(p.syntheticToken, this.agentAddress),
        ),
      ),
    ]);
  }

  private determineSide(
    input: ExchangeSynthereumToken,
    output: ExchangeSynthereumToken,
  ) {
    return determineSide(this.activePools, input, output);
  }

  private isCollateral(token: ExchangeSynthereumToken) {
    return isSupportedCollateral(this.activePools, token);
  }

  private static getExpiration(): number {
    const timeout = 4 * 36000;
    return ((Date.now() / 1000) | 0) + timeout;
  }

  public async universalExchange({
    inputToken,
    outputToken,
    inputAmountWei,
    outputAmountWei,
  }: {
    inputToken: ExchangeSynthereumToken;
    outputToken: ExchangeSynthereumToken;
    inputAmountWei: Amount;
    outputAmountWei: Amount;
    txOptions?: TxOptions;
  }) {
    const side = this.determineSide(inputToken, outputToken);
    console.log(side);
    assert(
      side !== 'unsupported',
      'Unsupported exchange: ' +
        `input ${inputAmountWei} ${inputToken} -> output ${outputAmountWei} ${outputToken}`,
    );
    if (side !== 'atomicSwapMint' && side !== 'atomicSwapRedeem') {
      let targetPool = this.activePools[
        inputToken as SupportedSynthereumSymbol<Net>
      ];
      if (targetPool === undefined) {
        targetPool = this.activePools[
          outputToken as SupportedSynthereumSymbol<Net>
        ];
      }
      if (targetPool !== undefined) {
        const inputAmount = this.isCollateral(inputToken)
          ? weiToTokenAmount({
              wei: wei(inputAmountWei.toString()),
              decimals: targetPool.collateralToken.decimals,
            })
          : inputAmountWei;

        let txData: any = {};
        if (side === 'mint') {
          const netId = await this.realm.web3.eth.net.getId();
          const time = RealmAgent.getExpiration();

          const mintData = targetPool.instance.methods
            .mint([
              new FPN(0).toString(),
              inputAmount.toString(),
              time,
              this.agentAddress.toString(),
            ])
            .encodeABI();
          let estimateGas = 400000;
          try {
            estimateGas = await targetPool.instance.methods
              .mint([
                new FPN(0).toString(),
                inputAmount.toString(),
                time,
                this.agentAddress.toString(),
              ])
              .estimateGas({ from: this.agentAddress });
            // eslint-disable-next-line no-empty
          } catch (error) {}

          const forwardContract = await getTrustForwarder(
            this.realm.web3,
            '0x298259b647d57a8ff6ae82112656b7e30c76601b' as AddressOn<Net>,
          );
          const dataToSign = getForwardRequestTypedData({
            name: 'MinimalForwarder',
            version: '0.0.1',
            verifyingContract: '0x298259b647d57a8ff6ae82112656b7e30c76601b',
            chainId: netId,
            from: this.agentAddress.toString(),
            to: targetPool.address.toString(),
            value: '0',
            gas: estimateGas,
            nonce: parseInt(
              await forwardContract.instance.methods
                .getNonce(this.agentAddress)
                .call(),
              10,
            ),
            data: mintData,
          });
          console.log(dataToSign);

          const signature = util.signTypedData<any>(
            Buffer.from(this.privateKey.slice(2), 'hex'),
            {
              data: dataToSign,
            },
          );

          txData = {
            spender: RELAYER,
            deadline: time,
            to: targetPool.collateralToken.address,
            from: this.agentAddress,
            allowance_Value: weiToTokenAmount({
              wei: wei(new FPN(MAX_ALLOWANCE_IN_ETH).toString()),
              decimals: targetPool.collateralToken.decimals,
            }).toString(),
            value: inputAmount.toString(),
            op_type: 'FULL_MINT',
            mintGas: estimateGas,
            data: mintData,
            pool: targetPool.address,
            signature,
          };
        } else if (side === 'exchange') {
          const outputPool = this.activePools[
            outputToken as SupportedSynthereumSymbol<Net>
          ]!;

          const time = RealmAgent.getExpiration();
          const permitDataToSign = getTypedData({
            name: await targetPool.syntheticToken.instance.methods
              .name()
              .call(),
            version: '1',
            chainId: await this.realm.web3.eth.net.getId(),
            verifyingContract: targetPool.syntheticToken.address.toLowerCase(),
            nonce: parseInt(
              await targetPool.syntheticToken.instance.methods
                .nonces(this.agentAddress)
                .call(),
              10,
            ),
            owner: this.agentAddress.toLowerCase(),
            spender: targetPool.address.toLowerCase(),
            value: new FPN(MAX_ALLOWANCE_IN_ETH).toString(),
            deadline: time,
          });

          const netId = await this.realm.web3.eth.net.getId();

          const permitSignature = util.signTypedData<any>(
            Buffer.from(this.privateKey.slice(2), 'hex'),
            {
              data: permitDataToSign,
            },
          );
          const { r, s, v } = decodeSignature(permitSignature);
          // 2nd transaction
          const forwardContract = await getTrustForwarder(
            this.realm.web3,
            '0x298259b647d57a8ff6ae82112656b7e30c76601b' as AddressOn<Net>,
          );

          const exchangeData = await targetPool.instance.methods
            .exchange([
              outputPool.address.toString(),
              inputAmount.toString(),
              new FPN(0).toString(),
              time,
              this.agentAddress,
            ])
            .encodeABI();
          let estimateGas = 400000;
          try {
            estimateGas = await targetPool.instance.methods
              .exchange([
                outputPool.address.toString(),
                inputAmount.toString(),
                new FPN(0).toString(),
                time,
                this.agentAddress,
              ])
              .estimateGas({
                from: this.agentAddress,
              });
            // eslint-disable-next-line no-empty
          } catch (error: any) {}

          const dataToSign = getForwardRequestTypedData({
            name: 'MinimalForwarder',
            version: '0.0.1',
            verifyingContract: '0x298259b647d57a8ff6ae82112656b7e30c76601b',
            chainId: netId,
            from: this.agentAddress.toLowerCase().toString(),
            to: targetPool.address.toLowerCase().toString(),
            value: '0',
            gas: estimateGas,
            nonce: parseInt(
              await forwardContract.instance.methods
                .getNonce(this.agentAddress)
                .call(),
              10,
            ),
            data: exchangeData,
          });
          console.log(dataToSign);
          const signature = util.signTypedData<any>(
            Buffer.from(this.privateKey.slice(2), 'hex'),
            {
              data: dataToSign,
            },
          );

          txData = {
            spender: targetPool.address,
            deadline: time,
            to: targetPool.syntheticToken.address,
            from: this.agentAddress,
            allowance_Value: weiToTokenAmount({
              wei: wei(new FPN(MAX_ALLOWANCE_IN_ETH).toString()),
              decimals: targetPool.syntheticToken.decimals,
            }).toString(),
            value: inputAmount.toString(),
            op_type: 'FULL_EXCHANGE',
            pool: targetPool.address,
            destPool: outputPool.address,
            data: exchangeData,
            exchangeGas: estimateGas,
            signature,
            r,
            s,
            v,
          };
        } else if (side === 'redeem') {
          const time = RealmAgent.getExpiration();
          const netId = await this.realm.web3.eth.net.getId();
          const permitDataToSign = getTypedData({
            name: await targetPool.syntheticToken.instance.methods
              .name()
              .call(),
            version: '1',
            chainId: await this.realm.web3.eth.net.getId(),
            verifyingContract: targetPool.syntheticToken.address.toLowerCase(),
            nonce: parseInt(
              await targetPool.syntheticToken.instance.methods
                .nonces(this.agentAddress)
                .call(),
              10,
            ),
            owner: this.agentAddress.toLowerCase(),
            spender: targetPool.address.toLowerCase(),
            value: new FPN(MAX_ALLOWANCE_IN_ETH).toString(),
            deadline: time,
          });
          console.log({ permitDataToSign });
          const permitSignature = util.signTypedData<any>(
            Buffer.from(this.privateKey.toLowerCase().slice(2), 'hex'),
            {
              data: permitDataToSign,
            },
          );

          const { r, s, v } = decodeSignature(permitSignature);

          let estimateGas = 400000;
          try {
            estimateGas = await targetPool.instance.methods
              .redeem([
                inputAmount.toString(),
                new FPN(0).toString(),
                time,
                this.agentAddress,
              ])
              .estimateGas({
                from: this.agentAddress,
              });
            // eslint-disable-next-line no-empty
          } catch (err) {}

          // 2nd transaction

          const forwardContract = await getTrustForwarder(
            this.realm.web3,
            '0x298259b647d57a8ff6ae82112656b7e30c76601b' as AddressOn<Net>,
          );
          const redeemData = await targetPool.instance.methods
            .redeem([
              inputAmount.toString(),
              new FPN(0).toString(),
              time,
              this.agentAddress,
            ])
            .encodeABI();
          const dataToSign = getForwardRequestTypedData({
            name: 'MinimalForwarder',
            version: '0.0.1',
            verifyingContract: '0x298259b647d57a8ff6ae82112656b7e30c76601b',
            chainId: netId,
            from: this.agentAddress.toString(),
            to: targetPool.address.toString(),
            value: '0',
            gas: estimateGas,
            nonce: parseInt(
              await forwardContract.instance.methods
                .getNonce(this.agentAddress)
                .call(),
              10,
            ),
            data: redeemData,
          });
          console.log(dataToSign);

          const signature = util.signTypedData<any>(
            Buffer.from(this.privateKey.slice(2), 'hex'),
            {
              data: dataToSign,
            },
          );

          txData = {
            spender: RELAYER,
            deadline: time,
            to: targetPool.syntheticToken.address.toLowerCase(),
            from: this.agentAddress,
            allowance_Value: new FPN(MAX_ALLOWANCE_IN_ETH).toString(),
            value: inputAmount.toString(),
            op_type: 'FULL_REDEEM',
            pool: targetPool.address,
            data: redeemData,
            redeemGas: estimateGas,
            signature,
            r,
            s,
            v,
          };
        }
        return txData;
      }
    } else if (side === 'atomicSwapMint') {
      // (as ex wBTC → jEUR).
      const targetPool = this.activePools[
        outputToken as SupportedSynthereumSymbol<Net>
      ];
      if (targetPool) {
        const netId = (await this.realm.web3.eth.net.getId()) as SupportedNetworkId;

        const address = '0x7130d2A12B9BCbFAe4f2634d864A1Ee1Ce3Ead9c' as AddressOn<Net>;
        const tokenInfo = await get3rdPartyTokenInfo(this.realm.web3, address);

        const inputAmount = weiToTokenAmount({
          wei: wei(inputAmountWei.toString()),
          decimals: tokenInfo.decimals,
        });
        const time = RealmAgent.getExpiration();
        const extraParams = ethers.utils.defaultAbiCoder.encode(
          ['address[]'],
          [
            [
              '0x7130d2A12B9BCbFAe4f2634d864A1Ee1Ce3Ead9c',
              '0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56',
            ],
          ],
        );

        const swapAndMintData = {
          implementationId: '0x756e695632',
          inputParams: [
            true,
            inputAmount.toString(), // exactAmount
            0, // minOutOrMaxIn
            extraParams,
            this.agentAddress,
          ],
          synthereumPool: targetPool.address,
          mintParams: [
            0, // numTokens
            0, // minCollateral
            time, // expiration
            this.agentAddress, // recipient
          ],
        };
        let estimateGas = 400000;
        const onChainLiquidityRouterContract = getGenericTokenInfo(
          this.realm.web3,
          onChainLiquidationRouter as AddressOn<Net>,
          OnChainLiquidityRouterV2_Abi,
        );
        try {
          estimateGas = await onChainLiquidityRouterContract.instance.methods
            .swapAndMint(
              swapAndMintData.implementationId,
              swapAndMintData.inputParams,
              swapAndMintData.synthereumPool,
              swapAndMintData.mintParams,
            )
            .estimateGas({
              from: this.agentAddress,
            });
          console.log(estimateGas);
          // eslint-disable-next-line no-empty
        } catch (err) {
          console.log(err, '----------------------------');
        }

        const forwardContract = await getTrustForwarder(
          this.realm.web3,
          '0x298259b647d57a8ff6ae82112656b7e30c76601b' as AddressOn<Net>,
        );
        const mintData = await onChainLiquidityRouterContract.instance.methods
          .swapAndMint(
            swapAndMintData.implementationId,
            swapAndMintData.inputParams,
            swapAndMintData.synthereumPool,
            swapAndMintData.mintParams,
          )
          .encodeABI();
        const dataToSign = getForwardRequestTypedData({
          name: 'MinimalForwarder',
          version: '0.0.1',
          verifyingContract: '0x298259b647d57a8ff6ae82112656b7e30c76601b',
          chainId: netId,
          from: this.agentAddress.toString(),
          to: onChainLiquidationRouter,
          value: '0',
          gas: estimateGas,
          nonce: parseInt(
            await forwardContract.instance.methods
              .getNonce(this.agentAddress)
              .call(),
            10,
          ),
          data: mintData,
        });

        const signature = util.signTypedData<any>(
          Buffer.from(this.privateKey.slice(2), 'hex'),
          {
            data: dataToSign,
          },
        );
        const txData = {
          spender: RELAYER,
          deadline: time,

          to: tokenInfo.address,
          from: this.agentAddress,
          allowance_Value: weiToTokenAmount({
            wei: wei(new FPN(MAX_ALLOWANCE_IN_ETH).toString()),
            decimals: tokenInfo.decimals,
          }).toString(),
          value: inputAmount.toString(),
          op_type: 'FULL_ATOMIC_SWAP_MINT',

          pool: targetPool.address,
          data: mintData,
          mintGas: estimateGas,
          onChainLiquidationRouter,
          signature,
        };
        console.log(txData);
        return txData;
      }
    } else if (side === 'atomicSwapRedeem') {
      // (as ex  jEUR → wBTC ).
      const netId = await this.realm.web3.eth.net.getId();

      const targetPool = this.activePools[
        inputToken as SupportedSynthereumSymbol<Net>
      ];
      if (targetPool) {
        const inputAmount = weiToTokenAmount({
          wei: wei(inputAmountWei.toString()),
          decimals: targetPool.syntheticToken.decimals,
        });
        const time = RealmAgent.getExpiration();
        const permitDataToSign = getTypedData({
          name: await targetPool.syntheticToken.instance.methods.name().call(),
          version: '1',
          chainId: await this.realm.web3.eth.net.getId(),
          verifyingContract: targetPool.syntheticToken.address.toLowerCase(),
          nonce: parseInt(
            await targetPool.syntheticToken.instance.methods
              .nonces(this.agentAddress)
              .call(),
            10,
          ),
          owner: this.agentAddress.toLowerCase(),
          spender: onChainLiquidationRouter,
          value: new FPN(MAX_ALLOWANCE_IN_ETH).toString(),
          deadline: time,
        });

        const permitSignature = util.signTypedData<any>(
          Buffer.from(this.privateKey.toLowerCase().slice(2), 'hex'),
          {
            data: permitDataToSign,
          },
        );

        const { r, s, v } = decodeSignature(permitSignature);
        const extraParams = ethers.utils.defaultAbiCoder.encode(
          ['address[]'],
          [
            [
              '0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56',
              '0x7130d2A12B9BCbFAe4f2634d864A1Ee1Ce3Ead9c',
            ],
          ],
        );
        const redeemAndSwapData = {
          implementationId: '0x756e695632',
          inputParams: [
            true, // isExactBool
            false, // unwrapToETH
            0, // exactAmount
            0, // minOutOrMaxIn
            extraParams,
            this.agentAddress,
          ],
          synthereumPool: targetPool.address,
          redeemParams: [
            inputAmount.toString(), // numTokens
            0, // minCollateral
            time, // expiration
            this.agentAddress, // recipient
          ],
          recipient: this.agentAddress,
        };
        let estimateGas = 500000;
        const onChainLiquidityRouterContract = getGenericTokenInfo(
          this.realm.web3,
          onChainLiquidationRouter as AddressOn<Net>,
          OnChainLiquidityRouterV2_Abi,
        );
        try {
          estimateGas = await onChainLiquidityRouterContract.instance.methods
            .redeemAndSwap(
              redeemAndSwapData.implementationId,
              redeemAndSwapData.inputParams,
              redeemAndSwapData.synthereumPool,
              redeemAndSwapData.redeemParams,
              redeemAndSwapData.recipient,
            )
            .estimateGas({
              from: this.agentAddress,
            });
          console.log(estimateGas);
          // eslint-disable-next-line no-empty
        } catch (err) {
          console.log(err, '----------------------------');
        }
        const forwardContract = await getTrustForwarder(
          this.realm.web3,
          '0x298259b647d57a8ff6ae82112656b7e30c76601b' as AddressOn<Net>,
        );
        const redeemData = await onChainLiquidityRouterContract.instance.methods
          .redeemAndSwap(
            redeemAndSwapData.implementationId,
            redeemAndSwapData.inputParams,
            redeemAndSwapData.synthereumPool,
            redeemAndSwapData.redeemParams,
            redeemAndSwapData.recipient,
          )
          .encodeABI();
        const dataToSign = getForwardRequestTypedData({
          name: 'MinimalForwarder',
          version: '0.0.1',
          verifyingContract: '0x298259b647d57a8ff6ae82112656b7e30c76601b',
          chainId: netId,
          from: this.agentAddress.toString(),
          to: onChainLiquidationRouter,
          value: '0',
          gas: estimateGas,
          nonce: parseInt(
            await forwardContract.instance.methods
              .getNonce(this.agentAddress)
              .call(),
            10,
          ),
          data: redeemData,
        });

        const signature = util.signTypedData<any>(
          Buffer.from(this.privateKey.slice(2), 'hex'),
          {
            data: dataToSign,
          },
        );
        const txData = {
          spender: RELAYER,
          deadline: time,

          to: targetPool.syntheticToken.address,
          from: this.agentAddress,
          allowance_Value: weiToTokenAmount({
            wei: wei(new FPN(MAX_ALLOWANCE_IN_ETH).toString()),
            decimals: targetPool.syntheticToken.decimals,
          }).toString(),
          value: inputAmount.toString(),
          op_type: 'FULL_ATOMIC_SWAP_REDEEM',
          data: redeemData,

          onChainLiquidationRouter,
          redeemGas: estimateGas,
          signature,
          r,
          s,
          v,
        };
        console.log(txData);
        return txData;
      }
    }
  }

  public getEvents = (_: string) =>
    new Observable<ReduxAction>(eventObserver => {
      (async () => {
        const lastBlock = await this.realm.web3.eth.getBlockNumber();
        const startBlock = lastBlock - 10000;
        console.log(startBlock);
        const blockDifference = lastBlock - startBlock;
        console.log(blockDifference, 'blockDifference');
        // const numberOfStepsNormal = 80;
        // const numberOfStepsCollateral = 80;
        const stepNormal = 2000;
        const stepCollateral = 2000;

        const eventTypes = ['Mint', 'Redeem', 'Exchange', 'TransferSynth'];
        const allPools = Object.keys(this.activePools);
        const cases = cartesian(
          allPools,
          eventTypes,
          [...ascendingRange(startBlock, lastBlock, stepNormal)].map(start => [
            start,
            start + stepNormal,
          ]),
        );
        cases.push(
          ...cartesian(
            allPools.slice(0, 1),
            ['TransferCollateral'],
            [
              ...ascendingRange(startBlock, lastBlock, stepCollateral),
            ].map(start => [start, start + stepCollateral]),
          ),
        );
        cases.push(
          ...cartesian(
            ['WBTC'],
            ['TransferExtra'],
            [
              ...ascendingRange(startBlock, lastBlock, stepCollateral),
            ].map(start => [start, start + stepCollateral]),
          ),
        );
        const allEvents: any = [];
        const perChunk = cases.length / 2;
        const casesChunk = cases.reduce(
          (resultArray: any[][], item: any, index: number) => {
            const chunkIndex = Math.floor(index / perChunk);

            if (!resultArray[chunkIndex]) {
              resultArray[chunkIndex] = []; // start a new chunk
            }

            resultArray[chunkIndex].push(item);

            return resultArray;
          },
          [],
        );

        await Promise.all(
          casesChunk.map(async (chunk: any) => {
            for (const c of chunk) {
              try {
                const [synthSymbol, event, fromBlock, toBlock]: [
                  SupportedSynthereumSymbol<Net>,
                  string,
                  number,
                  number,
                ] = c;
                const events = [];
                const pool = this.activePools[synthSymbol];
                if (!event.startsWith('Transfer')) {
                  const eventsArray = await pool!.instance.getPastEvents(
                    event,
                    {
                      fromBlock,
                      toBlock,
                      // filter: {
                      //   account: relayAddress || RELAYER,
                      // },
                    },
                  );
                  events.push(
                    ...eventsArray.map((e: any) => ({
                      ...e,
                      info: {
                        symbol: pool?.symbol,
                        syntheticToken: pool?.syntheticToken.symbol,
                        collateralToken: pool?.collateralToken.symbol,
                        syntheticTokenDecimals: pool?.syntheticToken.decimals,
                        collateralTokenDecimals: pool?.collateralToken.decimals,
                      },
                    })),
                  );
                } else if (event === 'TransferSynth') {
                  const eventsArray = await this.fetchErc20Transfer(
                    pool?.syntheticToken.instance,
                    fromBlock,
                    toBlock,
                    this.agentAddress,
                  );
                  events.push(
                    ...eventsArray.map((e: any) => ({
                      ...e,
                      info: {
                        symbol: pool?.symbol,
                        syntheticToken: pool?.syntheticToken.symbol,
                        syntheticTokenDecimals: pool?.syntheticToken.decimals,
                      },
                    })),
                  );
                } else if (event === 'TransferCollateral') {
                  const eventsArray = await this.fetchErc20Transfer(
                    pool!.collateralToken.instance,
                    fromBlock,
                    toBlock,
                    this.agentAddress,
                  );
                  events.push(
                    ...eventsArray.map((e: any) => ({
                      ...e,
                      info: {
                        symbol: pool?.collateralToken.symbol,
                        collateralToken: pool?.collateralToken.symbol,
                        collateralTokenDecimals: pool?.collateralToken.decimals,
                      },
                    })),
                  );
                } else if (event === 'TransferExtra') {
                  const netId = (await this.realm.web3.eth.net.getId()) as SupportedNetworkId;
                  const extraToken = Object.values(burnerWalletConfig).filter(
                    token =>
                      token?.symbol ===
                      (synthSymbol as BurnerWalletExtraTokens),
                  )[0];
                  const address = extraToken.network[netId] as AddressOn<Net>;
                  const tokenInfo = await get3rdPartyTokenInfo(
                    this.realm.web3,
                    address,
                  );
                  const eventsArray = await this.fetchErc20Transfer(
                    tokenInfo.instance,
                    fromBlock,
                    toBlock,
                    this.agentAddress,
                  );
                  events.push(
                    ...eventsArray.map((e: any) => ({
                      ...e,
                      info: {
                        symbol: 'BTCBUSD',
                        collateralToken: 'BTCBUSD',
                        collateralTokenDecimals: tokenInfo?.decimals,
                      },
                    })),
                  );
                }

                allEvents.push(...events);

                const filterTransactions = await Promise.all(
                  allEvents
                    .filter(
                      (x: any) =>
                        (x.returnValues.to &&
                          x.returnValues.to.toLowerCase() ===
                            this.agentAddress.toLowerCase()) ||
                        (x.returnValues.from &&
                          x.returnValues.from.toLowerCase() ===
                            this.agentAddress.toLowerCase()) ||
                        (x.returnValues.recipient &&
                          x.returnValues.recipient.toLowerCase() ===
                            this.agentAddress.toLowerCase()),
                    )
                    .map(async (e: any) => {
                      const destPoolInfo = e.returnValues.destPool
                        ? Object.values(this.activePools)
                            .filter(
                              (x: any) => x.address === e.returnValues.destPool,
                            )
                            .map((synthereumPool: any) => ({
                              symbol: synthereumPool.symbol,
                              syntheticToken:
                                synthereumPool.syntheticToken.symbol,
                              collateralToken:
                                synthereumPool.collateralToken.symbol,
                              syntheticTokenDecimals:
                                synthereumPool.syntheticToken.decimals,
                              collateralTokenDecimals:
                                synthereumPool.collateralToken.decimals,
                            }))[0]
                        : {};

                      const blockData = await this.realm.web3.eth.getBlock(
                        e.blockNumber,
                      );
                      return {
                        type: e.event,
                        timeStamp: blockData.timestamp,
                        txHash: e.transactionHash,
                        rv:
                          e.event === 'Redeem'
                            ? {
                                account: e.returnValues.account,
                                feePaid: e.returnValues.feePaid,
                                pool: e.returnValues.pool,
                                recipient: e.returnValues.recipient,
                                collateralReceived: scaleTokenAmountToWei({
                                  amount: new BN(
                                    e.returnValues.collateralReceived,
                                  ),
                                  decimals: e.info.collateralTokenDecimals,
                                }).toString(),
                                numTokensSent: scaleTokenAmountToWei({
                                  amount: new BN(e.returnValues.numTokensSent),
                                  decimals: e.info.syntheticTokenDecimals,
                                }).toString(),
                                ...e.info,
                              }
                            : e.event === 'Mint'
                            ? {
                                account: e.returnValues.account,
                                collateralSent: scaleTokenAmountToWei({
                                  amount: new BN(e.returnValues.collateralSent),
                                  decimals: e.info.collateralTokenDecimals,
                                }).toString(),
                                feePaid: e.returnValues.feePaid,
                                numTokensReceived: scaleTokenAmountToWei({
                                  amount: new BN(
                                    e.returnValues.numTokensReceived,
                                  ),
                                  decimals: e.info.syntheticTokenDecimals,
                                }).toString(),
                                pool: e.returnValues.pool,
                                recipient: e.returnValues.recipient,
                                ...e.info,
                              }
                            : e.event === 'Exchange'
                            ? {
                                account: e.returnValues.account,

                                destNumTokensReceived: scaleTokenAmountToWei({
                                  amount: new BN(
                                    e.returnValues.destNumTokensReceived,
                                  ),
                                  decimals: e.info.syntheticTokenDecimals,
                                }).toString(),
                                destPool: e.returnValues.destPool,
                                feePaid: e.returnValues.feePaid,
                                numTokensSent: scaleTokenAmountToWei({
                                  amount: new BN(e.returnValues.numTokensSent),
                                  decimals: e.info.syntheticTokenDecimals,
                                }).toString(),
                                recipient: e.returnValues.recipient,
                                sourcePool: e.returnValues.sourcePool,
                                ...e.info,
                                destPoolInfo,
                              }
                            : {
                                from: e.returnValues.from,
                                to: e.returnValues.to,
                                value: e.info.syntheticToken
                                  ? scaleTokenAmountToWei({
                                      amount: new BN(e.returnValues.value),
                                      decimals: e.info.syntheticTokenDecimals,
                                    }).toString()
                                  : scaleTokenAmountToWei({
                                      amount: new BN(e.returnValues.value),
                                      decimals: e.info.collateralTokenDecimals,
                                    }).toString(),
                                ...e.info,
                              },

                        address: e.address,
                        blockNumber: e.blockNumber,
                      };
                    }),
                );
                eventObserver.next({
                  type: `transactions/setHistory`,
                  payload: filterTransactions.sort(
                    (a: any, b: any) => b.blockNumber - a.blockNumber,
                  ),
                });
              } catch (error) {
                console.log(c);
                console.log(error, 'Unable to load');
              }
            }
          }),
        );
        eventObserver.next({
          type: 'transactions/loaded',
          payload: true,
        });
        eventObserver.complete();
      })();
    });

  private async fetchErc20Transfer(
    erc20: ERC20Permit | any,
    fromBlock: number,
    toBlock: number,
    agentAddress: string,
  ): Promise<any> {
    const fromEv = await erc20.getPastEvents('Transfer', {
      fromBlock,
      toBlock,
      filter: { from: agentAddress },
    });
    const toEv = await erc20.getPastEvents('Transfer', {
      fromBlock,
      toBlock,
      filter: { to: agentAddress },
    });
    return fromEv.concat(toEv);
  }

  public async sendSynthetic({
    inputToken,
    inputAmountWei,
    recipientAddress,
  }: {
    inputToken: ExchangeSynthereumToken;
    inputAmountWei: Amount;
    recipientAddress: AddressOn<Net>;
    txOptions?: TxOptions;
  }) {
    const targetPool = this.activePools[
      inputToken as SupportedSynthereumSymbol<Net>
    ];
    if (!targetPool) {
      return;
    }
    const time = RealmAgent.getExpiration();
    const permitDataToSign = getTypedData({
      name: await targetPool.syntheticToken.instance.methods.name().call(),
      version: '1',
      chainId: await this.realm.web3.eth.net.getId(),
      verifyingContract: targetPool.syntheticToken.address.toLowerCase(),
      nonce: parseInt(
        await targetPool.syntheticToken.instance.methods
          .nonces(this.agentAddress)
          .call(),
        10,
      ),
      owner: this.agentAddress.toLowerCase(),
      spender: RELAYER,
      value: new FPN(MAX_ALLOWANCE_IN_ETH).toString(),
      deadline: time,
    });

    const permitSignature = util.signTypedData<any>(
      Buffer.from(this.privateKey.slice(2), 'hex'),
      {
        data: permitDataToSign,
      },
    );
    const { r, s, v } = decodeSignature(permitSignature);
    const txData = {
      spender: RELAYER,
      deadline: time,
      to: targetPool.syntheticToken.address.toLowerCase(),
      from: this.agentAddress,
      allowance_Value: new FPN(MAX_ALLOWANCE_IN_ETH).toString(),
      value: inputAmountWei.toString(),
      op_type: 'FULL_SEND',
      pool: targetPool.address,
      data: permitDataToSign,
      recipient: recipientAddress,
      r,
      s,
      v,
    };
    return txData;
  }

  // Only for USD

  public async sendUsdc({
    inputToken,
    inputAmountWei,
    recipientAddress,
  }: {
    inputToken: ExchangeSynthereumToken;
    inputAmountWei: Amount;
    recipientAddress: AddressOn<Net>;
    txOptions?: TxOptions;
  }) {
    const targetPool = this.activePools[
      inputToken !== 'USDC'
        ? (inputToken as SupportedSynthereumSymbol<Net>)
        : ('jEUR' as SupportedSynthereumSymbol<Net>)
    ]!;

    const inputAmount = this.isCollateral(inputToken)
      ? weiToTokenAmount({
          wei: wei(inputAmountWei.toString()),
          decimals: targetPool.collateralToken.decimals,
        })
      : inputAmountWei;

    let txData: any = {};
    if (inputToken === 'USDC') {
      const approveABI = targetPool.collateralToken.instance.methods
        .approve(
          RELAYER,
          weiToTokenAmount({
            wei: wei(new FPN(MAX_ALLOWANCE_IN_ETH).toString()),
            decimals: targetPool.collateralToken.decimals,
          }).toString(),
        )
        .encodeABI();

      const netId = await this.realm.web3.eth.net.getId();
      const time = RealmAgent.getExpiration();
      const dataToSign = getMetaTransactionTypedData({
        name: await targetPool.collateralToken.instance.methods.name().call(),
        version: '1',
        salt: `0x${this.realm.web3.utils.padLeft(netId.toString(16), 64)}`,
        verifyingContract: targetPool.syntheticToken.address.toLowerCase(),
        nonce: parseInt(
          await targetPool.collateralToken.instance.methods
            .nonces(this.agentAddress)
            .call(),
          10,
        ),
        from: this.agentAddress.toLowerCase(),
        functionSignature: approveABI,
      });

      const signature = util.signTypedData<any>(
        Buffer.from(this.privateKey.slice(2), 'hex'),
        {
          data: dataToSign,
        },
      );
      const { r, s, v } = decodeSignature(signature);

      txData = {
        spender: RELAYER,
        deadline: time,
        derivative: targetPool.derivative.address,
        to: targetPool.collateralToken.address,
        from: this.agentAddress,
        allowance_Value: weiToTokenAmount({
          wei: wei(new FPN(MAX_ALLOWANCE_IN_ETH).toString()),
          decimals: targetPool.collateralToken.decimals,
        }).toString(),
        value: inputAmount.toString(),
        op_type: 'USDC_FULL_SEND',
        functionSignature: approveABI,
        recipient: recipientAddress,
        r,
        s,
        v,
      };
    } else {
      const time = RealmAgent.getExpiration();
      const dataToSign = getTypedData({
        name: await targetPool.syntheticToken.instance.methods.name().call(),
        version: '1',
        chainId: await this.realm.web3.eth.net.getId(),
        verifyingContract: targetPool.syntheticToken.address.toLowerCase(),
        nonce: parseInt(
          await targetPool.syntheticToken.instance.methods
            .nonces(this.agentAddress)
            .call(),
          10,
        ),
        owner: this.agentAddress.toLowerCase(),
        spender: RELAYER,
        value: new FPN(MAX_ALLOWANCE_IN_ETH).toString(),
        deadline: time,
      });

      const signature = util.signTypedData<any>(
        Buffer.from(this.privateKey.toLowerCase().slice(2), 'hex'),
        {
          data: dataToSign,
        },
      );
      const { r, s, v } = decodeSignature(signature);

      txData = {
        spender: RELAYER,
        deadline: time,
        derivative: targetPool.derivative.address,
        to: targetPool.syntheticToken.address,
        from: this.agentAddress,
        allowance_Value: weiToTokenAmount({
          wei: wei(new FPN(MAX_ALLOWANCE_IN_ETH).toString()),
          decimals: targetPool.syntheticToken.decimals,
        }).toString(),
        value: inputAmount.toString(),
        op_type: 'FULL_SEND',
        recipient: recipientAddress,
        r,
        s,
        v,
      };
    }
    return txData;
  }

  // Only for USD

  public async sendBTC({
    inputToken,
    inputAmountWei,
    recipientAddress,
  }: {
    inputToken: ExchangeSynthereumToken;
    outputToken: ExchangeSynthereumToken;
    inputAmountWei: Amount;
    recipientAddress: AddressOn<Net>;
    txOptions?: TxOptions;
  }) {
    // (as ex wBTC → jEUR).

    const netId = (await this.realm.web3.eth.net.getId()) as SupportedNetworkId;
    const extraToken = Object.values(burnerWalletConfig).filter(
      token => token?.symbol === (inputToken as BurnerWalletExtraTokens),
    )[0];
    const address = extraToken.network[netId] as AddressOn<Net>;
    const tokenInfo = await get3rdPartyTokenInfo(this.realm.web3, address);
    const tokeName = await tokenInfo.instance.methods.name().call();

    const inputAmount = weiToTokenAmount({
      wei: wei(inputAmountWei.toString()),
      decimals: tokenInfo.decimals,
    });

    let txData: any = {};
    const approveABI = tokenInfo.instance.methods
      .approve(
        RELAYER,
        weiToTokenAmount({
          wei: wei(new FPN(MAX_ALLOWANCE_IN_ETH).toString()),
          decimals: tokenInfo.decimals,
        }).toString(),
      )
      .encodeABI();

    const time = RealmAgent.getExpiration();
    const dataToSign = getMetaTransactionTypedData({
      name: tokeName,
      version: '1',
      salt: `0x${this.realm.web3.utils.padLeft(netId.toString(16), 64)}`,
      verifyingContract: tokenInfo.address.toLowerCase(),
      nonce: parseInt(
        await tokenInfo.instance.methods.getNonce(this.agentAddress).call(),
        10,
      ),
      from: this.agentAddress.toLowerCase(),
      functionSignature: approveABI,
    });

    const signature = util.signTypedData<any>(
      Buffer.from(this.privateKey.slice(2), 'hex'),
      {
        data: dataToSign,
      },
    );
    const { r, s, v } = decodeSignature(signature);

    txData = {
      spender: RELAYER,
      deadline: time,
      to: tokenInfo.address,
      from: this.agentAddress,
      allowance_Value: weiToTokenAmount({
        wei: wei(new FPN(MAX_ALLOWANCE_IN_ETH).toString()),
        decimals: tokenInfo.decimals,
      }).toString(),
      value: inputAmount.toString(),
      op_type: 'BTC_FULL_SEND',
      functionSignature: approveABI,
      recipient: recipientAddress,
      r,
      s,
      v,
    };

    return txData;
  }

  public async sendBTCB({
    inputToken,
    inputAmountWei,
    recipientAddress,
  }: {
    inputToken: ExchangeSynthereumToken;
    outputToken: ExchangeSynthereumToken;
    inputAmountWei: Amount;
    recipientAddress: AddressOn<Net>;
    txOptions?: TxOptions;
  }) {
    // (as ex wBTC → jEUR).
    await this.ApproveBUSD({ inputToken, outputToken: undefined }, RELAYER);
    const netId = (await this.realm.web3.eth.net.getId()) as SupportedNetworkId;
    const address = burnerWalletConfig.WBTC.network[netId];

    const tokenInfo = await getTokenInfo(this.realm.web3, address as any);

    const inputAmount = weiToTokenAmount({
      wei: wei(inputAmountWei.toString()),
      decimals: tokenInfo.decimals,
    });
    // const txEstimageGas = await tokenInfo.instance.methods
    //   .transferFrom(this.agentAddress, recipientAddress, inputAmount.toString())
    //   .estimateGas({
    //     from: this.agentAddress,
    //   });

    // const tx = await tokenInfo.instance.methods
    //   .transferFrom(this.agentAddress, recipientAddress, inputAmount.toString())
    //   .send({
    //     from: this.agentAddress,
    //     gas: txEstimageGas,
    //   });

    const txData = {
      spender: RELAYER,

      to: tokenInfo.address,
      from: this.agentAddress,
      value: inputAmount.toString(),
      op_type: 'BTCB_FULL_SEND',
      recipient: recipientAddress,
    };

    return txData;
  }

  public async ApproveBUSD(
    {
      outputToken,
      inputToken,
    }: {
      outputToken: ExchangeSynthereumToken | undefined;
      inputToken: 'BTCB' | ExchangeSynthereumToken;
    },
    spender = onChainLiquidationRouter,
  ) {
    console.log(inputToken);
    if (inputToken === 'BTCB') {
      const netId = (await this.realm.web3.eth.net.getId()) as SupportedNetworkId;
      const address = burnerWalletConfig.WBTC.network[netId];

      const tokenInfo = await getTokenInfo(this.realm.web3, address as any);
      const currentAllowance = FPN.fromWei(
        await tokenInfo.instance.methods
          .allowance(this.agentAddress, spender)
          .call(),
      );
      if (currentAllowance.lt(new FPN(1))) {
        const txEstimageGas = await tokenInfo.instance.methods
          .approve(spender, new FPN(MAX_ALLOWANCE_IN_ETH).toString())
          .estimateGas({
            from: this.agentAddress,
          });
        const tx = await tokenInfo.instance.methods
          .approve(spender, new FPN(MAX_ALLOWANCE_IN_ETH).toString())
          .send({
            from: this.agentAddress,
            gas: txEstimageGas,
          });
        console.log(tx);
        return tx;
      }
      console.log('Already have allowance');
      return undefined;
    }
    if (!outputToken) {
      return undefined;
    }
    const targetPool = this.activePools[
      outputToken as SupportedSynthereumSymbol<Net>
    ];
    if (targetPool) {
      const txEstimageGas = await targetPool.collateralToken.instance.methods
        .approve(targetPool.address, new FPN(MAX_ALLOWANCE_IN_ETH).toString())
        .estimateGas({
          from: this.agentAddress,
        });

      const tx = await targetPool.collateralToken.instance.methods
        .approve(targetPool.address, new FPN(MAX_ALLOWANCE_IN_ETH).toString())
        .send({
          from: this.agentAddress,
          gas: txEstimageGas,
        });
      return tx;
    }

    return undefined;
  }
}
