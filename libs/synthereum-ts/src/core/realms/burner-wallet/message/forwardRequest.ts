export interface MetaTransactionMessage {
  name: string;
  version: string;
  verifyingContract: string;
  nonce: number;
  from: string;
  data: string;
  gas: number;
  value: string;
  to: string;
  chainId: number;
}
export const getForwardRequestTypedData = ({
  name,
  version,
  chainId,
  verifyingContract,
  nonce,
  from,
  to,
  value,
  gas,
  data,
}: MetaTransactionMessage) => ({
  types: {
    EIP712Domain: [
      {
        name: 'name',
        type: 'string',
      },
      {
        name: 'version',
        type: 'string',
      },
      {
        name: 'chainId',
        type: 'uint256',
      },
      {
        name: 'verifyingContract',
        type: 'address',
      },
    ],
    ForwardRequest: [
      {
        name: 'from',
        type: 'address',
      },
      {
        name: 'to',
        type: 'address',
      },

      {
        name: 'value',
        type: 'uint256',
      },
      {
        name: 'gas',
        type: 'uint256',
      },
      {
        name: 'nonce',
        type: 'uint256',
      },
      {
        name: 'data',
        type: 'bytes',
      },
    ],
  },
  primaryType: 'ForwardRequest',
  domain: {
    name,
    version,
    chainId,
    verifyingContract,
  },
  message: {
    from,
    to,
    value,
    gas,
    nonce,
    data,
  },
});
