export interface IEIP712Domain {
  name: string;
  version: string;
  chainId: number;
  verifyingContract: string;
}
export interface PermitMessage extends IEIP712Domain {
  nonce: number;
  owner: string;
  spender: string;
  value: string;
  deadline: number;
}
export const getTypedData = ({
  name,
  version,
  chainId,
  verifyingContract,
  nonce,
  owner,
  spender,
  value,
  deadline,
}: PermitMessage) => ({
  types: {
    EIP712Domain: [
      {
        name: 'name',
        type: 'string',
      },
      {
        name: 'version',
        type: 'string',
      },
      {
        name: 'chainId',
        type: 'uint256',
      },
      {
        name: 'verifyingContract',
        type: 'address',
      },
    ],
    Permit: [
      {
        name: 'owner',
        type: 'address',
      },
      {
        name: 'spender',
        type: 'address',
      },

      {
        name: 'value',
        type: 'uint256',
      },
      {
        name: 'nonce',
        type: 'uint256',
      },
      {
        name: 'deadline',
        type: 'uint256',
      },
    ],
  },
  primaryType: 'Permit',
  domain: {
    name,
    version,
    chainId,
    verifyingContract,
  },
  message: {
    owner,
    spender,
    value,
    nonce,
    deadline,
  },
});
