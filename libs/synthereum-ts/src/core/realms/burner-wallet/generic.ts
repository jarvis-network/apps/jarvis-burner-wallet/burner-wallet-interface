import { ether } from '@jarvis-network/core-utils/dist/base/big-number';
import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';
import { Observable } from 'rxjs';

import { Context, ReduxAction } from '../../../epics/types';

import { ContractParams } from './interface';

export const formatUSDValue = (assetOutPrice: string, price: string) =>
  FPN.fromWei(assetOutPrice)
    .mul(FPN.toWei(price))
    .div(FPN.fromWei(ether))
    .format(2);

export const genericTx = (
  context: Context,
  opType: 'approve' | 'mint' | 'exchange' | 'send',
  { ...payload }: ContractParams,
) =>
  new Observable<ReduxAction>(observer => {
    (async () => {
      try {
        if (opType === 'send') {
          if (payload.inputToken === 'USDC') {
            const postData = await context.burnerRealmAgent?.sendUsdc(
              payload as any,
            );
            observer.next({
              type: `transactions/setPayload`,
              payload: postData,
            });
          } else if (payload.inputToken === 'WBTC') {
            const postData = await context.burnerRealmAgent?.sendBTC(
              payload as any,
            );
            observer.next({
              type: `transactions/setPayload`,
              payload: postData,
            });
          } else if (payload.inputToken === 'BTCB') {
            const postData = await context.burnerRealmAgent?.sendBTCB(
              payload as any,
            );
            observer.next({
              type: `transactions/setPayload`,
              payload: postData,
            });
          }
          const postData = await context.burnerRealmAgent?.sendSynthetic(
            payload as any,
          );
          observer.next({
            type: `transactions/setPayload`,
            payload: postData,
          });
          return;
        }
        if (opType === 'approve') {
          console.log('Submitting approve tx');
          const tx = await context.burnerRealmAgent?.ApproveBUSD(payload);
          if (tx) {
            const { blockHash, blockNumber, transactionHash } = tx;
            observer.next({
              type: `transactions/approveTx`,
              payload: {
                blockHash,
                blockNumber,
                transactionHash,
              },
            });
          }

          return;
        }
        const postData = await context.burnerRealmAgent?.universalExchange(
          payload as any,
        );
        observer.next({
          type: `transactions/setPayload`,
          payload: postData,
        });
      } catch (error: any) {
        console.log(error);
        observer.next({
          type: `transactions/metaMaskError`,
          payload: {
            code: error.code,
            message: error.message,
            data: error.data,
          },
        });
      }
      observer.complete();
    })();
  });
