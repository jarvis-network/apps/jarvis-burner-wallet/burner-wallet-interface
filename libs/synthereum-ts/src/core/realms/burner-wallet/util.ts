/* eslint-disable @typescript-eslint/no-explicit-any */
export const cartesian = (...arrayOfArrays: any): any =>
  arrayOfArrays.reduce((a: any[], b: any[]) =>
    a.flatMap((d: any) => b.map((e: any) => [d, e].flat())),
  );
