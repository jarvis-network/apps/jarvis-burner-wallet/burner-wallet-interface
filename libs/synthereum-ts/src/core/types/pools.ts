import { assertIncludes } from '@jarvis-network/core-utils/dist/base/asserts';
import {
  ContractInfo,
  ContractInstance,
} from '@jarvis-network/core-utils/dist/eth/contracts/types';
import {
  NetworkName,
  ToNetworkId,
} from '@jarvis-network/core-utils/dist/eth/networks';
import type {
  SupportedNetworkName,
  SupportedSynthereumSymbol,
} from '@jarvis-network/synthereum-config';
import { SynthereumLiquidityPool as SynthereumLiquidityPoolContract } from '@jarvisnetwork/synthereum-contracts/build/typechain';
import { ERC20Permit } from '@jarvisnetwork/synthereum-contracts/build/typechain/ERC20Permit';
import { SynthereumTrustedForwarder } from '@jarvisnetwork/synthereum-contracts/build/typechain/SynthereumTrustedForwarder';
import { UChildERC20 } from '@jarvisnetwork/synthereum-contracts/build/typechain/UChildERC20';
import { BaseContract } from 'libs/core-utils/dist/eth/contracts/typechain/types';

export interface TokenInstancePermit<
  Net extends NetworkName,
  TokenSymbol extends string = string
> extends ContractInstance<Net, ERC20Permit> {
  symbol: TokenSymbol;
  decimals: number;
}

export interface TokenInstanceUChildERC20<
  Net extends NetworkName,
  TokenSymbol extends string = string
> extends ContractInstance<Net, UChildERC20> {
  symbol: TokenSymbol;
  decimals: number;
}

export interface TokenInstanceTrustForwarder<
  Net extends NetworkName,
  TokenSymbol extends string = string
> extends ContractInstance<Net, SynthereumTrustedForwarder> {
  symbol: TokenSymbol;
  decimals: number;
}

export const poolVersions = ['v5'] as const;
export type PoolVersions = typeof poolVersions;
export type PoolVersion = PoolVersions[number];

export function assertIsSupportedPoolVersion(x: unknown): PoolVersion {
  return assertIncludes(
    poolVersions,
    x,
    `'${x}' is not a supported pool version`,
  );
}

export type PoolContract<Version extends PoolVersion> = Version extends 'v5'
  ? SynthereumLiquidityPoolContract
  : never;

export interface SynthereumPool<
  Version extends PoolVersion,
  Net extends SupportedNetworkName = SupportedNetworkName,
  SynthSymbol extends SupportedSynthereumSymbol<Net> = SupportedSynthereumSymbol<Net>
> extends ContractInstance<Net, PoolContract<Version>> {
  networkId: ToNetworkId<Net>;
  versionId: Version;
  symbol: SynthSymbol;

  collateralToken: TokenInstancePermit<Net>;
  syntheticToken: TokenInstancePermit<Net>;
  derivative: ContractInfo<Net, BaseContract>;
}

export type PoolsForVersion<
  Version extends PoolVersion,
  Net extends SupportedNetworkName
> = {
  [SynthSymbol in SupportedSynthereumSymbol<Net>]?: SynthereumPool<
    Version,
    Net,
    SynthSymbol
  >;
};
