import { ContractInstance } from '@jarvis-network/core-utils/dist/eth/contracts/types';
import { ToNetworkId } from '@jarvis-network/core-utils/dist/eth/networks';
import { Web3On } from '@jarvis-network/core-utils/dist/eth/web3-instance';
import { ISynthereumRegistry as ISynthereumRegistryContract } from '@jarvisnetwork/synthereum-contracts/build/typechain/ISynthereumRegistry';

import { SupportedNetworkName } from '../../config';

import { PoolsForVersion, PoolVersion } from './pools';

/**
 * Describes a specifc deployment of all Synthereum contracts on a given network.
 *
 * This is the central point for interaction with all parts of the deployment.
 */
export interface SynthereumRealm<
  Net extends SupportedNetworkName = SupportedNetworkName
> {
  readonly poolRegistry: ContractInstance<Net, ISynthereumRegistryContract>;
  readonly pools: {
    [Version in PoolVersion]?: PoolsForVersion<Version, Net>;
  };
}

/**
 * Synthereum realm with an associated Web3 instance.
 */
export interface SynthereumRealmWithWeb3<
  Net extends SupportedNetworkName = SupportedNetworkName
> extends SynthereumRealm<Net> {
  readonly web3: Web3On<Net>;
  readonly netId: ToNetworkId<Net>;
}
