import { StringAmount } from '@jarvis-network/core-utils/dist/base/big-number';
import { AddressOn } from '@jarvis-network/core-utils/dist/eth/address';
import { getTokenBalance } from '@jarvis-network/core-utils/dist/eth/contracts/erc20';
import { SupportedNetworkId } from '@jarvis-network/synthereum-config';
import { burnerWalletConfig } from '@jarvis-network/synthereum-config/dist/data';
import { ExchangeSynthereumToken } from '@jarvis-network/synthereum-config/dist/supported/all-synthereum-pairs';
import {
  distinctUntilChanged,
  filter,
  map,
  mapTo,
  switchMap,
  switchMapTo,
  takeUntil,
} from 'rxjs';

import { getTokenInfo } from '../core/realms/common-realm';

import { MinimalState } from './core-burner';
import { interval$ } from './price-feed';
import { Dependencies, Epic, ReduxAction } from './types';

export interface WalletBalance {
  asset: ExchangeSynthereumToken;
  amount: StringAmount;
  nonce: number;
}

export type OutputAction = ReduxAction<
  'wallet/setWalletBalances',
  WalletBalance[]
>;

type SelectedWalletBalance = ReduxAction<
  'GET_WALLET_BALANCE',
  ExchangeSynthereumToken[]
>;
export type InputAction = SelectedWalletBalance | OutputAction;
export interface TokenBalance {
  amount: StringAmount;
  asset: string;
  nonce: number;
  name: string;
  decimals: number;
}
export const walletBurnerEpic: Epic<ReduxAction, ReduxAction, MinimalState> = (
  action$,
  state$,
  { context$ }: Dependencies,
) =>
  action$.pipe(
    filter(action => action.type === 'GET_WALLET_BALANCE'),
    switchMapTo(state$.pipe(map(state => state.assets.list))),
    distinctUntilChanged(),
    switchMap(symbols => interval$.pipe(mapTo(symbols))),
    switchMap(symbols =>
      context$!.pipe(
        map(context => ({ context, symbols })),
        takeUntil(
          action$.pipe(
            filter(
              a => a.type === 'networkSwitch' || a.type === 'addressSwitch',
            ),
          ),
        ),
      ),
    ),
    switchMap(async ({ context }) => {
      try {
        const balances = await context.burnerRealmAgent?.getAllBalances(); // eslint-disable-line
        const extraTokens = Object.values(burnerWalletConfig);

        const netId = (await context.web3?.eth.net.getId()) as SupportedNetworkId;
        const agentAddress = context.burnerRealmAgent
          ?.agentAddress as AddressOn<'mainnet'>;
        const extraTokenArrays = await Promise.all(
          extraTokens.map(async token => {
            const address = token.network[netId];

            const tokenInfo = await getTokenInfo(
              context.burnerRealmAgent!.realm.web3,
              address,
            );
            const balance = await getTokenBalance(tokenInfo, agentAddress);
            // #TODO: Filx me tokenInfo.symbol
            return [tokenInfo.symbol, balance];
          }),
        );
        balances?.push(...(extraTokenArrays as any));

        return balances;
      } catch (error) {
        return undefined;
      }
    }),

    map(results => ({
      type: 'wallet/setWalletBalances',
      payload:
        (results?.map(([asset, amount]) => ({
          asset,
          amount: amount.toString() as StringAmount,
        })) as WalletBalance[]) ?? [],
    })),
  );
