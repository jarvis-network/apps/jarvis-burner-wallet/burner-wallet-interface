import { AddressOn } from '@jarvis-network/core-utils/dist/eth/address';
import { Web3On } from '@jarvis-network/core-utils/dist/eth/web3-instance';
import {
  SupportedNetworkId,
  SupportedNetworkName,
} from '@jarvis-network/synthereum-config';
import { distinctUntilChanged, filter, switchMap, take } from 'rxjs';
import Web3 from 'web3';

import { loadRealm } from '../core/realms/burner-wallet/load';
import { RealmAgent } from '../core/realms/burner-wallet/realm-agent';
import { ChainLinkPriceFeed } from '../price-feed/chainlink';

import { Assets } from './assets-burner';
import { PriceFeedSymbols } from './price-feed';
import { Context, Dependencies, Epic, ReduxAction } from './types';

export const createBurnerContext = async (
  priceFeedWeb3: Web3On<SupportedNetworkId>,
  web3: Web3On<SupportedNetworkId>,
  privateKey: string,
): Promise<Context> => {
  const priceFeedNetId = (await priceFeedWeb3.eth.net.getId()) as SupportedNetworkId;
  const netId = (await web3.eth.net.getId()) as SupportedNetworkId;
  const v = 'v5';
  const realm = await loadRealm(web3, netId, { [v]: null });
  const pools = Object.values(realm.pools.v5!).map(
    p => `${p?.syntheticToken.symbol}/${p?.collateralToken.symbol}`,
  );

  const chainLinkPriceFeed = new ChainLinkPriceFeed({
    netId: priceFeedNetId,
    web3: priceFeedWeb3,
    symbols: [...pools, 'WBTC/BUSD', 'BUSD'] as PriceFeedSymbols[],
  });
  chainLinkPriceFeed.init();
  const agentAddress = web3.eth.defaultAccount!.toLowerCase() as AddressOn<SupportedNetworkName>;
  const realmAgent = new RealmAgent(realm, agentAddress, privateKey, 'v5');

  return {
    web3,
    priceFeedWeb3,
    networkId: netId,
    chainLinkPriceFeed,
    burnerRealmAgent: realmAgent,
  };
};
export type MinimalState = {
  app: {
    privateKey: string;
  };
  assets: {
    list: Assets;
  };
};
export const realmBurnerEpic: Epic<ReduxAction, ReduxAction, MinimalState> = (
  action$,
  state$,
  { context$ }: Dependencies,
) =>
  action$.pipe(
    filter(action => action.type === 'INIT_CORE_CONTEXT'),
    distinctUntilChanged(),
    switchMap(action =>
      context$!.pipe(
        distinctUntilChanged(),
        take(1),
        switchMap(async (_: Context) => {
          const priceFeedWeb3 = new Web3(action.payload.priceFeedNode);

          const mainFeedWeb3 = new Web3(action.payload.appNode);

          const priceFeedNetId = (await priceFeedWeb3.eth.net.getId()) as SupportedNetworkId;
          const netId = (await mainFeedWeb3.eth.net.getId()) as SupportedNetworkId;

          const account = await mainFeedWeb3.eth.accounts.wallet.add(
            action.payload.privateKey,
          );
          mainFeedWeb3.eth.defaultAccount = account.address;

          context$!.next(
            await createBurnerContext(
              priceFeedWeb3 as Web3On<typeof priceFeedNetId>,
              mainFeedWeb3 as Web3On<typeof netId>,
              action.payload.privateKey,
            ),
          );
          return {
            type: 'app/contextUpdate',
            payload: {
              networkId: netId,
              address: account.address.toLowerCase(),
            },
          };
        }),
      ),
    ),
  );
