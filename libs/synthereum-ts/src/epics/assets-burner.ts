import { StringAmount } from '@jarvis-network/core-utils/dist/base/big-number';
import { Empty } from '@jarvis-network/core-utils/dist/base/optional';
import { SupportedNetworkId } from '@jarvis-network/synthereum-config';
import { burnerWalletConfig } from '@jarvis-network/synthereum-config/dist/data';
import { SupportedSynthereumPairExact } from '@jarvis-network/synthereum-config/dist/supported/all-synthereum-pairs';
import { BurnerWalletTokenConfig } from '@jarvis-network/synthereum-config/dist/types/config';
import {
  BehaviorSubject,
  filter,
  map,
  switchMap,
  switchMapTo,
  takeUntil,
  tap,
} from 'rxjs';

import { getTokenInfo } from '../core/realms/common-realm';

import { interval$, PriceFeedSymbols } from './price-feed';
import { Context, Dependencies, Epic, ReduxAction } from './types';

export interface Asset extends BurnerWalletTokenConfig {
  pair: string | SupportedSynthereumPairExact;
  price?: StringAmount | undefined;
}

export type OutputAction = ReduxAction<'markets/setMarketsList', Assets>;

export type InputAction = ReduxAction<'GET_MARKET_LIST', Empty> | OutputAction;

export type Assets = {
  [Pair in SupportedSynthereumPairExact]?: Asset;
};

export const getActiveAssets = async ({
  burnerRealmAgent,
  chainLinkPriceFeed,
}: Partial<Context>): Promise<Assets | undefined> => {
  try {
    if (!burnerRealmAgent) {
      return;
    }
    const syntheticPairs = Object.entries(burnerRealmAgent!.activePools);
    const assetsArray = await Promise.all(
      syntheticPairs.map(async ([_, pool]) => {
        const data = {
          [`${pool?.syntheticToken.symbol}/${pool?.collateralToken.symbol}`]: {
            pair: `${pool?.syntheticToken.symbol}/${pool?.collateralToken.symbol}`,
            syntheticName: await pool?.syntheticToken.instance.methods
              .name()
              .call(),
            decimals: pool?.syntheticToken.decimals,
            syntheticSymbol: pool?.symbol,
            address: pool?.syntheticToken.address,
            balance: await (
              await burnerRealmAgent.syntheticTokenBalanceOf(pool?.symbol)
            ).toString(),
            allowance: await burnerRealmAgent.collateralToken.instance.methods
              .allowance(burnerRealmAgent.agentAddress, pool.address)
              .call(),
            price: await chainLinkPriceFeed!.getPrice(
              `${pool?.syntheticToken.symbol}/${pool?.collateralToken.symbol}` as PriceFeedSymbols,
            ),
          },
        };
        return data;
      }),
    );

    const collateral = burnerRealmAgent.collateralToken;
    const name = await collateral.instance.methods.name().call();
    const collateralArray = [
      {
        [collateral.symbol]: {
          pair: collateral.symbol,
          syntheticName: name,
          decimals: collateral.decimals,
          syntheticSymbol: collateral.symbol as any,
          chainLinkFeedIdentifier: collateral.symbol,
          address: collateral.address as any,
          balance: await (
            await burnerRealmAgent.collateralBalance()
          ).toString(),
          price: await chainLinkPriceFeed!.getPrice(
            collateral.symbol as PriceFeedSymbols,
          ),
        },
      },
    ];
    const extraTokens = Object.values(burnerWalletConfig);

    const extraTokenArrays = await Promise.all(
      extraTokens.map(async token => {
        const netId = (await burnerRealmAgent.realm.web3.eth.net.getId()) as SupportedNetworkId;
        const address = token.network[netId];

        const tokenInfo = await getTokenInfo(
          burnerRealmAgent.realm.web3,
          address,
        );

        const tokeName = await tokenInfo.instance.methods.name().call();
        // `${tokenInfo.symbol}/BUSD`
        return {
          [`${tokenInfo.symbol}/BUSD`]: {
            pair: `${tokenInfo.symbol}/BUSD`,
            syntheticName: tokeName,
            decimals: tokenInfo.decimals,
            syntheticSymbol: tokenInfo.symbol as any,
            chainLinkFeedIdentifier: tokenInfo.symbol,
            address: tokenInfo.address as any,
            allowance: await tokenInfo.instance.methods
              .allowance(
                burnerRealmAgent.agentAddress,
                '0x49E88C9036e4F888069a354465F1738B372DF03B',
              )
              .call(),
            balance: await (
              await burnerRealmAgent.collateralBalance()
            ).toString(),
            price: await chainLinkPriceFeed!.getPrice(
              `WBTC/BUSD` as PriceFeedSymbols,
            ),
          },
        };
      }),
    );
    const assets = [
      ...assetsArray,
      ...collateralArray,
      ...extraTokenArrays,
    ].flatMap(a => Object.values(a));

    return Object.fromEntries(assets.map(a => [a.pair, a]));
  } catch (error) {
    console.log(error, '//*/*/*/*/*/*');
    return {};
  }
};

const currentPairs = new BehaviorSubject<SupportedSynthereumPairExact>(
  'jEUR/BUSD',
);

export const assetsEpic: Epic<ReduxAction, ReduxAction> = (
  action$,
  _state$: any,
  { context$ }: Dependencies,
) =>
  action$.pipe(
    filter(action => action.type === 'GET_MARKET_LIST'),
    tap(action => {
      switch (action.type) {
        case 'GET_MARKET_LIST':
          currentPairs.next(action.payload!);
          break;
        default:
          break;
      }
    }),
    switchMapTo(interval$),
    switchMapTo(currentPairs),
    switchMap(_ =>
      context$!.pipe(
        map(context => ({
          context,
        })),
        takeUntil(
          action$.pipe(
            filter(
              a => a.type === 'networkSwitch' || a.type === 'addressSwitch',
            ),
          ),
        ),
      ),
    ),

    switchMap(
      async ({ context }) => await getActiveAssets(context), // eslint-disable-line
    ),
    map(results => ({
      type: 'assets/setAssetsList',
      payload: results,
    })),
  );
