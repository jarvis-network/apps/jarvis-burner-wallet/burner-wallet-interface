import type { Epic as RoEpic } from 'redux-observable';
import { BehaviorSubject, Observable } from 'rxjs';
import type Web3 from 'web3';

import { RealmAgent } from '../core/realms/burner-wallet/realm-agent';
import { ChainLinkPriceFeed } from '../price-feed/chainlink';

export interface Context {
  web3: Web3 | null;
  priceFeedWeb3?: Web3 | null;
  networkId: number | null;
  chainLinkPriceFeed: ChainLinkPriceFeed | null;
  burnerRealmAgent?: RealmAgent | null;
}

export const context$ = new BehaviorSubject<Context | null>({
  networkId: null,
  burnerRealmAgent: null,
  chainLinkPriceFeed: null,
  web3: null,
  priceFeedWeb3: null,
});
export type ReduxAction<Type extends string = string, Payload = any> = {
  type: Type;
  payload?: Payload;
};

export interface StateObservable<S> extends Observable<S> {
  value: S;
}

export type Epic<
  Input extends ReduxAction,
  Output extends Input,
  State = unknown
> = RoEpic<Input, Output, State, Dependencies>;

export interface Dependencies {
  context$: BehaviorSubject<Context> | null;
}
