import { typeCheck } from '@jarvis-network/core-utils/dist/base/meta';
import { PerSynthereumPairExact2 } from '@jarvis-network/synthereum-config/src/supported/all-synthereum-pairs';

interface Expression {
  simple: string;
  inverted?: string;
}
export type SyntheticPriceExpression = PerSynthereumPairExact2<Expression>;

const common = {
  'jCAD/UMA': {
    simple: 'CADUSD * (1/ETHUSD) * (1/UMAETH)',
  },
  'jGBP/UMA': {
    simple: 'GBPUSD * (1/ETHUSD) * (1/UMAETH)',
  },
  'jCHF/UMA': {
    simple: 'CHFUSD * (1/ETHUSD) * (1/UMAETH)',
  },
  'jEUR/UMA': {
    simple: '(EURUSD / (ETHUSD * UMAETH))',
  },
  'jZAR/UMA': {
    simple: 'ZARUSD * (1/ETHUSD) * (1/UMAETH)',
  },
  'jPHP/UMA': {
    simple: 'PHPUSD * (1/ETHUSD) * (1/UMAETH)',
  },
  'jNGN/UMA': {
    simple: 'NGNUSD * (1/ETHUSD) * (1/UMAETH)',
  },
  'jKRW/UMA': {
    simple: 'KRWUSD * (1/ETHUSD) * (1/UMAETH)',
  },
  'jJPY/UMA': {
    simple: 'JPYUSD * (1/ETHUSD) * (1/UMAETH)',
  },
  UMA: {
    simple: 'ETHUSD * UMAETH',
  },

  // USDC-based
  'jCAD/USDC': {
    simple: 'CADUSD',
  },
  'jGBP/USDC': {
    simple: 'GBPUSD',
  },
  'jCHF/USDC': {
    simple: 'CHFUSD',
  },
  'jEUR/USDC': {
    simple: 'EURUSD',
  },
  'jZAR/USDC': {
    simple: 'ZARUSD',
  },
  'jPHP/USDC': {
    simple: 'PHPUSD',
  },
  'jNGN/USDC': {
    simple: 'NGNUSD',
  },
  'jBRL/USDC': {
    simple: 'BRLUSD',
  },
  'jKRW/USDC': {
    simple: 'KRWUSD',
  },
  'jJPY/USDC': {
    simple: 'JPYUSD',
  },
  'jAUD/USDC': {
    simple: 'AUDUSD',
  },
  'jCOP/USDC': {
    simple: 'COPUSD',
  },
  'jSGD/USDC': {
    simple: 'SGDUSD',
  },
  'jSEK/USDC': {
    simple: 'SEKUSD',
  },
  'WBTC/USDC': {
    simple: 'BTCUSD',
  },
  'BTC/USDC': {
    simple: 'BTCUSD',
  },
  'ETH/USDC': {
    simple: 'ETHUSD',
  },
  USDC: {
    simple: '1',
  },
  'jSPX/USDC': { simple: '' },
  'jXTI/USDC': { simple: '' },
  'jXAG/USDC': { simple: '' },
  'jXAU/USDC': { simple: '' },
  'WETH/USDC': { simple: '' },
  'BTCB/USDC': {
    simple: 'BTCUSD',
  },
  // BUSD
  BUSD: {
    simple: '1',
  },
  'ETH/BUSD': {
    simple: 'ETHUSD',
  },
  'WBTC/BUSD': {
    simple: 'BTCUSD',
  },
  'BTC/BUSD': {
    simple: 'BTCUSD',
  },
  'BTCB/BUSD': {
    simple: 'BTCUSD',
  },

  'jNGN/BUSD': {
    simple: 'NGNUSD',
  },
  'jBRL/BUSD': {
    simple: 'BRLUSD',
  },
  'jGBP/BUSD': {
    simple: 'GBPUSD',
  },
  'jCHF/BUSD': {
    simple: 'CHFUSD',
  },
  'jEUR/BUSD': {
    simple: 'EURUSD',
  },
  'jZAR/BUSD': {
    simple: 'ZARUSD',
  },

  'jCAD/BUSD': {
    simple: 'CADUSD',
  },

  'jPHP/BUSD': {
    simple: 'PHPUSD',
  },

  'jKRW/BUSD': {
    simple: 'KRWUSD',
  },
  'jJPY/BUSD': {
    simple: 'JPYUSD',
  },
  'jAUD/BUSD': {
    simple: 'AUDUSD',
  },
  'jCOP/BUSD': {
    simple: 'COPUSD',
  },
  'jSGD/BUSD': {
    simple: 'SGDUSD',
  },
  'jSEK/BUSD': {
    simple: 'SEKUSD',
  },

  'jSPX/BUSD': { simple: '' },
  'jXTI/BUSD': { simple: '' },
  'jXAG/BUSD': { simple: '' },
  'jXAU/BUSD': { simple: '' },
  'WETH/BUSD': { simple: '' },
};
export const syntheticPriceExpression = typeCheck<SyntheticPriceExpression>()({
  1: {
    // UMA-based
    ...common,
  },
  42: {
    ...common,
    'jGBP/UMA': {
      simple: 'GBPUSD * (1/ETHUSD) * 10',
    },
    'jCHF/UMA': {
      simple: 'CHFUSD * (1/ETHUSD) * 10',
    },
    'jEUR/UMA': {
      simple: 'EURUSD * (1/ETHUSD) * 10',
    },
  },
  137: {
    ...common,
    USDC: {
      simple: '1',
    },
  },
  56: {
    ...common,
    USDC: {
      simple: '1',
    },
  },
  80001: {
    ...common,
    USDC: {
      simple: '1',
    },
  },
} as const);
