import { context$ } from '@jarvis-network/synthereum-ts/dist/epics/types';

export const dependencies = {
  context$,
};
