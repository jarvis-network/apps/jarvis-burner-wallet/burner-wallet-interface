import { createAction } from '@reduxjs/toolkit';

export const logoutAction = createAction('logout');
export const addressSwitchAction = createAction('addressSwitch');
export const networkSwitchAction = createAction('networkSwitch');
