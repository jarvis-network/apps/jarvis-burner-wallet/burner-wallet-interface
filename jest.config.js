module.exports = {
  projects: [
    '<rootDir>/libs/core-utils',
    '<rootDir>/libs/synthereum-ts',
    '<rootDir>/libs/synthereum-config',
    '<rootDir>/libs/meta-tx-lib',
    '<rootDir>/apps/burner-wallet-v2',
  ],
};
