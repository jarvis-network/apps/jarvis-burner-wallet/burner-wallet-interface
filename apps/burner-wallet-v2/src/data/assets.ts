import { BurnerWalletAssets } from '@/state/slices/assets';
import { StringAmount } from '@jarvis-network/core-utils/dist/base/big-number';
import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';
import { AddressOn } from '@jarvis-network/core-utils/dist/eth/address';
import {
  SupportedNetworkId,
  SupportedNetworkName,
} from '@jarvis-network/synthereum-config/dist';
import {
  ExchangeSynthereumToken,
  SupportedSynthereumPairExact,
  SupportedSynthereumSymbol,
} from '@jarvis-network/synthereum-config/dist/supported/all-synthereum-pairs';
import { SynthereumCollateralSymbol } from '@jarvis-network/synthereum-config/dist/types/all-price-feed-symbols';

export interface MarketAsset<T extends SupportedSynthereumSymbol> {
  name: T;
}
export interface MarketCollateral<T extends SynthereumCollateralSymbol> {
  name: T;
}
export interface Asset<
  Out extends SupportedSynthereumSymbol = SupportedSynthereumSymbol,
  In extends SynthereumCollateralSymbol = SynthereumCollateralSymbol
> {
  name?: string;
  syntheticSymbol?: ExchangeSynthereumToken;
  pair: `${Out}/${In}` | SupportedSynthereumPairExact | In;
  readonly assetIn: MarketCollateral<In>;
  readonly assetOut: MarketAsset<Out> | MarketCollateral<In> | { name: 'BTCB' };
  price?: StringAmount | undefined;
  allowance?: StringAmount | undefined;
  decimals?: number;
  syntheticName?: string; /// Example: "Jarvis Synthetic Euro",
  chainLinkPriceFeed?: SupportedSynthereumPairExact;
  address?: AddressOn<any>;
  network?: {
    [Net in SupportedNetworkId]: AddressOn<Net>;
  };
}

export interface AssetPair {
  input: Asset;
  output: Asset;
  name: string;
}

export const PRIMARY_STABLE_COIN_TEXT_SYMBOL = '$';

export interface AssetWithWalletInfo extends Asset {
  stableCoinValue: FPN | null;
  ownedAmount: FPN;
  parsed: number;
}

export const burnerWalletAssets: BurnerWalletAssets<SupportedNetworkName> = {
  'jCHF/BUSD': {
    pair: 'jCHF/BUSD',
    assetIn: {
      name: 'BUSD',
    },
    assetOut: {
      name: 'jCHF',
    },
  },
  'jEUR/BUSD': {
    pair: 'jEUR/BUSD',
    assetIn: {
      name: 'BUSD',
    },
    assetOut: {
      name: 'jEUR',
    },
  },
  'jGBP/BUSD': {
    pair: 'jGBP/BUSD',
    assetIn: {
      name: 'BUSD',
    },
    assetOut: {
      name: 'jGBP',
    },
  },

  'jZAR/BUSD': {
    pair: 'jZAR/BUSD',
    assetIn: {
      name: 'BUSD',
    },
    assetOut: {
      name: 'jZAR',
    },
  },
  'jBRL/BUSD': {
    pair: 'jBRL/BUSD',
    assetIn: {
      name: 'BUSD',
    },
    assetOut: {
      name: 'jBRL',
    },
  },
  'jNGN/BUSD': {
    pair: 'jNGN/BUSD',
    assetIn: {
      name: 'BUSD',
    },
    assetOut: {
      name: 'jNGN',
    },
  },
  'BTCB/BUSD': {
    pair: 'BTCB/BUSD',
    assetIn: {
      name: 'BUSD',
    },
    assetOut: {
      name: 'BTCB',
    },
  },
  BUSD: {
    pair: 'BUSD',
    assetIn: {
      name: 'BUSD',
    },
    assetOut: {
      name: 'BUSD',
    },
  },
} as const;
