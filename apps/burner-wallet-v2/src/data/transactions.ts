import BN from 'bn.js';

import { Asset } from './assets';

// @todo After TS library will be implemented:
// move all this types to the library and prepare transformers for frontend

type RawAmount = BN;

export type TransactionType =
  | 'Mint'
  | 'Exchange'
  | 'Redeem'
  | 'Transfer'
  | 'Receive'
  | 'Send'
  | 'SendToSelf';

export type TransactionStatus = 'pending' | 'success' | 'failure';

export interface TransactionIO {
  asset: Asset;
  amount: RawAmount;
}

// Base interface for any kind of transaction
interface TransactionBase {
  timeStamp: number; // we can't store Date in Redux; timestamp is value in MS (as default to JS)
  txHash: string;
  type: TransactionType;
  status: TransactionStatus;
}

// Represents a plain ETH or ERC20 token transfer transaction
export interface RegularTransaction extends TransactionBase {
  // 'send' if `from` is our address, 'receive' if `to` is ours, and 'sendToSelf' if both.
  type: 'Transfer' | 'Send' | 'Receive' | 'SendToSelf';
  from: string;
  to: string;
  input: TransactionIO;
}

export interface SynthereumTransaction extends TransactionBase {
  type: 'Mint' | 'Exchange' | 'Redeem' | 'Transfer';
  rv: {
    collateralToken?: string;
    symbol?: string;
    syntheticToken?: string;
    // Transfer
    from?: string;
    to?: string;
    value?: string;
    // Mint
    account?: string;
    collateralSent?: string;
    feePaid?: string;
    numTokensReceived?: string;
    pool?: string;
    recipient?: string;
    // Redeem
    collateralReceived?: string;
    numTokensSent?: string;
    // Exchange
    destPoolInfo: {
      collateralToken?: string;
      symbol?: string;
      syntheticToken?: string;
    };
    destNumTokensReceived: string;
    sourcePool: string;
  };
}

export type Transaction = SynthereumTransaction;
