import { assertIsAddress as A } from '@jarvis-network/core-utils/dist/eth/address';
import { ExchangeSynthereumToken } from '@jarvis-network/synthereum-config/dist';

export const Tokens = {
  jEUR: {
    syntheticName: 'Jarvis Synthetic Euro',
    syntheticSymbol: 'jEUR' as ExchangeSynthereumToken,
    priceFeedIdentifier: 'jEUR/BUSD',
    jarvisPriceFeedIdentifier: 'EURUSD',
    flag: 'eur',
    network: {
      80001: A<80001>('0x02cbe6055f8aad745321f70d6add4711455c7f45'),
      137: A<137>('0x0Fa1A6b68bE5dD9132A09286a166d75480BE9165'),
    },
  },
  jCHF: {
    syntheticName: 'Jarvis Synthetic Swiss Franc',
    syntheticSymbol: 'jCHF',
    priceFeedIdentifier: 'jCHF/BUSD',
    jarvisPriceFeedIdentifier: 'CHFUSD',
    flag: 'chf',
    network: {
      80001: A<80001>('0x2b7d8fb31676ab02da37a124acfa726125b1b95b'),
      137: A<137>('0x9B0a1C61F234e2D21b6f7c0Da6178dfbbaA3756f'),
    },
  },
  jGBP: {
    syntheticName: 'Jarvis Synthetic British Pound',
    syntheticSymbol: 'jGBP',
    priceFeedIdentifier: 'jGBP/BUSD',
    jarvisPriceFeedIdentifier: 'GBPUSD',
    flag: 'gbp',
    network: {
      80001: A<80001>('0x915E1f55eC2dc1524101ef44d4a0EebFe10Bc6bc'),
      137: A<137>('0xA87b3e78D128dAB9db656Cf459C9266c4C1D5255'),
    },
  },
};
