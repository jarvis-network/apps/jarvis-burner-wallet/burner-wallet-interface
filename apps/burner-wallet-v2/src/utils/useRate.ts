import { Rate } from '@/state/initialState';
import { useReduxSelector } from '@/state/useReduxSelector';
import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';
import {
  ExchangeSynthereumToken,
  SupportedSynthereumPairExact,
} from '@jarvis-network/synthereum-config/dist/supported/all-synthereum-pairs';

export const useRate = (
  inputSymbol: ExchangeSynthereumToken | null,
  outputSymbol: ExchangeSynthereumToken | null,
): Rate | null =>
  useReduxSelector(state => {
    if (!inputSymbol || !outputSymbol) {
      return null;
    }

    const inputAsset = state.assets.list[
      inputSymbol !== 'BUSD'
        ? (`${inputSymbol}/BUSD` as SupportedSynthereumPairExact)
        : ('BUSD' as SupportedSynthereumPairExact)
    ]!;
    const outputAsset = state.assets.list[
      outputSymbol !== 'BUSD'
        ? (`${outputSymbol}/BUSD` as SupportedSynthereumPairExact)
        : ('BUSD' as SupportedSynthereumPairExact)
    ]!;

    // asset not found or price not yet loaded
    if (
      !inputAsset ||
      !outputAsset ||
      !inputAsset.price ||
      !outputAsset.price
    ) {
      return null;
    }

    return {
      rate: FPN.fromWei(outputAsset.price).div(FPN.fromWei(inputAsset.price)),
    };
  });
