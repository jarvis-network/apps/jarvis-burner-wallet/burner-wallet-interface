import { Asset } from '@/data/assets';
import { State } from '@/state/initialState';
import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';

interface Data {
  assetPay: Asset | undefined;
  assetReceive: Asset | undefined;
  base: State['exchange']['base'];
  pay: State['exchange']['pay'];
  receive: State['exchange']['receive'];
  fee: FPN;
}

export const calcExchange = ({
  assetPay,
  assetReceive,
  base,
  pay,
  receive,
  fee,
}: Data) => {
  let payValue = null;
  let receiveValue = null;
  let netCollateral = null;
  let grossCollateral = null;
  let transactionCollateral = null;
  const paySymbol = assetPay!.syntheticSymbol;
  const receiveSymbol = assetReceive!.syntheticSymbol;

  if (paySymbol === receiveSymbol || !assetPay?.price || !assetReceive?.price) {
    return {
      payValue,
      receiveValue,
      netCollateral,
      grossCollateral,
      transactionCollateral,
    };
  }

  if (paySymbol === 'BUSD') {
    // mint
    if (base === 'pay') {
      payValue = new FPN(pay);
      grossCollateral = payValue;
      netCollateral = grossCollateral.div(new FPN(1).add(fee));
      transactionCollateral = netCollateral;
      receiveValue = netCollateral.div(FPN.fromWei(assetReceive.price));
    } else {
      receiveValue = new FPN(receive);
      netCollateral = receiveValue.mul(FPN.fromWei(assetReceive.price));
      grossCollateral = netCollateral.mul(new FPN(1).add(fee));
      transactionCollateral = netCollateral;
      payValue = grossCollateral;
    }
  } else if (receiveSymbol === 'BUSD') {
    // redeem
    if (base === 'pay') {
      payValue = new FPN(pay);
      grossCollateral = payValue.mul(FPN.fromWei(assetPay.price));
      netCollateral = grossCollateral.mul(new FPN(1).sub(fee));
      transactionCollateral = grossCollateral;
      receiveValue = netCollateral;
    } else {
      receiveValue = new FPN(receive);
      netCollateral = receiveValue;
      grossCollateral = netCollateral.div(new FPN(1).sub(fee));
      transactionCollateral = grossCollateral;
      payValue = grossCollateral.div(FPN.fromWei(assetPay.price));
    }
  } else {
    // exchange
    // eslint-disable-next-line no-lonely-if
    if (base === 'pay') {
      payValue = new FPN(pay);
      grossCollateral = payValue.mul(FPN.fromWei(assetPay.price));
      netCollateral = grossCollateral.mul(new FPN(1).sub(fee));
      transactionCollateral = grossCollateral;
      receiveValue = netCollateral.div(FPN.fromWei(assetReceive.price));
    } else {
      receiveValue = new FPN(receive);
      netCollateral = receiveValue.mul(FPN.fromWei(assetReceive.price));
      grossCollateral = netCollateral.div(new FPN(1).sub(fee));
      transactionCollateral = grossCollateral;
      payValue = grossCollateral.div(FPN.fromWei(assetPay.price));
    }
  }

  return {
    payValue,
    receiveValue,
    netCollateral,
    grossCollateral,
    transactionCollateral,
  };
};
