import { hexToNumber, isHexStrict } from 'web3-utils';

export const decodeSignature = (signature: any) => {
  if (!isHexStrict(signature)) {
    throw new Error(
      'Given value "'.concat(signature, '" is not a valid hex string.'),
    );
  }
  const r = signature.slice(0, 66);
  const s = '0x'.concat(signature.slice(66, 130));
  let v: any = '0x'.concat(signature.slice(130, 132));
  v = hexToNumber(v);
  if (![27, 28].includes(v)) v += 27;
  return { r, s, v };
};
