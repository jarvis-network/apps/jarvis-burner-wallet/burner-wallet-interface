/* eslint-disable no-underscore-dangle */
import { useReduxSelector } from '@/state/useReduxSelector';
import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';
import { SupportedSynthereumPairExact } from '@jarvis-network/synthereum-config/dist/supported/all-synthereum-pairs';

import { calcExchange } from './calcExchange';
import { useRate } from './useRate';

export const useExchangeValues = () => {
  const { base, pay, receive, assetPay, assetReceive } = useReduxSelector(
    state => ({
      ...state.exchange,
      assetPay:
        state.assets.list[
          state.exchange.payAsset !== 'BUSD'
            ? (`${state.exchange.payAsset}/BUSD` as SupportedSynthereumPairExact)
            : ('BUSD' as SupportedSynthereumPairExact)
        ],
      assetReceive:
        state.assets.list[
          state.exchange.receiveAsset !== 'BUSD'
            ? (`${state.exchange.receiveAsset}/BUSD` as SupportedSynthereumPairExact)
            : ('BUSD' as SupportedSynthereumPairExact)
        ],
    }),
  );

  const paySymbol = assetPay?.syntheticSymbol || null;
  const receiveSymbol = assetReceive?.syntheticSymbol || null;

  const rate = useRate(paySymbol!, receiveSymbol);
  const {
    payValue,
    receiveValue,
    netCollateral,
    grossCollateral,
    transactionCollateral,
  } = calcExchange({
    assetPay,
    assetReceive,
    base,
    pay,
    receive,
    fee: new FPN(0),
  });
  const fee =
    grossCollateral && netCollateral
      ? grossCollateral.sub(netCollateral)
      : null;
  const payString = payValue?.format() || '';
  const receiveString = receiveValue?.format() || '';

  return {
    base,
    pay,
    receive,
    paySymbol,
    receiveSymbol,
    assetPay,
    assetReceive,
    fee,
    rate,
    payValue,
    payString,
    receiveValue,
    receiveString,
    transactionCollateral,
  };
};
