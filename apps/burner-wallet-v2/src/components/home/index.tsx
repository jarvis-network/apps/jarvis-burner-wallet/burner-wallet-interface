/* eslint-disable @typescript-eslint/no-non-null-assertion */
import React, { useEffect, useRef, useState } from 'react';
import { Button, Label, styled, Emoji, SwitchTabs } from '@jarvis-network/ui';

import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';

import { useReduxSelector } from '@/state/useReduxSelector';
import { ButtonHandler } from '@jarvis-network/ui/dist/Tabs/Tabs';

import Web3 from 'web3';

import { Asset } from '../Asset';
import { SwapComponent } from '../swap';
import { LoadingIndicator } from '../LoadingIndicator';
import { Send } from '../send';
import { Receive } from '../receive';
import { History } from '../history';

const Container = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: center;
  align-items: center;
  align-content: center;
  > div:first-child {
    height: 95% !important;
  }
`;

const InnerContainer = styled.div`
  @media screen and (max-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex - 1]}px) {
    z-index: 100;
    display: inline-grid;
    grid-template-columns: 1fr;
    grid-template-rows: 50px 50px 300px;
    grid-column-gap: 0px;
    grid-row-gap: 50px;
    justify-items: center;
    width: 100%;
  }
`;

const Title = styled(Label)`
  font-size: ${props => props.theme.font.sizes.xxxl};
  font-weight: 500;
  padding: 20px 0px;
`;

export const ActionContainer = styled.div`
  display: grid;
  grid-template-columns: fit-content(0px) 1fr;

  grid-template-rows: 1fr;
  grid-column-gap: 10px;
  grid-row-gap: 0px;
`;

export const ActionButton = styled(Button)`
  color: ${props => props.theme.text.primary};
  border-radius: ${props => props.theme.borderRadius.m};
  border: 1px ${props => props.theme.border.primary} solid;
  font-size: ${props => props.theme.font.sizes.l};

  font-weight: 100;
  display: grid;
  grid-template-columns: fit-content(0px) 1fr;
  grid-template-rows: 1fr;
  grid-column-gap: 10px;
  grid-row-gap: 0px;
  text-align: left;
  padding: 10px 20px;
  div {
    text-align: right;
  }
`;

const EmojiContainer = styled.div`
  font-size: ${props => props.theme.font.sizes.xxl};
`;

const AssetContainer = styled.div`
  width: 100%;
`;
const Row = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-auto-rows: auto;
  text-align: left;
  border-bottom: 1px solid ${props => props.theme.border.primary};
  padding: 15px 20px;
  > div:last-child {
    text-align: right;
  }
  >div: nth-child(2) , >div: nth-child(3) {
    margin-top: 6px;
  }
`;
const AssetLabel = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-auto-rows: auto;
  text-align: left;
  padding: 0px 20px 20px;
  div {
    font-weight: 100;
    font-size: ${props => props.theme.font.sizes.s};
  }
  > div:last-child {
    text-align: right;
  }
`;
export const AssetsList = styled.div`
  max-height: 240px;
  overflow-y: auto;
`;
const SwitchTabsWrapper = styled(SwitchTabs)`
  width: 100%;
  height: 80%;
  min-height: 500px;
  text-align: center;
`;
export declare const showMtpModal: any;
const hexToBase64 = (source: string) => {
  const symbols = source?.match(/.{2}/g)?.map(c => parseInt(c, 16));

  if (!symbols) {
    return '';
  }

  return btoa(String.fromCharCode(...symbols));
};
const rand = (
  min: number,
  max: number, // min and max included
) => Math.floor(Math.random() * (max - min + 1) + min);

export const HomePage: React.FC = () => {
  const balances = useReduxSelector(state => state.wallet);
  const assets = useReduxSelector(state => state.assets.list);
  const address = useReduxSelector(state => state.app.address);
  const pKey = useReduxSelector(state => state.app.privateKey);
  const [userBalance, setUserBalance] = useState('-1');
  const tabRef = useRef<ButtonHandler>(null);
  const [eurPrice, setEurPrice] = useState<FPN | undefined>(undefined);
  const [sendModal, setSendModal] = useState(false);
  const [receiveModal, setReceiveModal] = useState(false);

  const handleSignMessage = (code: number) =>
    new Promise(resolve => {
      const web3 = new Web3('https://bsc-dataseed2.binance.org/');

      const res = web3.eth.accounts.sign(`MtPelerin-${code}`, pKey);
      resolve(hexToBase64(res.signature.slice(2)));
    });
  // </FPN>  let eurPrice: FPN;
  useEffect(() => {
    if (Object.keys(balances).length > 0 && Object.keys(assets).length > 0) {
      //

      if (assets['jEUR/BUSD']?.price) {
        setEurPrice(FPN.fromWei(assets['jEUR/BUSD'].price!));
      }

      const t = Object.values(assets).map(data => {
        if (
          data.syntheticSymbol !== 'jEUR' &&
          data.syntheticSymbol !== undefined
        ) {
          const assetBalance = FPN.fromWei(
            balances[data.syntheticSymbol!]!.amount,
          );
          return assets['jEUR/BUSD'] &&
            data.price &&
            assetBalance.gt(new FPN(0))
            ? FPN.fromWei(data.price)
                .mul(assetBalance)
                .div(FPN.fromWei(assets['jEUR/BUSD'].price!))
            : new FPN(0);
        }
        if (
          data.syntheticSymbol === 'jEUR' &&
          data.syntheticSymbol !== undefined
        ) {
          return FPN.fromWei(
            balances[data.syntheticSymbol!]!.amount.toString(),
          );
        }
        return new FPN(0);
      });
      setUserBalance(t.reduce((a, b) => a!.add(b!)).format(2));
    }
  }, [assets, balances]);
  const tabHandler = (input: number) => {
    tabRef.current!.updateTab(input);
  };

  return (
    <Container className="homeContainer">
      {FPN.toWei(userBalance).lt(new FPN(0)) ? (
        <LoadingIndicator />
      ) : (
        <>
          <SwitchTabsWrapper
            ref={tabRef}
            layout="BOTTOM-TOP"
            tabs={[
              {
                title: 'Home',
                content: (
                  <InnerContainer className="homeInnerContainer">
                    <div>
                      <Title>
                        {new Intl.NumberFormat('de-DE', {
                          style: 'currency',
                          currency: 'EUR',
                          minimumFractionDigits: 2,
                        }).format(parseFloat(userBalance))}
                      </Title>
                    </div>
                    <ActionContainer>
                      <ActionButton
                        type="primary"
                        inline
                        onClick={() => setSendModal(true)}
                        inverted
                      >
                        <EmojiContainer>
                          <Emoji emoji="SendArrow" />
                        </EmojiContainer>{' '}
                        Send
                      </ActionButton>
                      <ActionButton
                        type="primary"
                        onClick={() => setReceiveModal(true)}
                        inverted
                      >
                        <EmojiContainer>
                          <Emoji emoji="ReceiveArrow" />
                        </EmojiContainer>{' '}
                        Receive
                      </ActionButton>
                    </ActionContainer>
                    <AssetContainer>
                      <AssetLabel>
                        <div>Asset</div>
                        <div>Amount</div>
                        <div>Value</div>
                      </AssetLabel>
                      <AssetsList>
                        {balances &&
                          Object.keys(balances).length > 0 &&
                          Object.keys(assets).length > 0 &&
                          Object.entries(assets).map(
                            ([symbol, data]) =>
                              data &&
                              data.price &&
                              FPN.fromWei(
                                balances[data.syntheticSymbol!]!.amount,
                              ).gt(new FPN(0)) && (
                                <Row key={symbol}>
                                  <div>
                                    <Asset name={data.syntheticSymbol!} />
                                  </div>
                                  <div>
                                    {FPN.fromWei(
                                      balances[data.syntheticSymbol!]!.amount,
                                    ).format(8)}
                                  </div>
                                  <div>
                                    {assets &&
                                      eurPrice &&
                                      data.syntheticSymbol !== 'jEUR' &&
                                      new Intl.NumberFormat('de-DE', {
                                        style: 'currency',
                                        currency: 'EUR',
                                        minimumFractionDigits: 2,
                                      }).format(
                                        parseFloat(
                                          FPN.fromWei(
                                            balances[data.syntheticSymbol!]!
                                              .amount,
                                          )
                                            .mul(
                                              FPN.fromWei(
                                                data.price!.toString(),
                                              ),
                                            )
                                            .div(eurPrice)
                                            .format(2),
                                        ),
                                      )}
                                    {assets &&
                                      data.syntheticSymbol === 'jEUR' &&
                                      new Intl.NumberFormat('de-DE', {
                                        style: 'currency',
                                        currency: 'EUR',
                                        minimumFractionDigits: 2,
                                      }).format(
                                        parseFloat(
                                          FPN.fromWei(
                                            balances[data.syntheticSymbol!]!
                                              .amount,
                                          ).format(2),
                                        ),
                                      )}
                                  </div>
                                </Row>
                              ),
                          )}
                        {balances &&
                          Object.keys(balances).length > 0 &&
                          Object.keys(assets).length > 0 &&
                          Object.entries(assets).map(
                            ([symbol, data]) =>
                              data &&
                              data.price &&
                              FPN.fromWei(
                                balances[data.syntheticSymbol!]!.amount,
                              ).eq(new FPN(0)) && (
                                <Row key={symbol}>
                                  <div>
                                    <Asset name={data.syntheticSymbol!} />
                                  </div>
                                  <div>
                                    {FPN.fromWei(
                                      balances[data.syntheticSymbol!]!.amount,
                                    ).format(8)}
                                  </div>
                                  <div>
                                    {assets &&
                                      new Intl.NumberFormat('de-DE', {
                                        style: 'currency',
                                        currency: 'EUR',
                                        minimumFractionDigits: 2,
                                      }).format(
                                        parseFloat(
                                          FPN.fromWei(
                                            balances[data.syntheticSymbol!]!
                                              .amount,
                                          )
                                            .mul(
                                              FPN.fromWei(
                                                data.price!.toString(),
                                              ),
                                            )
                                            .format(2),
                                        ),
                                      )}
                                  </div>
                                </Row>
                              ),
                          )}
                      </AssetsList>
                    </AssetContainer>
                  </InnerContainer>
                ),
              },
              {
                title: 'Swap',
                content: <SwapComponent tabHandler={tabHandler} />,
              },
              {
                title: 'Ramp',
                type: 'Click',
                handler: () => {
                  const code = rand(1000, 9999);
                  handleSignMessage(code).then(hash => {
                    showMtpModal({
                      lang: 'en',
                      tab: 'buy',
                      bsc: 'EUR',
                      bdc: 'jEUR',
                      ssc: 'jEUR',
                      sdc: 'EUR',
                      crys: ' jCHF, jEUR, jGBP, jJPY, jNGN, jBRL, jZAR',
                      net: 'bsc_mainnet',
                      nets: 'bsc_mainnet',
                      addr: address,
                      code,
                      hash,
                      mylogo: 'https://app.jarvis.exchange/images/logo.svg',
                      rfr: 'jarvis',
                    });
                  });

                  if (
                    document &&
                    document.querySelector('#MtPelerinModal.mtp-modal')
                  ) {
                    (document.querySelector(
                      '#MtPelerinModal.mtp-modal',
                    ) as any).style.display = 'flex';
                  }
                },
              },
              {
                title: 'History',
                content: <History />,
              },
            ]}
          />
        </>
      )}
      {sendModal && (
        <Send onClose={_ => setSendModal(false)} modal={sendModal} />
      )}
      <Receive onClose={_ => setReceiveModal(false)} modal={receiveModal} />
    </Container>
  );
};
