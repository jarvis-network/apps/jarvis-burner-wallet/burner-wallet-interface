import React from 'react';
import { Button, Label, styled, Emoji } from '@jarvis-network/ui';

const Container = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: center;
  align-items: center;
  align-content: center;
`;

const InnerContainer = styled.div`
  margin-top: -50px;
  @media screen and (max-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex - 1]}px) {
    z-index: 100;
    display: grid;
    grid-template-columns: 1fr;
    grid-template-rows: 1fr;
    grid-column-gap: 0px;
    grid-row-gap: 0px;
    justify-items: center;
    width: 90%;
  }
`;
const Title = styled(Label)`
  font-size: ${props => props.theme.font.sizes.xl};
  font-weight: 500;
`;

const Message = styled.div`
  text-align: center;
  color: ${props => props.theme.text.primary};
  font-weight: 300;
  font-size: ${props => props.theme.font.sizes.m};
  padding: 10px 0px;
`;

const CustomButton = styled(Button)`
  font-size: ${props => props.theme.font.sizes.m};
  width: 100%;
  height: ${props => props.theme.sizes.row};
  border-radius: ${props => props.theme.borderRadius.m};

  margin: 50px auto auto;
  text-align: center;
`;

const EmojiContainer = styled.div`
  font-size: ${props => props.theme.font.sizes.xxxl};
  padding-bottom: 50px;
`;
interface WelcomeProps {
  scanWalletHandler: () => void;
}
export const WelcomePage: React.FC<WelcomeProps> = ({ scanWalletHandler }) => (
  <Container>
    <InnerContainer>
      <>
        <EmojiContainer>
          <Emoji emoji="WavingHand" />
        </EmojiContainer>
        <Title>Hey!</Title>
        <Message>
          Scan the QR code to load <br />
          your first funds.
        </Message>
        <CustomButton type="success" onClick={scanWalletHandler}>
          Scan Wallet
        </CustomButton>
      </>
    </InnerContainer>
  </Container>
);
