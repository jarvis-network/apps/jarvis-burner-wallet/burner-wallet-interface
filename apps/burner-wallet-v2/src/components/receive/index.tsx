import React from 'react';
import {
  Emoji,
  Modal,
  NotificationType,
  OnMobile,
  styled,
  useNotifications,
} from '@jarvis-network/ui';

import { useReduxSelector } from '@/state/useReduxSelector';

import QRCode from 'react-qr-code';

import { RWebShare } from 'react-web-share';

import { ExchangeBox } from '../swap/style';
import { ActionButton, ActionContainer, CustomCard } from '../send/style';

const EmojiContainer = styled.div`
  font-size: ${props => props.theme.font.sizes.xxl};
`;
export interface ReceiveProps {
  modal: boolean;
  onClose: (isClose: boolean) => void;
}
export const Mobile = styled(OnMobile)`
  width: 100%;
`;

export const Address = styled.div`
  color: ${props => props.theme.text.primary};
  text-align: center;
  font-size: ${props => props.theme.font.sizes.l};
  padding: 20px;
  word-break: break-all;
  width: 90%;
  margin: 0px auto;
`;
export const QRCodeContainer = styled.div`
  width: 60%;
  margin: 0px auto;
  text-align: center;
`;

export const ModalRoot = styled.div`
  background: ${props => props.theme.scroll.background};
  color: ${props => props.theme.text.primary};
  border-radius: 20px;

  width: 100%;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;

  position: absolute;
  height: 70%;
  bottom: 0px;
  border-bottom-left-radius: 0;
  border-bottom-right-radius: 0;
`;

export const CustomModal = styled(Modal)`
  width: 100%;
  text-align: center;
  bottom: 0px;
`;

export const Receive: React.FC<ReceiveProps> = ({ modal, onClose }) => {
  const notify = useNotifications();

  const address = useReduxSelector(state => state.app.address);
  const handleClose = () => {
    console.log('close me');
  };

  return (
    <>
      {modal ? (
        <Mobile>
          <CustomModal
            overlayClassName="customModal"
            isOpened={!!modal}
            onClose={handleClose}
            duration={0.2}
            overlayContainerStyle={{
              width: '100%',
              justifyContent: 'end',
            }}
            overlayStyle={{
              width: '100%',
            }}
            animation="slideBottom"
          >
            <ModalRoot className="modalRoot">
              <CustomCard
                title="Your Wallet Address"
                titleBackground={false}
                pointer={false}
                leftEmoji="None"
                onBack={() => {
                  // setConfirm(false);
                  onClose(true);
                }}
              >
                <ExchangeBox className="container" error={false}>
                  <QRCodeContainer>
                    <QRCode size={200} value={address!.toString()} />
                  </QRCodeContainer>
                  <Address>{address}</Address>
                  <ActionContainer>
                    <RWebShare
                      data={{
                        text: address!.toString(),
                        url: `https://polygonscan.com/address/${address!.toString()}`,
                        title: 'Address',
                      }}
                      onClick={() => console.log('shared successfully!')}
                    >
                      <ActionButton type="primary" inline inverted>
                        <EmojiContainer>
                          <Emoji emoji="OutBox" />
                        </EmojiContainer>{' '}
                        Share
                      </ActionButton>
                    </RWebShare>
                    <ActionButton
                      type="primary"
                      onClick={() => {
                        navigator.clipboard.writeText(address!);

                        notify(
                          'Address copied successfully',
                          {
                            type: NotificationType.success,
                            icon: '✅',
                          },
                          'global',
                          1000,
                        );
                      }}
                      inverted
                    >
                      <EmojiContainer>
                        <Emoji emoji="PaperClip" />
                      </EmojiContainer>{' '}
                      Copy
                    </ActionButton>
                  </ActionContainer>
                </ExchangeBox>
              </CustomCard>
            </ModalRoot>
          </CustomModal>
        </Mobile>
      ) : null}
    </>
  );
};
