import { styled } from '@jarvis-network/ui';

export const ExchangeBoxConfirm = styled.div<{ error: boolean }>`
  margin: 30px 0px;
  display: grid;
  grid-template-columns: 1fr;

  grid-column-gap: 0px;
  grid-auto-rows: minmax(min-content, max-content);

  position: relative;
  margin-top: 15px;

  overflow: hidden;

  > div:nth-child(2n) {
    padding-top: 5px;
    padding-bottom: 15px;
    border-bottom: 1px solid ${props => props.theme.border.primary};
  }

  > div:last-child {
    border-bottom: 0px !important;
  }
  > div:nth-child(n + 1) {
    padding-top: 10px;
  }

  .txHash {
    text-align: left;
    word-break: break-all;
    a {
      color: ${props => props.theme.common.success} !important;
    }
  }
  .success {
    color: ${props => props.theme.text.secondary};
  }
`;
export const ConfirmTitle = styled.div`
  font-size: ${props => props.theme.font.sizes.m};
  padding: 5px 15px;
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
`;

export const ConfirmItem = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-content: stretch;
  padding: 0px 20px;
  flex-grow: 1
  align-items: center;

  width: fit-content
  > div {
    width: fit-content
  }

  img {
    width: 22px;
    height: 22px;
    object-fit: contain;
    vertical-align: middle;
  }

  i svg {
    width: 11px;
    height: 11px;
  }

  .assetName,
  .assetAmount {
    display: inline-flex;
    justify-content: space-between;
    vertical-align: middle;

    margin: 0px 10px;
    font-size: ${props => props.theme.font.sizes.l};
    font-family: Krub;
    font-weight: 300;
  }
  .assetName {
    margin-left: 0px;
  }

  .usdAmount {
    line-height:25px;
    color: ${props => props.theme.text.secondary};
  }


`;
