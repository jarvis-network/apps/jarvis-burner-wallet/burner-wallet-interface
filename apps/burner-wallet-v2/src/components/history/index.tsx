import _ from 'lodash';
import React, { useEffect } from 'react';

import { DateTime } from 'luxon';

import { styled } from '@jarvis-network/ui';

import { useDispatch } from 'react-redux';

import { useReduxSelector } from '@/state/useReduxSelector';

import { RELAYER } from '@jarvis-network/synthereum-ts/dist/core/realms/burner-wallet/realm-agent';

import { PageTitle } from '../swap/style';

import { LoadingIndicator } from '../LoadingIndicator';

import { Transaction } from './transaction';

const ICON_MAP = {
  SWAPPED: 'BsArrowLeftRight',
  RECEIVE: 'BsArrowDownLeft',
  SEND: 'BsArrowDownRight',
} as const;

export type TransactionType = keyof typeof ICON_MAP;

const Note = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: center;
  align-content: stretch;
  align-items: center;
  height: 100%;
`;
const TransactionWrapper = styled.div`
  height: calc(100% - 79px);
  overflow: scroll;
`;
const DateTile = styled.div`
  padding: 0px 20px;
  text-align: left;
  color: ${props => props.theme.text.secondary};
  font-size: ${props => props.theme.font.sizes.s};
`;

const Container = styled.div`
  position: relative;
  height: 100%;
`;
export const History = () => {
  const dispatch = useDispatch();
  const transactions = useReduxSelector(state => state.transactions.list);
  const loaded = useReduxSelector(state => state.transactions.loaded);

  useEffect(() => {
    dispatch({ type: 'LOAD_TRANSACTION', payload: RELAYER });
    return () => {
      dispatch({
        type: 'transactions/loaded',
        payload: false,
      });
    };
  }, []);

  const groupedResults = _.groupBy(transactions, tx =>
    DateTime.fromSeconds(tx.timeStamp).startOf('day'),
  );
  console.log(transactions, loaded);
  return (
    <Container>
      <PageTitle>History </PageTitle>
      {/* {!loaded && (
        <div>
          {' '}
          <LoadingIndicator />
        </div>
      )} */}
      {transactions && transactions.length > 0 ? (
        <TransactionWrapper>
          {groupedResults &&
            Object.values(groupedResults).map(gTransactions => (
              <div key={gTransactions[0].timeStamp}>
                <DateTile>
                  {DateTime.fromSeconds(
                    gTransactions[0].timeStamp,
                  ).toLocaleString(DateTime.DATE_MED)}
                </DateTile>
                {gTransactions.map(t => (
                  <Transaction
                    key={t.timeStamp + t.type + Math.random()}
                    type={t.type}
                    timestamp={t.timeStamp}
                    rv={t.rv}
                    transactionHash={t.txHash}
                  />
                ))}
              </div>
            ))}
        </TransactionWrapper>
      ) : !loaded ? (
        <LoadingIndicator />
      ) : (
        <Note>No Transaction found</Note>
      )}
    </Container>
  );
};
