import { styled, Icon, Flag, FlagKeys } from '@jarvis-network/ui';

import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';

import React, { useEffect, useState } from 'react';

import { DateTime } from 'luxon';

import { TransactionType } from '@/data/transactions';

import { useReduxSelector } from '@/state/useReduxSelector';

import { CustomModal } from '../swap/style';

import { ModalRoot } from '../receive';
import { CustomCard } from '../send/style';

import { ConfirmItem, ConfirmTitle, ExchangeBoxConfirm } from './style';

const ICON_MAP = {
  SWAPPED: 'BsArrowLeftRight',
  RECEIVE: 'BsArrowDownLeft',
  SEND: 'BsArrowDownRight',
} as const;

export interface TransactionProps {
  type: TransactionType;
  timestamp: number;
  rv: {
    collateralToken?: string;
    symbol?: string;
    syntheticToken?: string;
    // Transfer
    from?: string;
    to?: string;
    value?: string;
    // Mint
    account?: string;
    collateralSent?: string;
    feePaid?: string;
    numTokensReceived?: string;
    pool?: string;
    recipient?: string;
    // Redeem
    collateralReceived?: string;
    numTokensSent?: string;
    // Exchange
    destPoolInfo: {
      collateralToken?: string;
      symbol?: string;
      syntheticToken?: string;
    };
    destNumTokensReceived: string;
    sourcePool: string;
  };
  valueCurrency?: string;
  transactionHash?: string;
  toAmount?: FPN;
  toCurrency?: string;
  toPrice?: FPN;
}
const TransactionContainer = styled.div`
  display: grid;
  grid-template-columns: fit-content(20%) 1fr;
  grid-template-rows: ;
  grid-column-gap: 0px;
  grid-row-gap: 0px;
  padding: 10px 20px;
  margin: 10px 20px;
  border: 1px solid ${props => props.theme.border.primary};
  border-radius: ${props => props.theme.borderRadius.m};
`;
const IconContainer = styled.div`
  width: 50px;
  margin-right: 10px;
`;

const DetailContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-template-rows: repeat(2, 1fr);
  grid-column-gap: 0px;
  grid-row-gap: 0px;
  text-align: left;
  .valueUsd,
  .amount {
    text-align: right;
  }
  .timestamp,
  .valueUsd {
    color: ${props => props.theme.text.secondary};
  }
  font-size: ${props => props.theme.font.sizes.m};
  color: ${props => props.theme.text.primary};
  font-family: Krub;
  font-weight: 300;
`;

const ValueContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr fit-content(30%);
  grid-template-rows: ;
  grid-column-gap: 0px;
  grid-row-gap: 0px;
  .symbol {
    margin-left: 5px;
  }
`;

const WrappedIcon = styled(Icon)`
  font-size: ${props => props.theme.font.sizes.xl};
  border-radius: 30px;
  width: 50px;
  height: 50px;
  background: ${props => props.theme.background.secondary};
`;

const capitalizeFirstLetter = (string: string) =>
  string.charAt(0).toUpperCase() + string.slice(1);
export const Transaction: React.FC<TransactionProps> = ({
  timestamp,
  type,
  rv,
  transactionHash,
}) => {
  const handleClose = () => {
    console.log('close me');
  };
  const [showDetails, setShowDetails] = useState(false);
  const myAddress = useReduxSelector(state => state.app.address);
  const assets = useReduxSelector(state => state.assets.list);
  const [eurPrice, setEurPrice] = useState<FPN | undefined>(undefined);

  const syntheticPrice =
    rv.symbol &&
    Object.values(assets).filter(x => x.syntheticSymbol === rv.symbol)[0];
  const collateralPrice =
    rv.collateralToken &&
    Object.values(assets).filter(
      x => x.syntheticSymbol === rv.collateralToken,
    )[0];

  const destPrice =
    rv.destPoolInfo &&
    Object.values(assets).filter(
      x => x.syntheticSymbol === rv.destPoolInfo.symbol,
    )[0];

  const collateralAmount =
    (rv.collateralReceived && FPN.fromWei(rv.collateralReceived)) ||
    (rv.collateralSent && FPN.fromWei(rv.collateralSent)) ||
    new FPN(0);
  const amount =
    (rv.value && FPN.fromWei(rv.value)) ||
    (rv.numTokensReceived && FPN.fromWei(rv.numTokensReceived)) ||
    (rv.numTokensSent && FPN.fromWei(rv.numTokensSent)) ||
    new FPN(0);
  useEffect(() => {
    if (Object.keys(assets).length > 0) {
      //
      if (assets['jEUR/BUSD']?.price) {
        setEurPrice(FPN.fromWei(assets['jEUR/BUSD'].price!));
      }
    }
  }, [assets]);

  return (
    <>
      <TransactionContainer onClick={() => setShowDetails(true)}>
        <IconContainer>
          <WrappedIcon
            icon={
              ICON_MAP[
                type !== 'Transfer'
                  ? 'SWAPPED'
                  : rv.from?.toLowerCase() === myAddress?.toLowerCase()
                  ? 'SEND'
                  : 'RECEIVE'
              ]
            }
          />
        </IconContainer>
        <DetailContainer>
          <div className="name">
            {capitalizeFirstLetter(
              type !== 'Transfer'
                ? type.toLowerCase()
                : rv.from?.toLowerCase() === myAddress?.toLowerCase()
                ? 'Send'
                : 'Receive',
            )}
          </div>
          <ValueContainer className="amount">
            <div className="value">{amount.format(8)}</div>
            <div className="symbol">{rv.symbol}</div>
          </ValueContainer>
          <div className="timestamp">
            {DateTime.fromSeconds(timestamp).toLocaleString(
              DateTime.TIME_SIMPLE,
            )}
          </div>
          {syntheticPrice && (
            <div className="valueUsd">
              {rv.symbol !== 'jEUR'
                ? eurPrice &&
                  amount
                    .mul(FPN.fromWei(syntheticPrice.price!))
                    .div(eurPrice)
                    .format(4)
                : amount.format(4)}
            </div>
          )}
        </DetailContainer>
      </TransactionContainer>
      {showDetails ? (
        <CustomModal
          isOpened
          onClose={handleClose}
          duration={0.2}
          overlayContainerStyle={{
            width: '100%',
            justifyContent: 'end',
          }}
          overlayStyle={{
            height: '510px',
            width: '100%',
          }}
          animation="slideBottom"
        >
          <ModalRoot>
            <CustomCard
              title={
                type === 'Transfer'
                  ? `${
                      rv.from?.toLowerCase() === myAddress?.toLowerCase()
                        ? 'Send'
                        : 'Receive'
                    }`
                  : `${capitalizeFirstLetter(type.toLowerCase())}`
              }
              titleBackground={false}
              onBack={() => {
                setShowDetails(false);
              }}
              pointer={false}
            >
              <ExchangeBoxConfirm error={false}>
                <ConfirmTitle>Amount</ConfirmTitle>
                <ConfirmItem>
                  <Flag flag={rv.symbol as FlagKeys} />
                  <div className="assetAmount">{amount.format(8)}</div>
                  <div className="assetName">{rv.symbol}</div>
                  <div className="usdAmount">
                    (
                    {syntheticPrice &&
                      eurPrice &&
                      new Intl.NumberFormat('de-DE', {
                        style: 'currency',
                        currency: 'EUR',
                        minimumFractionDigits: 2,
                      }).format(
                        parseFloat(
                          rv.symbol !== 'jEUR'
                            ? amount
                                .mul(FPN.fromWei(syntheticPrice.price!))
                                .div(eurPrice)
                                .format(4)
                            : amount.format(4),
                        ),
                      )}
                    )
                  </div>
                </ConfirmItem>
                <ConfirmTitle>
                  {type === 'Mint' || type === 'Exchange' || type === 'Redeem'
                    ? 'For'
                    : type === 'Transfer'
                    ? rv.from?.toLowerCase() === myAddress?.toLowerCase()
                      ? 'To'
                      : 'From'
                    : null}{' '}
                </ConfirmTitle>
                <ConfirmItem>
                  {type === 'Mint' || type === 'Redeem' ? (
                    <>
                      <Flag flag={rv.collateralToken as FlagKeys} />
                      <div className="assetAmount">
                        {collateralAmount.format(8)}
                      </div>
                      <div className="assetName">{rv.collateralToken}</div>
                      <div className="usdAmount">
                        (
                        {collateralPrice &&
                          eurPrice &&
                          new Intl.NumberFormat('de-DE', {
                            style: 'currency',
                            currency: 'EUR',
                            minimumFractionDigits: 2,
                          }).format(
                            parseFloat(
                              rv.symbol !== 'jEUR'
                                ? FPN.fromWei(collateralPrice.price!)
                                    .mul(collateralAmount)
                                    .div(eurPrice)
                                    .format(4)
                                : collateralAmount.format(4),
                            ),
                          )}
                        )
                      </div>
                    </>
                  ) : type === 'Exchange' ? (
                    <>
                      <Flag flag={rv.destPoolInfo.syntheticToken as FlagKeys} />
                      <div className="assetAmount">
                        {FPN.fromWei(rv.destNumTokensReceived).format(8)}
                      </div>
                      <div className="assetName">
                        {rv.destPoolInfo.syntheticToken}
                      </div>
                      <div className="usdAmount">
                        (
                        {destPrice &&
                          rv.symbol !== 'jEUR' &&
                          eurPrice &&
                          new Intl.NumberFormat('de-DE', {
                            style: 'currency',
                            currency: 'EUR',
                            minimumFractionDigits: 2,
                          }).format(
                            parseFloat(
                              FPN.fromWei(destPrice.price!)
                                .mul(FPN.fromWei(rv.destNumTokensReceived))
                                .div(eurPrice)
                                .format(4),
                            ),
                          )}
                        {destPrice &&
                          rv.symbol === 'jEUR' &&
                          new Intl.NumberFormat('de-DE', {
                            style: 'currency',
                            currency: 'EUR',
                            minimumFractionDigits: 2,
                          }).format(
                            parseFloat(
                              FPN.fromWei(rv.destNumTokensReceived).format(4),
                            ),
                          )}
                        )
                      </div>
                    </>
                  ) : type === 'Transfer' ? (
                    rv.from?.toLowerCase() === myAddress?.toLowerCase() ? (
                      <a
                        href={`https://bscscan.com/address/${rv.to}`}
                        target="_blank"
                        rel="noreferrer"
                      >
                        {rv.to!.slice(0, 20)}...
                        {rv.to!.slice(30, 42)}
                      </a>
                    ) : (
                      <a
                        href={`https://bscscan.com/address/${rv.from}`}
                        target="_blank"
                        rel="noreferrer"
                      >
                        {rv.from!.slice(0, 20)}...
                        {rv.from!.slice(30, 42)}
                      </a>
                    )
                  ) : null}{' '}
                </ConfirmItem>
                <ConfirmTitle>Status </ConfirmTitle>
                <ConfirmItem className="success">Success</ConfirmItem>
                <ConfirmTitle>Transaction Hash </ConfirmTitle>
                <ConfirmItem className="txHash">
                  <a
                    href={`https://bscscan.com/tx/${transactionHash}`}
                    target="_blank"
                    rel="noreferrer"
                  >
                    {transactionHash!.slice(0, 20)}...
                    {transactionHash!.slice(30, 42)}
                  </a>
                </ConfirmItem>
              </ExchangeBoxConfirm>
            </CustomCard>
          </ModalRoot>
        </CustomModal>
      ) : null}
    </>
  );
};
