import React, { useEffect, useState } from 'react';
import { Emoji, Flag, Icon, OnMobile, styled } from '@jarvis-network/ui';
import { useReduxSelector } from '@/state/useReduxSelector';
import axios from 'axios';

import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';

import { ExchangeSynthereumToken } from '@jarvis-network/synthereum-config/dist';

import { SupportedSynthereumPairExact } from '@jarvis-network/synthereum-config/dist/supported/all-synthereum-pairs';

import { burnerWalletAssets } from '@/data/assets';
import { ContractParams } from '@jarvis-network/synthereum-ts/dist/core/realms/burner-wallet/interface';

import { useDispatch } from 'react-redux';

import coinAddressValidator from 'coin-address-validator';

import { RELAYER_HOOK } from '../swap/constants';

import {
  AssetSelect,
  Balance,
  ExchangeBox,
  handleKeyPress,
  SwapButton,
  Title,
} from '../swap/style';
import { ChooseAsset } from '../swap/ChooseAsset';

import { LoadingSection, SuccessContainer } from '../swap';
import { LoadingIndicator } from '../LoadingIndicator';

import { ScannerPage } from '../scanner';

import { Max } from './Max';

import {
  ActionButton,
  ActionContainer,
  SendNote,
  ConfirmItem,
  ConfirmTitle,
  CustomCard,
  Footer,
  CustomModal,
  ExchangeBoxConfirm,
  ModalRoot,
  TwoIconsButton,
  InputBox,
  Amount,
} from './style';

const EmojiContainer = styled.div`
  font-size: ${props => props.theme.font.sizes.xxl};
`;

const SendContainer = styled.div`
  z-index: 100;
  .sendChooseAsset {
    height: 100% !important;
  }
  .sendAmountModal {
    div.tabTitle {
      line-height: 26px;
      margin-top: 5px;
    }
  }
  .confirmTransactionModal {
    div.tabTitle {
      line-height: 26px;
      margin-top: 5px;
    }
  }
`;

export const Mobile = styled(OnMobile)`
  width: 100%;
`;
export interface SendProps {
  modal: boolean;
  onClose: (isClose: boolean) => void;
}

const SuccessEmoji = styled(Emoji)`
  height: ${props => props.theme.sizes.row};
  padding: 0px 20px;
  font-size: 100px;
`;

export const Send: React.FC<SendProps> = ({ modal, onClose }) => {
  const dispatch = useDispatch();

  const [inProgress, setInProgress] = useState<boolean>(false);
  const [showSuccess, setShowSuccess] = useState<boolean>(false);
  const [getAmountModal, setAmountModal] = useState<boolean>(false);
  const [getRecipientInfoModal, setRecipientInfoModal] = useState(false);
  const [showConfirm, setConfirm] = useState(false);
  const balances = useReduxSelector(state => state.wallet);
  const assets = useReduxSelector(state => state.assets.list);
  const [showScanner, setShowScanner] = useState(false);

  const [recipientAddress, setRecipientAddress] = useState('');
  const [sendAmount, setSendAmount] = useState('');
  const [sendAmountUSD, setSendAmountUSD] = useState('');
  const payload = useReduxSelector(state => state.transactions.payload);

  const [selectedAsset, setSelectedAsset] = useState<
    ExchangeSynthereumToken | undefined
  >(undefined);

  const sendAsset =
    burnerWalletAssets[
      selectedAsset !== 'BUSD'
        ? (`${selectedAsset}/BUSD` as SupportedSynthereumPairExact)
        : ('BUSD' as SupportedSynthereumPairExact)
    ];
  useEffect(() => {
    if (selectedAsset && assets && sendAmount) {
      if (assets['jEUR/BUSD']?.price) {
        const jEUR = FPN.fromWei(assets['jEUR/BUSD'].price!);
        const assetKey =
          selectedAsset !== 'BUSD'
            ? (`${selectedAsset}/BUSD` as SupportedSynthereumPairExact)
            : ('BUSD' as SupportedSynthereumPairExact);
        const price =
          selectedAsset !== 'jEUR'
            ? FPN.toWei(sendAmount)
                .mul(FPN.fromWei(assets[assetKey]!.price!.toString()))
                .div(jEUR)
            : FPN.toWei(sendAmount);
        setSendAmountUSD(price.format(4));
      }
    }
    if (sendAmount === '') {
      setSendAmountUSD('');
    }
  }, [assets, selectedAsset, sendAmount]);
  const handleSend = () => {
    dispatch({
      type: 'transactions/setPayload',
      payload: {},
    });
    dispatch({
      type: 'CALL_SEND',
      payload: {
        inputToken: selectedAsset,
        inputAmountWei: FPN.toWei(sendAmount).toString(),
        recipientAddress,
      } as ContractParams,
    });
    setInProgress(true);
    setShowSuccess(false);
  };

  const submitData = async (data: any) => {
    if (data && data.op_type !== undefined && inProgress) {
      dispatch({
        type: 'transactions/setPayload',
        payload: {},
      });
      const result = await axios.post(RELAYER_HOOK, data, {
        headers: { 'Content-Type': 'application/json' },
      });

      console.log(result.data);
      setShowSuccess(true);
    }
  };
  useEffect(() => {
    console.log({ payload });
    submitData(payload);
  }, [payload]);

  return (
    <SendContainer>
      {modal && !showScanner ? (
        <Mobile>
          <CustomModal
            overlayClassName="sendChooseAsset"
            isOpened={!!modal}
            onClose={() => {
              // if (!getAmountModal) {
              //   onClose(true);
              // }
            }}
            duration={0.2}
            overlayContainerStyle={{
              width: '100%',
              justifyContent: 'end',
            }}
            overlayStyle={{
              height: '580px',
              width: '100%',
            }}
            animation="slideBottom"
          >
            <ModalRoot>
              <CustomCard
                title="What do you want to send?"
                titleBackground={false}
                pointer={false}
                leftEmoji="Purse"
                onBack={() => {
                  onClose(true);
                }}
              >
                <ChooseAsset
                  hideZeroAsset
                  callback={symbol => {
                    setSelectedAsset(symbol);
                    setRecipientInfoModal(true);
                  }}
                />
              </CustomCard>
            </ModalRoot>
          </CustomModal>
          {/* Recipient Info */}
          <CustomModal
            isOpened={getRecipientInfoModal}
            onClose={() => {
              if (!getAmountModal) {
                onClose(true);
              }
            }}
            duration={0.2}
            overlayContainerStyle={{
              width: '100%',
              justifyContent: 'end',
            }}
            overlayStyle={{
              height: '510px',
              width: '100%',
            }}
            animation="slideBottom"
          >
            <ModalRoot>
              <CustomCard
                title={
                  getRecipientInfoModal ? 'Who is the recipient' : 'Success'
                }
                titleBackground={false}
                onBack={() => {
                  onClose(true);
                  // setConfirm(false);
                  setRecipientInfoModal(false);
                }}
                pointer={false}
                leftEmoji="Ledger"
              >
                <>
                  <InputBox error={false}>
                    <Title>Recipient&#39;s Address </Title>
                    <AssetSelect error={Boolean(false)}>
                      <Amount
                        value={recipientAddress}
                        inputMode="text"
                        placeholder="0x..."
                        onChange={e => setRecipientAddress(e.target.value)}
                      />
                    </AssetSelect>
                  </InputBox>
                  <ExchangeBox error={false}>
                    <ActionContainer>
                      <ActionButton
                        type="primary"
                        inline
                        onClick={() => {
                          navigator.clipboard
                            .readText()
                            .then(text => {
                              setRecipientAddress(text);
                            })
                            .catch(err => {
                              console.error(
                                'Failed to read clipboard contents: ',
                                err,
                              );
                            });
                        }}
                        inverted
                      >
                        <EmojiContainer>
                          <Emoji emoji="PaperClip" />
                        </EmojiContainer>{' '}
                        Paste
                      </ActionButton>
                      <ActionButton
                        type="primary"
                        onClick={() => {
                          setShowScanner(true);
                        }}
                        inverted
                      >
                        <EmojiContainer>
                          <Emoji emoji="Camera" />
                        </EmojiContainer>{' '}
                        Scan
                      </ActionButton>
                    </ActionContainer>
                    <SendNote>Paste address, type ens or scan QR code</SendNote>
                  </ExchangeBox>
                  <Footer>
                    <SwapButton
                      disabled={
                        !coinAddressValidator.validate(
                          recipientAddress.toLowerCase(),
                          'eth',
                        )
                      }
                      onClick={() => {
                        setAmountModal(true);
                        setRecipientInfoModal(false);
                      }}
                      type="success"
                      size="l"
                    >
                      Next
                    </SwapButton>
                  </Footer>
                </>
              </CustomCard>
            </ModalRoot>
          </CustomModal>
          {/* Amount Info */}
          <CustomModal
            isOpened={getAmountModal}
            overlayClassName="sendAmountModal"
            onClose={() => {
              if (!getAmountModal && !showConfirm) {
                onClose(true);
              }
            }}
            duration={0.2}
            overlayContainerStyle={{
              width: '100%',
              justifyContent: 'end',
            }}
            overlayStyle={{
              height: '510px',
              width: '100%',
            }}
            animation="slideBottom"
          >
            <ModalRoot>
              <CustomCard
                title={
                  getAmountModal ? 'How much do you want to send?' : 'Success'
                }
                titleBackground={false}
                onBack={() => {
                  if (getAmountModal && !showConfirm) {
                    setAmountModal(false);
                    onClose(true);
                  }
                }}
                pointer={false}
                leftEmoji="MoneyBag"
              >
                <>
                  <ExchangeBox error={false}>
                    <Title>{selectedAsset} </Title>
                    <Balance>
                      {' '}
                      {balances &&
                        selectedAsset &&
                        `Balance: ${FPN.fromWei(
                          balances[selectedAsset!]!.amount,
                        ).format(8)}`}
                    </Balance>
                    <AssetSelect error={Boolean(false)}>
                      <Amount
                        value={sendAmount}
                        inputMode="text"
                        disabled={false}
                        placeholder="0.000"
                        onKeyPress={e =>
                          handleKeyPress(e, {
                            decimals: assets[sendAsset!.pair]!.decimals!,
                          })
                        }
                        onChange={e => {
                          setSendAmount(e.target.value.replace(',', '.'));
                        }}
                      />
                      <Max
                        handleClick={() => {
                          setSendAmount(
                            FPN.fromWei(
                              balances[selectedAsset!]!.amount,
                            ).format(assets[sendAsset!.pair]!.decimals!),
                          );
                        }}
                      />
                    </AssetSelect>
                  </ExchangeBox>
                  <TwoIconsButton>
                    <Icon icon="IoIosRemove" />
                    <Icon icon="IoIosRemove" />
                    <br />
                    <Icon icon="IoIosRemove" />
                    <Icon icon="IoIosRemove" />
                  </TwoIconsButton>

                  <ExchangeBox error={false}>
                    <Title>Equivalent </Title>
                    <AssetSelect error={Boolean(false)}>
                      <Amount
                        inputMode="text"
                        value={new Intl.NumberFormat('de-DE', {
                          style: 'currency',
                          currency: 'EUR',
                          minimumFractionDigits: 2,
                        }).format(
                          parseFloat(
                            sendAmountUSD !== '' ? sendAmountUSD : '0',
                          ),
                        )}
                        placeholder="0.000"
                      />
                    </AssetSelect>
                  </ExchangeBox>
                  <Footer>
                    <SwapButton
                      onClick={() => setConfirm(true)}
                      type="success"
                      size="l"
                    >
                      Send
                    </SwapButton>
                  </Footer>
                </>
              </CustomCard>
            </ModalRoot>
          </CustomModal>
          {/* Confirm screen */}
          <CustomModal
            isOpened={showConfirm}
            overlayClassName="confirmTransactionModal"
            onClose={() => null}
            duration={0.2}
            overlayContainerStyle={{
              width: '100%',
              justifyContent: 'end',
            }}
            overlayStyle={{
              height: '510px',
              width: '100%',
            }}
            animation="slideBottom"
          >
            <ModalRoot>
              <CustomCard
                title={
                  showSuccess ? 'Success' : 'Please confirm your transaction'
                }
                titleBackground={false}
                onBack={() => {
                  onClose(true);
                  setRecipientInfoModal(false);
                  setAmountModal(false);
                  setConfirm(false);
                  setSendAmount('');
                  setSendAmountUSD('');
                  setShowSuccess(false);
                  setInProgress(false);
                }}
                pointer={false}
                leftEmoji={showSuccess ? 'CheckMark' : 'FountainPen'}
              >
                {showSuccess ? (
                  <div>
                    <SuccessContainer>
                      <SuccessEmoji emoji="Money" />
                    </SuccessContainer>
                    <div>Your Transaction had been submitted </div>
                  </div>
                ) : (
                  <ExchangeBoxConfirm error={false}>
                    <ConfirmTitle>You send </ConfirmTitle>
                    <ConfirmItem>
                      {sendAsset && <Flag flag={sendAsset!.assetOut.name!} />}
                      <div className="assetAmount">
                        {sendAmount !== '' && FPN.toWei(sendAmount).format(4)}
                      </div>
                      <div className="assetName">{`${selectedAsset}`}</div>
                      <div className="usdAmount">
                        (
                        {new Intl.NumberFormat('de-DE', {
                          style: 'currency',
                          currency: 'EUR',
                          minimumFractionDigits: 2,
                        }).format(parseFloat(sendAmountUSD))}
                        )
                      </div>
                    </ConfirmItem>
                    <ConfirmTitle>To </ConfirmTitle>
                    <ConfirmItem>
                      <div className="assetAmount">
                        {recipientAddress.slice(0, 20)}...
                        {recipientAddress.slice(30, 42)}
                      </div>
                    </ConfirmItem>
                  </ExchangeBoxConfirm>
                )}

                {!inProgress ? (
                  <SwapButton
                    disabled={false}
                    type="success"
                    onClick={handleSend}
                    size="l"
                  >
                    Confirm
                  </SwapButton>
                ) : showSuccess ? (
                  <SwapButton
                    disabled={false}
                    type="success"
                    onClick={() => {
                      onClose(true);
                      setRecipientInfoModal(false);
                      setAmountModal(false);
                      setConfirm(false);
                      setSendAmount('');
                      setSendAmountUSD('');
                      setShowSuccess(false);
                      setInProgress(false);
                    }}
                    size="l"
                  >
                    Done
                  </SwapButton>
                ) : (
                  <LoadingSection>
                    <LoadingIndicator />
                  </LoadingSection>
                )}
              </CustomCard>
            </ModalRoot>
          </CustomModal>
        </Mobile>
      ) : showScanner ? (
        <ScannerPage
          onClose={() => {
            setShowScanner(false);
          }}
          onScan={(data: string) => {
            if (data && data.toString()) {
              if (data.toString().indexOf('ethereum:') !== -1) {
                setRecipientAddress(data.replace('ethereum:', '').slice(0, 42));
                setShowScanner(false);
                return;
              }
              if (data.toString().indexOf('0x') === -1) {
                setRecipientAddress(`0x${data}`);
              }
              setRecipientAddress(data);
            }
            setShowScanner(false);
          }}
        />
      ) : null}
    </SendContainer>
  );
};
