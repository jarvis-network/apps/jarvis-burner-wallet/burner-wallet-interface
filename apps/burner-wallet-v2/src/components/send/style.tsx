import { Button, CardAssets, Modal, styled } from '@jarvis-network/ui';

export const ModalRoot = styled.div`
  background: ${props => props.theme.scroll.background};
  color: ${props => props.theme.text.primary};
  border-radius: 20px;
  width: 100%;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  position: absolute;
  height: 60%;
  bottom: 0px;
  border-bottom-left-radius: 0;
  border-bottom-right-radius: 0;
`;

export const InputBox = styled.div<{ error: boolean }>`
  margin: 5px 15px;
  display: grid;
  grid-template-columns: auto;
  grid-template-rows: auto;

  grid-template-areas:
    'title'
    'asset-select'
    'error'
    'value';
  position: relative;
  margin-top: 15px;
  width: 90%;
`;

export const Amount = styled.input`
  grid-area: amount;
  border: none;
  padding: none;
  background: none;
  color: ${props => props.theme.text.secondary};
  font-size: ${props => props.theme.font.sizes.l};
  border-radius: ${props => props.theme.borderRadius.m};

  width: 100%;
  outline: none !important;
  margin-top: 5px;
  height: 100%;
  font-family: Krub;

  &::placeholder {
    color: currentColor;
  }
`;

export const Footer = styled.div`
  width: 100%;
`;
export const ActionContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;

  grid-template-rows: 1fr;
  grid-column-gap: 10px;
  grid-row-gap: 0px;

  & button {
    width: 100%;
    text-align: center;
  }
`;

export const ActionButton = styled(Button)`
  color: ${props => props.theme.text.primary};
  border-radius: ${props => props.theme.borderRadius.m};
  border: 1px ${props => props.theme.border.primary} solid;
  font-size: ${props => props.theme.font.sizes.l};

  font-weight: 100;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: 1fr;
  grid-column-gap: 10px;
  grid-row-gap: 0px;
  text-align: left;
  padding: 10px 20px;
  div {
    text-align: right;
  }
`;

export const SendNote = styled.div`
  color: ${props => props.theme.text.secondary};
  text-align: left;
  font-size: ${props => props.theme.font.sizes.s};
  margin-top: 15px;
`;

export const ConfirmTitle = styled.div`
  font-size: ${props => props.theme.font.sizes.m};
  padding: 5px 15px;
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
`;

export const ConfirmItem = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-content: stretch;
  padding: 0px 20px;
  flex-grow: 1
  align-items: center;
  width: fit-content
  > div {
    width: fit-content
  }

  img {
    width: 22px;
    height: 22px;
    object-fit: contain;
    vertical-align: middle;
  }

  i svg {
    width: 11px;
    height: 11px;
  }

  .assetName,
  .assetAmount {
    display: inline-flex;
    justify-content: space-between;
    vertical-align: middle;
    padding-right:3px;
    margin-left: 8px;
    font-size: ${props => props.theme.font.sizes.l};
    font-family: Krub;
    font-weight: 300;
  }
  .usdAmount{
    line-height:25px;
    color: ${props => props.theme.text.secondary};
  }
`;

export const ExchangeBoxConfirm = styled.div<{ error: boolean }>`
  margin: 30px 0px;
  display: grid;
  grid-template-columns: 1fr;

  grid-column-gap: 0px;
  grid-auto-rows: minmax(min-content, max-content);

  position: relative;
  margin-top: 15px;

  overflow: hidden;

  > div:nth-child(2) {
    padding-top: 5px;
    padding-bottom: 15px;
    border-bottom: 1px solid ${props => props.theme.border.primary};
  }
  > div:nth-child(4) {
    padding-top: 5px;
    padding-bottom: 15px;
  }
`;

export const CustomCard = styled(CardAssets)`
  @media screen and (max-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex - 1]}px) {
    border-radius: 0 !important;
    text-align: center;

    > *:first-child {
      border-radius: 0 !important;
    }

    > *:last-child {
      border-radius: 0 !important;
      overflow: scroll;
    }
  }
`;

export const CustomModal = styled(Modal)`
  width: 100%;
  text-align: center;
  bottom: 0px;
`;

export const TwoIconsButton = styled.button`
  border: none;
  padding: 0;
  background: none;
  cursor: pointer;
  outline: none !important;
  width: 100%;
  transform: translateY(14px);

  svg {
    width: 24px;
    height: 24px;
    position: absolute;
    margin-left: -8px;
    margin-right: -8px;
    fill: ${props => props.theme.text.secondary};
  }
  padding-bottom: 10px;
`;
