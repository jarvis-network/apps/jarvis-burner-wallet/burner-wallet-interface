import React from 'react';
import QRCode from 'qrcode.react';
import { styled } from '@jarvis-network/ui';

const StyledQRCode = styled(QRCode)`
  height: auto !important;
  width: 100% !important;
  filter: blur(1px);
`;

const FallbackQRCode: React.FC<any> = ({ defaultAccount }) =>
  defaultAccount ? <StyledQRCode value={defaultAccount} /> : null;

export default FallbackQRCode;
