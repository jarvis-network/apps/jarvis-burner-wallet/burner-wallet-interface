import React from 'react';
import { Button, Icon, styled } from '@jarvis-network/ui';

import { Zoomer } from './Zoomer';
import FallbackQRCode from './FallbackQRCode';

const Wrapper = styled.div`
  position: absolute;
  width: 100%;
  top: 0;
  height: 100%;
  left: 0;
  display: flex;
  flex-direction: column;

  .top,
  .left,
  .bottom,
  .right,
  .loader {
    background: rgba(0, 0, 0, 0.5);
  }

  .top {
    flex: 1;
    z-index: 999;
  }

  .row {
    flex: 1;
    display: flex;
    flex-direction: row;
    min-height: 250px;
    max-height: 250px;
    .left {
      flex: 1;
    }

    .center {
      width: 250px;
      height: 250px;
      z-index: 10;
      display: flex;
    }

    .right {
      flex: 1;
    }
  }

  .bottom {
    display: flex;
    justify-content: center;
    align-items: center;
    flex: 1;
  }
`;

const InstructionsContainer = styled.div`
  display: flex;
  width: 100px;
  justify-content: space-around;
  align-items: center;
`;

const Loader = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  color: #fff;

  @keyframes rotate {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }
  i {
    -webkit-transition: all 0.5s ease-in;
    -webkit-animation-name: rotate;
    -webkit-animation-duration: 1s;
    -webkit-animation-iteration-count: infinite;
    -webkit-animation-timing-function: linear;

    transition: all 0.5s ease-in;
    animation-name: rotate;
    animation-duration: 1s;
    animation-iteration-count: infinite;
    animation-timing-function: linear;
  }
`;

interface OverlayProps {
  loading: boolean;
  legacyMode: boolean;
  reader: React.RefObject<any>;
  readSuccessFully: boolean;
  onClose: () => void;
}
const TopContainer = styled.div`
  color: white;
  flex: 1;
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  padding: 10px;
  font-size: ${props => props.theme.font.sizes.m};
  font-weight: 300;
  line-height: ${props => props.theme.font.sizes.m};
  span {
    padding: 0px 5px;
  }
`;
const Overlay: React.FC<OverlayProps> = ({
  loading,
  legacyMode,
  reader,
  readSuccessFully,
  onClose,
}) => (
  <Wrapper>
    <TopContainer
      onClick={() => {
        onClose();
      }}
      className="top"
    >
      <Icon icon="BsArrowLeft" /> <span>Back</span>
    </TopContainer>
    <div className="row">
      <div className="left" />
      <div className="center">
        {loading ? (
          <Loader className="loader">
            <Icon icon="AiOutlineLoading3Quarters" />
          </Loader>
        ) : !legacyMode ? (
          <Zoomer color={readSuccessFully ? '#4efa74' : '#fff'} />
        ) : (
          <FallbackQRCode />
        )}
      </div>
      <div className="right" />
    </div>
    <div className="bottom">
      {legacyMode && (
        <InstructionsContainer>
          <span>Upload a photo of a QR code</span>
          <Button onClick={() => reader.current!.openImageDialog()}>
            Upload
          </Button>
        </InstructionsContainer>
      )}
    </div>
  </Wrapper>
);

export default Overlay;
