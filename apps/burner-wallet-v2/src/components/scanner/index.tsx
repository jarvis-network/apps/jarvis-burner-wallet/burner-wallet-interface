import { styled } from '@jarvis-network/ui';

import React, { useRef, useState } from 'react';
import dynamic from 'next/dynamic';

import Overlay from './Overlay';

const QrReader = dynamic(() => import('react-qr-reader'), { ssr: false });

interface ContentContainerProps {
  readerImageLoaded: boolean;
  facemode: boolean;
}

const ContentContainer = styled.div<ContentContainerProps>`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  z-index: 999;
  .reader {
    display: ${({ readerImageLoaded }) =>
      !readerImageLoaded ? 'none' : 'block'};
  }

  video {
    transform: ${({ facemode }) => (facemode ? 'scaleX(-1)' : 'none')};
  }
`;

const StyledQrReader = styled(QrReader)`
  width: 100%;

  section {
    position: unset !important;

    div {
      border: none !important;
      box-shadow: none !important;
    }
  }
`;

export interface ScannerProps {
  onScan: (result: string) => void;
  onClose: () => void;
  loading?: boolean;
}

export const ScannerPage: React.FC<ScannerProps> = ({
  loading = false,
  onScan,
  onClose,
}: ScannerProps) => {
  const reader = useRef<any>(null);
  const [isLoading, setIsLoading] = useState(true);
  const [legacyMode, setLegacyMode] = useState(false);
  const [legacyModeImageLoaded, setLegacyModeImageLoaded] = useState(false);
  const [readSuccessFully, setReadSuccessFully] = useState(false);
  return (
    <ContentContainer
      readerImageLoaded={legacyModeImageLoaded || !legacyMode}
      facemode={false}
    >
      <StyledQrReader
        ref={reader}
        delay={300}
        onScan={(data: any) => {
          if (readSuccessFully && data === null) {
            setReadSuccessFully(false);
          }
          if (data === null) return;
          onScan(data);
          setReadSuccessFully(true);
        }}
        onError={e => {
          console.log({ e });
          setIsLoading(false);
          setLegacyMode(true);
        }}
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore (wrong react-qr-scanner types)
        onLoad={_ref => {
          // if (/front|user|face/gi.test(streamLabel)) setFacemode(true);
          setIsLoading(false);
        }}
        onImageLoad={() => setLegacyModeImageLoaded(true)}
        facingMode="environment"
        legacyMode={legacyMode}
        className="reader"
      />
      <Overlay
        loading={loading || isLoading}
        legacyMode={loading ? false : legacyMode}
        reader={reader}
        readSuccessFully={readSuccessFully}
        onClose={onClose}
      />
    </ContentContainer>
  );
};
