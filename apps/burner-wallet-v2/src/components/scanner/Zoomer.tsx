import React from 'react';

interface ZoomerProps {
  color: string;
}
export const Zoomer: React.FC<ZoomerProps> = ({ color = 'white' }) => (
  <svg viewBox="0 0 250 250" height="250px">
    <path d="M50,2 L2,2 L2,50" fill="none" stroke={color} strokeWidth="3" />
    <path
      d="M2,198 L2,248 L50,248"
      fill="none"
      stroke={color}
      strokeWidth="3"
    />
    <path
      d="M198,248 L248,248 L248,198"
      fill="none"
      stroke={color}
      strokeWidth="3"
    />
    <path
      d="M248,50 L248,2 L198,2"
      fill="none"
      stroke={color}
      strokeWidth="3"
    />
  </svg>
);

export const SuccessZoomer: React.FC = () => (
  <svg viewBox="0 0 250 250" height="250px">
    <path d="M50,2 L2,2 L2,50" fill="none" stroke="#4efa74" strokeWidth="3" />
    <path
      d="M2,198 L2,248 L50,248"
      fill="none"
      stroke="#4efa74"
      strokeWidth="3"
    />
    <path
      d="M198,248 L248,248 L248,198"
      fill="none"
      stroke="#4efa74"
      strokeWidth="3"
    />
    <path
      d="M248,50 L248,2 L198,2"
      fill="none"
      stroke="#4efa74"
      strokeWidth="3"
    />
  </svg>
);
