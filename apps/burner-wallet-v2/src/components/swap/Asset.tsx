import React from 'react';
import { useDispatch } from 'react-redux';
import { Flag, Icon, styled, themeValue } from '@jarvis-network/ui';

import { burnerWalletAssets } from '@/data/assets';
import { AssetType } from '@/state/initialState';
import { setChooseAsset } from '@/state/slices/exchange';
import { useReduxSelector } from '@/state/useReduxSelector';

import { SupportedSynthereumPairExact } from '@jarvis-network/synthereum-config/dist/supported/all-synthereum-pairs';

const Container = styled.div`
  grid-area: asset;
  justify-self: end;
  margin-top: 5px;
  font-size: ${props => props.theme.font.sizes.m};
  width: 100px;

  img {
    width: 22px;
    height: 22px;
    object-fit: contain;
    vertical-align: middle;
  }

  i svg {
    width: 11px;
    height: 11px;
  }

  .assetName {
    display: inline-flex;
    justify-content: space-between;
    vertical-align: middle;
    width: 65px;
    margin-left: 8px;
    font-size: ${props => props.theme.font.sizes.l};
    font-family: Krub;
    font-weight: 300;
  }
`;

const AssetChangeButton = styled.button`
  border: none;
  padding: 0;
  display: inline-block;
  background: none;
  cursor: pointer;
  outline: none !important;
  color: ${props => props.theme.text.primary};
`;

const AssetSelectButton = styled.button`
  background: red;
  border: none;
  padding: 0 6px;
  height: 26px;
  font-size: ${props => props.theme.font.sizes.xxs};
  color: ${themeValue(
    {
      light: theme => theme.text.secondary,
    },
    theme => theme.text.primary,
  )};
  background: ${props => props.theme.background.disabled};
  outline: none !important;
  margin-right: -5px;
  cursor: pointer;
  font-family: Krub;
  font-weight: 300;

  i {
    color: ${themeValue(
      {
        light: theme => theme.text.secondary,
      },
      theme => theme.text.primary,
    )}!important;
    vertical-align: middle;

    svg {
      width: 11px;
      height: 11px;
    }
  }
`;

interface Props {
  type: AssetType;
}

export const Asset: React.FC<Props> = ({ type }) => {
  const dispatch = useDispatch();

  const assetSymbol = useReduxSelector(state =>
    type === 'pay' ? state.exchange.payAsset : state.exchange.receiveAsset,
  );

  const asset =
    burnerWalletAssets[
      assetSymbol !== 'BUSD'
        ? (`${assetSymbol}/BUSD` as SupportedSynthereumPairExact)
        : ('BUSD' as SupportedSynthereumPairExact)
    ];
  const handleChooseAsset = () => dispatch(setChooseAsset(type));
  // console.log(asset);
  if (asset) {
    const flag = asset.assetOut.name ? (
      <Flag flag={asset.assetOut.name} />
    ) : null;

    return (
      <Container>
        <AssetChangeButton onClick={handleChooseAsset}>
          {flag}
          <div className="assetName">
            {asset.assetOut.name} <Icon icon="BsChevronDown" />
          </div>
        </AssetChangeButton>
      </Container>
    );
  }

  return (
    <Container>
      <AssetSelectButton onClick={handleChooseAsset}>
        Select an asset <Icon icon="BsChevronDown" />
      </AssetSelectButton>
    </Container>
  );
};
