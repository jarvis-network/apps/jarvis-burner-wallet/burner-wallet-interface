export const MAX_ALLOWANCE_IN_ETH = 1000000000000;
export type SWAP_TYPES = 'MINT' | 'EXCHANGE' | 'REDEEM';
export const RELAYER_HOOK =
  'https://api.defender.openzeppelin.com/autotasks/8f8066ef-9146-4143-bf92-5d2b00809c50/runs/webhook/199613b7-8b9f-49c9-8d40-f37483c57ad8/UKz8R45afzjz9VgiaRbPjm';

export const RELAYER = '0xca777b46b9eb34a862307f15301eb3811f4ad7a4';
