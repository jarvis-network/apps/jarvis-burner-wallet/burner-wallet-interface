import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { CellInfo } from 'react-table';
import type { RowInfo } from 'react-table';

import {
  ColumnType,
  DataGrid,
  Flag,
  noColorGrid,
  styled,
  themeValue,
} from '@jarvis-network/ui';
import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';

import {
  setChooseAsset,
  setPayAsset,
  setReceiveAsset,
} from '@/state/slices/exchange';
import { useReduxSelector } from '@/state/useReduxSelector';
import { Asset, AssetWithWalletInfo, burnerWalletAssets } from '@/data/assets';

import { ExchangeSynthereumToken } from '@jarvis-network/synthereum-config/dist/supported/all-synthereum-pairs';

const grid = {
  columns: [
    {
      key: 'flag',
      type: ColumnType.CustomCell,
      cell: ({ original }: CellInfo) => {
        const o = original as Asset;
        if (!o.assetOut.name) {
          return null;
        }
        return <Flag flag={o.assetOut.name} />;
      },
      className: 'flag',
    },
    {
      key: 'syntheticSymbol',
      type: ColumnType.Text,
      className: 'asset',
    },
    {
      key: 'value',
      type: ColumnType.CustomCell,
      className: 'number',
      cell: ({ original }: CellInfo) => {
        const o = original as AssetWithWalletInfo;

        const stableValue = o.stableCoinValue && (
          <div className="dollars">
            $ {FPN.fromWei(o.stableCoinValue.toString()).format(2)}
          </div>
        );
        return (
          <>
            <div className="value">
              {FPN.fromWei(o.ownedAmount.toString()).format(8)}
            </div>
            {stableValue}
          </>
        );
      },
    },
  ],
};

const StyledGrid = styled(DataGrid)`
  .text,
  .asset,
  .flag {
    text-align: left;
    padding: 8px 16px !important;
  }

  .number {
    text-align: right;
    padding: 8px 16px !important;
  }

  .asset,
  .number .value {
    color: ${props => props.theme.text.primary};
    font-size: ${props => props.theme.font.sizes.m};
  }

  .flag {
    flex-grow: 0 !important;
    width: auto !important;
    padding-right: 0 !important;
    display: flex;

    img {
      width: 24px;
      height: 24px;
    }
  }

  .rt-tbody {
    .rt-tr-group:first-child {
      border-top: none !important;
    }

    .rt-tr-group {
      border-color: ${props => props.theme.border.secondary}!important;
    }
  }

  .rt-table {
    overflow: hidden;
    .rt-tr {
      align-items: center;
      display: flex;
    }
  }

  .dollars {
    color: ${themeValue(
      {
        light: theme => theme.text.secondary,
      },
      theme => theme.text.medium,
    )};
  }

  ${noColorGrid()}
`;
export interface ChooseAssetProps {
  callback?: (data: any) => void;
  hideZeroAsset?: boolean;
}

export const ChooseAsset: React.FC<ChooseAssetProps> = ({
  callback,
  hideZeroAsset = false,
}) => {
  const dispatch = useDispatch();
  const balances = useReduxSelector(state => state.wallet);
  const [list, setList] = useState<unknown[]>([]);

  const assets = useReduxSelector(state => state.assets.list);
  const [eurPrice, setEurPrice] = useState<FPN | undefined>(undefined);
  useEffect(() => {
    if (Object.keys(balances).length > 0 && Object.keys(assets).length > 0) {
      //
      if (assets['jEUR/BUSD']?.price) {
        setEurPrice(FPN.fromWei(assets['jEUR/BUSD'].price!));
      }
      const t = Object.values(assets).map(
        (asset): AssetWithWalletInfo => {
          const ownedAmount =
            FPN.fromWei(balances[asset.syntheticSymbol!]!.amount) ||
            new FPN('0');
          const price = FPN.fromWei(asset.price!);

          return {
            ...burnerWalletAssets[asset.pair],
            ...asset,

            stableCoinValue:
              price && eurPrice && asset.syntheticSymbol !== 'jEUR'
                ? ownedAmount.mul(price).div(eurPrice)
                : ownedAmount,
            ownedAmount,
            parsed: parseFloat(ownedAmount.format(8)),
          };
        },
      );
      setList(t);
    }
  }, [assets, balances]);

  const asset = useReduxSelector(state => state.exchange.chooseAssetActive);

  const onBack = () => dispatch(setChooseAsset(null));

  const handleSelected = (symbol: ExchangeSynthereumToken) => {
    dispatch(asset === 'pay' ? setPayAsset(symbol) : setReceiveAsset(symbol));
    onBack();
    if (callback) {
      callback(symbol);
    }
  };

  const getTrProps = (_: any, rowInfo?: RowInfo) => ({
    onClick: () =>
      handleSelected(
        rowInfo!.original.syntheticSymbol as ExchangeSynthereumToken,
      ),
    style: {
      cursor: 'pointer',
    },
  });

  return (
    <StyledGrid
      columns={grid.columns}
      data={list!
        .filter((x: any) => {
          if (hideZeroAsset) {
            return !!x.ownedAmount.gt(new FPN(0));
          }
          return true;
        })
        .sort((a: any, b: any) =>
          a.parsed < b.parsed ? 1 : b.parsed < a.parsed ? -1 : 0,
        )}
      showPagination={false}
      loading={false}
      getTrProps={getTrProps}
      pageSize={list && list.length}
    />
  );
};
