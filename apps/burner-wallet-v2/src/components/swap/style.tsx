import { Button, CardAssets, Label, Modal, styled } from '@jarvis-network/ui';

export const Form = styled.div`
  flex: 1;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  display: flex;
`;

export const ExchangeBox = styled.div<{ error: boolean }>`
  margin: 5px 15px;
  display: grid;
  grid-template-columns: auto;
  grid-template-rows: auto;

  grid-template-areas:
    'title'
    'asset-select'
    'error'
    'value';
  position: relative;
  margin-top: 15px;
  width: 90%;
`;

export const ExchangeBoxConfirm = styled.div<{ error: boolean }>`
  margin: 30px 0px;
  display: grid;
  grid-template-columns: 1fr;

  grid-column-gap: 0px;
  grid-auto-rows: minmax(min-content, max-content);

  position: relative;
  margin-top: 15px;
  width: 90%;

  > div:nth-child(2) {
    padding-top: 5px;
    padding-bottom: 15px;
    border-bottom: 1px solid ${props => props.theme.border.primary};
  }
  > div:nth-child(4) {
    padding-top: 5px;
    padding-bottom: 15px;
  }
`;

export const Balance = styled.div`
  color: ${props => props.theme.text.secondary};
  text-align: right;
  font-size: 18px;
  margin-right: 15px;
  grid-area: title;
`;

export const AssetSelect = styled.div<{ error: boolean }>`
  grid-area: asset-select;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 5px 10px 10px 10px;
  height: ${props => props.theme.sizes.row};
  box-sizing: border-box;
  margin-top: 3px;
  border: 1px solid
    ${props =>
      !props.error ? props.theme.border.secondary : props.theme.border.invalid};
  border-radius: ${props => props.theme.borderRadius.m};
`;

export const Amount = styled.input`
  grid-area: amount;
  border: none;
  padding: none;
  background: none;
  color: ${props => props.theme.text.secondary};
  font-size: ${props => props.theme.font.sizes.l};
  border-radius: ${props => props.theme.borderRadius.m};

  width: 45%;
  outline: none !important;
  margin-top: 5px;
  height: 100%;
  font-family: Krub;

  &::placeholder {
    color: currentColor;
  }
`;

const allowedKeys = '0123456789.'.split('');

export const handleKeyPress = (
  e: React.KeyboardEvent<HTMLInputElement>,
  asset: { decimals: number },
) => {
  const somethingSelected =
    e.currentTarget.selectionStart !== e.currentTarget.selectionEnd;
  const parts = e.currentTarget.value.split('.');
  const decimals = parts[1] || '';
  const num = parts[0] || '';

  if (
    !allowedKeys.includes(e.key) ||
    (e.key === '.' && e.currentTarget.value.includes('.')) ||
    (decimals.length > asset.decimals && !somethingSelected)
  ) {
    console.log(decimals.length, e.currentTarget.value, num.length);
    console.log('prevented input');
    e.preventDefault();
  }
};

export const SwapButton = styled(Button)`
  font-size: 20px;
  font-weight: normal;
  font-family: 'Krub';
  width: 90%;
  text-align: center;
  margin-top: 5px;
  border-radius: ${props => props.theme.borderRadius.m};
  height: ${props => props.theme.sizes.row};
  box-shadow: none;
`;
export const Footer = styled.div`
  margin: 5px 15px 15px;
  width: 100%;
`;

export const PageTitle = styled(Label)`
  font-size: ${props => props.theme.font.sizes.xl};
  font-weight: 500;
  text-align: left;
  padding: 10px 20px;
  display: block;
  font-weight: normal;
`;

export const Title = styled.div`
  font-size: ${props => props.theme.font.sizes.m};
  grid-area: title;
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
`;

export const ContentContainer = styled.div`
  background: ${props => props.theme.background.primary};
  height: 100%;
  border-radius: 0 0 ${props => props.theme.borderRadius.m}
    ${props => props.theme.borderRadius.m};
`;

export const CustomCard = styled(CardAssets)`
  @media screen and (max-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex - 1]}px) {
    border-radius: 0 !important;

    > *:first-child {
      border-radius: 0 !important;
    }

    > *:last-child {
      border-radius: 0 !important;
      overflow: scroll;
      height: calc(100% - 60px);
    }
  }
`;

export const CustomModal = styled(Modal)`
  width: 100%;
`;

export const ModalRoot = styled.div`
  background: ${props => props.theme.scroll.background};
  color: ${props => props.theme.text.primary};
  border-radius: 20px;
  height: 560px;
  width: 100%;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  position: absolute;
  height: 70%;
  bottom: 0px;
  border-bottom-left-radius: 0;
  border-bottom-right-radius: 0;
`;

export const ConfirmTitle = styled.div`
  font-size: ${props => props.theme.font.sizes.m};
  padding: 5px 15px;
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
`;

export const ConfirmItem = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-content: stretch;
  padding: 0px 20px;
  flex-grow: 1
  align-items: center;
  width: fit-content
  > div {
    width: fit-content
  }

  img {
    width: 22px;
    height: 22px;
    object-fit: contain;
    vertical-align: middle;
  }

  i svg {
    width: 11px;
    height: 11px;
  }

  .assetName,
  .assetAmount {
    display: inline-flex;
    justify-content: space-between;
    vertical-align: middle;
    margin-left: 8px;
    font-size: ${props => props.theme.font.sizes.l};
    font-family: Krub;
    font-weight: 300;
  }
`;
