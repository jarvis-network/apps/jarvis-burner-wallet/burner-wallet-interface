/* eslint-disable camelcase */

import { Emoji, Flag, Icon, styled } from '@jarvis-network/ui';

import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { TwoIconsButton } from '@/components/TwoIconsButton';

import { useDispatch } from 'react-redux';

import {
  setBase,
  setChooseAsset,
  setPay,
  setPayAsset,
  setReceive,
  setReceiveAsset,
} from '@/state/slices/exchange';

import { useReduxSelector } from '@/state/useReduxSelector';
import { formatExchangeAmount } from '@jarvis-network/app-toolkit';

import { useExchangeValues } from '@/utils/useExchangeValues';

import { State } from '@/state/initialState';

import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';

import { burnerWalletAssets } from '@/data/assets';

import { SupportedSynthereumPairExact } from '@jarvis-network/synthereum-config/dist/supported/all-synthereum-pairs';

import { ContractParams } from '@jarvis-network/synthereum-ts/dist/core/realms/burner-wallet/interface';

import { LoadingIndicator } from '../LoadingIndicator';

import { ChooseAsset } from './ChooseAsset';

import { Asset } from './Asset';
import { Max } from './Max';
import {
  Amount,
  AssetSelect,
  Balance,
  ConfirmItem,
  ConfirmTitle,
  ContentContainer,
  CustomCard,
  CustomModal,
  ExchangeBox,
  ExchangeBoxConfirm,
  Footer,
  Form,
  handleKeyPress,
  ModalRoot,
  PageTitle,
  SwapButton,
  Title,
} from './style';

import { RELAYER_HOOK } from './constants';

export const LoadingSection = styled.div`
  height: 100px;
`;

export const SuccessContainer = styled.div`
  height: 150px;
  margin-top: 50px;
`;

const SuccessEmoji = styled(Emoji)`
  height: ${props => props.theme.sizes.row};
  padding: 0px 20px;
  font-size: 100px;
`;

export const WarningText = styled.div`
  background: ${props => props.theme.background.medium};
  border-radius: ${props => props.theme.borderRadius.s};
  font-size: ${props => props.theme.font.sizes.xs};
  color: ${props => props.theme.text.secondary};
  text-align: center;
  padding: 10px;
  font-weight: normal;
  margin-top: 20px;
`;

export interface ActionProps {
  tabHandler: (input: number) => void;
}

export const SwapComponent: React.FC<ActionProps> = ({ tabHandler }) => {
  const dispatch = useDispatch();
  const [inProgress, setInProgress] = useState<boolean>(false);
  const [showSuccess, setShowSuccess] = useState<boolean>(false);
  const [requiredAllowance, setRequiredAllowance] = useState<boolean>(false);
  const [approveText, setApproveText] = useState('Approve');
  const payload = useReduxSelector(state => state.transactions.payload);
  const chooseAsset = useReduxSelector(
    state => state.exchange.chooseAssetActive,
  );
  const approveTx = useReduxSelector(state => state.transactions.approveTx);
  const [processingApproveTx, setProcessingApproveTx] = useState(false);
  const [showConfirm, setConfirm] = useState(false);
  const balance = useReduxSelector(state => state.wallet);
  const assets = useReduxSelector(state => state.assets.list);
  const {
    base,
    pay,
    receive,
    paySymbol,
    receiveSymbol,
    payString,
    receiveString,
  } = useExchangeValues();

  const updateBase = (baseValue: State['exchange']['base']) => {
    dispatch(setBase(baseValue));
  };

  const updatePay = (inputValue: State['exchange']['pay']) => {
    dispatch(setPay(inputValue));
  };

  const updateReceive = (inputValue: State['exchange']['receive']) => {
    dispatch(setReceive(inputValue));
  };
  const flipValues = () => {
    dispatch(setPayAsset(receiveSymbol));
    dispatch(setReceiveAsset(paySymbol));

    if (base === 'pay') {
      updateBase('receive');
      updateReceive(payString);
      return;
    }
    updateBase('pay');
    updatePay(receiveString);
  };

  const getFormattedPay = () => {
    if (base === 'pay') {
      return pay;
    }

    return formatExchangeAmount(payString);
  };

  const getFormattedReceive = () => {
    if (base === 'receive') {
      return receive;
    }

    return formatExchangeAmount(receiveString);
  };

  const payAsset =
    burnerWalletAssets[
      paySymbol !== 'BUSD'
        ? (`${paySymbol}/BUSD` as SupportedSynthereumPairExact)
        : ('BUSD' as SupportedSynthereumPairExact)
    ];
  const receiveAsset =
    burnerWalletAssets[
      receiveSymbol !== 'BUSD'
        ? (`${receiveSymbol}/BUSD` as SupportedSynthereumPairExact)
        : ('BUSD' as SupportedSynthereumPairExact)
    ];

  const handleClose = () => {
    console.log('close me');
    setConfirm(false);
  };

  const handleSwap = () => {
    dispatch({
      type: 'transactions/setPayload',
      payload: {},
    });

    dispatch({
      type: 'CALL_MINT',
      payload: {
        inputToken: paySymbol,
        outputToken: receiveSymbol,
        inputAmountWei: FPN.toWei(getFormattedPay()!.toString()).toString(),
        outputAmountWei: FPN.toWei(
          getFormattedReceive()!.toString(),
        ).toString(),
      } as ContractParams,
    });
    setInProgress(true);
    setShowSuccess(false);
  };
  const submitData = async (data: any) => {
    if (data && data.op_type !== undefined && inProgress) {
      console.log(data);
      dispatch({
        type: 'transactions/setPayload',
        payload: {},
      });
      const result = await axios.post(RELAYER_HOOK, data, {
        headers: { 'Content-Type': 'application/json' },
      });

      console.log(result.data);
      setShowSuccess(true);
    }
  };
  useEffect(() => {
    submitData(payload);
  }, [payload]);
  const [invalidPair, setInvalidPair] = useState(false);
  useEffect(() => {
    if (receiveAsset && payAsset) {
      if (
        (payAsset.pair === 'BUSD' && receiveAsset.pair === 'BTCB/BUSD') ||
        (payAsset.pair === 'BTCB/BUSD' && receiveAsset.pair === 'BUSD')
      ) {
        setInvalidPair(true);
      } else {
        setInvalidPair(false);
      }
    }

    if (receiveAsset && payAsset && payAsset.pair === 'BUSD') {
      if (
        assets[receiveAsset.pair]?.allowance === '0' ||
        assets[receiveAsset.pair]?.allowance === undefined
      ) {
        setRequiredAllowance(true);
      } else {
        setRequiredAllowance(false);
        setProcessingApproveTx(false);
      }
    } else if (receiveAsset && payAsset && payAsset.pair === 'BTCB/BUSD') {
      if (
        assets[payAsset.pair]?.allowance === '0' ||
        assets[payAsset.pair]?.allowance === undefined
      ) {
        setRequiredAllowance(true);
      } else {
        setRequiredAllowance(false);
        setProcessingApproveTx(false);
      }
    } else {
      setRequiredAllowance(false);
      setProcessingApproveTx(false);
    }
  }, [receiveAsset, payAsset]);
  useEffect(() => {
    if (approveTx) {
      setRequiredAllowance(false);
      setProcessingApproveTx(false);
      setApproveText('Approve');
      dispatch({
        type: 'transactions/approveTx',
        payload: undefined,
      });
    }
  }, [approveTx]);
  return (
    <div>
      <PageTitle>Swap</PageTitle>
      <ContentContainer>
        <Form>
          <ExchangeBox error={false}>
            <Title>You swap </Title>
            <Balance>
              {balance &&
                paySymbol &&
                `Balance: ${FPN.fromWei(balance[paySymbol!]!.amount).format(
                  8,
                )}`}
            </Balance>
            <AssetSelect error={Boolean(false)}>
              <Amount
                value={getFormattedPay()}
                inputMode="decimal"
                onKeyPress={e =>
                  handleKeyPress(e, {
                    decimals: assets[payAsset!.pair]!.decimals!,
                  })
                }
                onChange={e => {
                  updateBase('pay');
                  updatePay(e.target.value.replace(',', '.'));
                }}
                onFocus={e => {
                  e.target.select();
                  if (!Number(payString) && payString.length) {
                    updatePay('');
                  }
                }}
                disabled={false}
                placeholder="0.0000"
              />
              <Max />
              <Asset type="pay" />
            </AssetSelect>
          </ExchangeBox>
          <TwoIconsButton onClick={flipValues}>
            <Icon icon="IoIosArrowRoundUp" />
            <Icon icon="IoIosArrowRoundDown" />
          </TwoIconsButton>
          <ExchangeBox error={false}>
            <Title>For</Title>
            <AssetSelect error={Boolean(false)}>
              <Amount
                inputMode="none"
                onKeyPress={e =>
                  handleKeyPress(e, {
                    decimals: assets[receiveAsset!.pair]!.decimals!,
                  })
                }
                value={getFormattedReceive()}
                onChange={e => {
                  updateBase('receive');
                  updateReceive(e.target.value.replace(',', '.'));
                }}
                onFocus={_ => {
                  if (!Number(receiveString) && receiveString.length) {
                    updateReceive('');
                  }
                }}
                placeholder="0"
              />
              <Asset type="receive" />
            </AssetSelect>
            {requiredAllowance ? (
              <>
                <WarningText>
                  Please note that the following transaction has network fees
                  associated with it
                </WarningText>
              </>
            ) : null}
          </ExchangeBox>

          <Footer>
            {processingApproveTx ? (
              <LoadingSection>
                <LoadingIndicator />
              </LoadingSection>
            ) : requiredAllowance ? (
              <SwapButton
                disabled={approveText !== 'Approve'}
                type="success"
                onClick={() => {
                  setApproveText('Approving');
                  setProcessingApproveTx(true);

                  dispatch({
                    type: 'CALL_APPROVE',
                    payload: {
                      outputToken: receiveSymbol,
                      inputToken: paySymbol,
                    } as ContractParams,
                  });
                }}
                size="l"
              >
                {approveText}
              </SwapButton>
            ) : invalidPair ? (
              <SwapButton
                disabled
                type="danger"
                onClick={() => setConfirm(true)}
                size="l"
              >
                Invalid Pair
              </SwapButton>
            ) : (
              <SwapButton
                disabled={FPN.toWei(pay === '' ? '0' : pay).lte(new FPN(0))}
                type="success"
                onClick={() => setConfirm(true)}
                size="l"
              >
                Swap
              </SwapButton>
            )}
          </Footer>
        </Form>
      </ContentContainer>
      <CustomModal
        isOpened={!!chooseAsset}
        onClose={handleClose}
        duration={0.2}
        overlayContainerStyle={{
          width: '100%',
          justifyContent: 'end',
        }}
        overlayStyle={{
          height: '510px',
          width: '100%',
        }}
        animation="slideBottom"
      >
        <ModalRoot>
          <CustomCard
            title="Choose asset"
            titleBackground={false}
            onBack={() => dispatch(setChooseAsset(null))}
            pointer={false}
          >
            <ChooseAsset />
          </CustomCard>
        </ModalRoot>
      </CustomModal>
      <CustomModal
        isOpened={showConfirm}
        onClose={handleClose}
        duration={0.2}
        overlayContainerStyle={{
          width: '100%',
          justifyContent: 'end',
        }}
        overlayStyle={{
          height: '510px',
          width: '100%',
        }}
        animation="slideBottom"
      >
        <ModalRoot>
          <CustomCard
            title={showSuccess ? 'Success' : 'Please Confirm your Swap'}
            titleBackground={false}
            onBack={() => {
              setConfirm(false);
              setInProgress(false);
              setShowSuccess(false);
            }}
            pointer={false}
            leftEmoji={showSuccess ? 'CheckMark' : 'FountainPen'}
          >
            {showSuccess ? (
              <div>
                <SuccessContainer>
                  <SuccessEmoji emoji="Money" />
                </SuccessContainer>
                <div>Your Transaction had been submitted </div>
              </div>
            ) : (
              <ExchangeBoxConfirm error={false}>
                <ConfirmTitle>You swap </ConfirmTitle>
                <ConfirmItem>
                  <Flag flag={payAsset!.assetOut.name!} />
                  <div className="assetAmount">
                    {FPN.toWei(
                      getFormattedPay() !== '' ? getFormattedPay() : '0',
                    ).format(8)}
                  </div>
                  <div className="assetName">{payAsset!.assetOut.name!}</div>
                </ConfirmItem>
                <ConfirmTitle>With </ConfirmTitle>
                <ConfirmItem>
                  <Flag flag={receiveAsset!.assetOut.name!} />
                  <div className="assetAmount">
                    {' '}
                    {FPN.toWei(
                      getFormattedReceive() !== ''
                        ? getFormattedReceive()
                        : '0',
                    ).format(8)}
                  </div>
                  <div className="assetName">
                    {receiveAsset!.assetOut.name!}
                  </div>
                </ConfirmItem>
              </ExchangeBoxConfirm>
            )}

            {!inProgress ? (
              <SwapButton
                disabled={false}
                type="success"
                onClick={handleSwap}
                size="l"
              >
                Confirm
              </SwapButton>
            ) : showSuccess ? (
              <SwapButton
                disabled={false}
                type="success"
                onClick={() => tabHandler(0)}
                size="l"
              >
                Done
              </SwapButton>
            ) : (
              <LoadingSection>
                <LoadingIndicator />
              </LoadingSection>
            )}
          </CustomCard>
        </ModalRoot>
      </CustomModal>
    </div>
  );
};
