import { Asset } from '@/data/assets';
import { initialAppState, State } from '@/state/initialState';
import { SupportedNetworkName } from '@jarvis-network/synthereum-config/dist';
import {
  SupportedSynthereumPairExact,
  SupportedSynthereumSymbol,
} from '@jarvis-network/synthereum-config/dist/supported/all-synthereum-pairs';
import { createSlice } from '@reduxjs/toolkit';

export interface Action<T> {
  payload: T;
}

export type BurnerWalletAssets<
  Net extends SupportedNetworkName = SupportedNetworkName
> = {
  [Pair in
    | SupportedSynthereumPairExact<Net>
    | 'USDC'
    | 'BUSD']?: Asset<SupportedSynthereumSymbol>;
};

const assetsSlice = createSlice({
  name: 'assets',
  initialState: initialAppState.assets,
  reducers: {
    setAssetsList(state, { payload: list }: Action<State['assets']['list']>) {
      if (list === null || list === undefined || list === {}) {
        return;
      }

      const newMarketLists = list;

      for (const marketKey of Object.keys(newMarketLists)) {
        const newMarket =
          newMarketLists[marketKey as keyof typeof newMarketLists];

        if (marketKey in state.list) {
          // Add new Market to the state:
          state.list[marketKey as keyof typeof state.list] = newMarket as any;
          continue;
        }

        const existingMarket = state.list[
          marketKey as keyof typeof state.list
        ]!;
        if (existingMarket) {
          for (const key of Object.keys(existingMarket)) {
            (existingMarket as any)[key] = newMarket![
              key as keyof typeof newMarket
            ];
          }
        }
      }
    },
  },
});

export const { reducer } = assetsSlice;
export const { setAssetsList } = assetsSlice.actions;
