import { initialAppState, State } from '@/state/initialState';
import { createSlice } from '@reduxjs/toolkit';

import { resetSwapAction } from '../actions';

import { Action } from './assets';

interface SetModalVisibilityAction {
  payload: boolean;
}

interface SetMobileTabAction {
  payload: number;
}

interface SetPrivateKeyAction {
  payload: string;
}

const appSlice = createSlice({
  name: 'app',
  initialState: initialAppState.app,
  reducers: {
    setWindowLoaded(state, action: SetModalVisibilityAction) {
      return {
        ...state,
        isWindowLoaded: action.payload,
      };
    },
    setPrivatekey(state, action: SetPrivateKeyAction) {
      return {
        ...state,
        privateKey: action.payload,
      };
    },
    setUnsupportedNetworkModalVisible(state, action: SetModalVisibilityAction) {
      return {
        ...state,
        isUnsupportedNetworkModalVisible: action.payload,
      };
    },
    setMobileTab(state, action: SetMobileTabAction) {
      return {
        ...state,
        mobileTab: action.payload,
      };
    },
    contextUpdate(state, action: Action<State['app']>) {
      return {
        ...state,
        address: action.payload.address,
        networkId: action.payload.networkId,
      };
    },
  },
  extraReducers: {
    [resetSwapAction.type](state) {
      return {
        ...state,
        isSwapLoaderVisible: false,
        isExchangeConfirmationVisible: false,
      };
    },
  },
});

export const {
  setWindowLoaded,
  setUnsupportedNetworkModalVisible,
  setMobileTab,
  setPrivatekey,
} = appSlice.actions;
export const { reducer } = appSlice;
