import { Transaction } from '@/data/transactions';
import { addressSwitch, logoutAction, networkSwitch } from '@/state/actions';
import { initialAppState } from '@/state/initialState';
import { createSlice } from '@reduxjs/toolkit';
import _ from 'lodash';

interface Action<T> {
  payload: T;
}

const initialState = initialAppState.transactions;
const isArrayEqual = function (x: Transaction[], y: Transaction[]) {
  return _(x).xorWith(y, _.isEqual).isEmpty();
};
const transactionsSlice = createSlice({
  name: 'transactions',
  initialState,
  reducers: {
    setHistory: (state, { payload: transaction }: Action<any>) => {
      if (!isArrayEqual(state.list, transaction)) {
        state.list = transaction;
      }
    },
    setPayload: (state, { payload: transaction }: Action<any>) => {
      state.payload = transaction;
    },
    approveTx: (state, { payload: transaction }: Action<any>) => {
      state.approveTx = transaction;
    },
    loaded: (state, { payload: status }: Action<any>) => {
      state.loaded = status;
    },
  },
  extraReducers: {
    [addressSwitch.type]() {
      return initialState;
    },
    [logoutAction.type]() {
      return initialState;
    },
    [networkSwitch.type]() {
      return initialState;
    },
  },
});

export const { reducer } = transactionsSlice;
