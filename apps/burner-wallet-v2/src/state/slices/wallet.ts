import { addressSwitch, logoutAction, networkSwitch } from '@/state/actions';
import { initialAppState } from '@/state/initialState';
import { StringAmount } from '@jarvis-network/core-utils/dist/base/big-number';
import { ExchangeSynthereumToken } from '@jarvis-network/synthereum-config/src/supported/all-synthereum-pairs';
import { createSlice } from '@reduxjs/toolkit';

interface Action<T> {
  payload: T;
}

export interface WalletBalance {
  asset: ExchangeSynthereumToken;
  amount: StringAmount;
  nonce: number;
  decimals: number;
  name: string;
}

const initialState = initialAppState.wallet;

const walletSlice = createSlice({
  name: 'wallet',
  initialState,
  reducers: {
    setWalletBalances(state, { payload: list }: Action<WalletBalance[]>) {
      list.forEach(({ asset, amount, nonce, name, decimals }) => {
        state[asset] = { amount, nonce, name, decimals };
      });
    },
  },
  extraReducers: {
    [logoutAction.type]() {
      return initialState;
    },
    [addressSwitch.type]() {
      return initialState;
    },
    [networkSwitch.type]() {
      return initialState;
    },
  },
});

export const { reducer } = walletSlice;
