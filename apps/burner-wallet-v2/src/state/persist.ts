import { RootState } from '@/state/reducer';
import { cache } from '@jarvis-network/app-toolkit';
import { Middleware } from '@reduxjs/toolkit';

let lastState: RootState | null = null;

const get = <T>(
  source: Record<string, unknown> | null,
  path: string[],
): T | null => {
  if (!path.length) {
    return source as T;
  }
  if (source == null) {
    return source;
  }

  const parts = [...path];
  const current = parts.shift();
  return get(source[current!] as Record<string, unknown>, parts) as T;
};

export const createPersistMiddleware = (pathsToStore: string[]) => {
  const persistMiddleware: Middleware = store => next => action => {
    const result = next(action);

    if (typeof window !== 'undefined') {
      const appState: RootState = store.getState();

      if (lastState) {
        pathsToStore.forEach(path => {
          const current = get(appState as any, path.split('.'));
          // const previous = get(lastState, path.split('.'));
          // console.log({ pathsToStore, previous, current });
          // if (current !== previous) {
          cache.set(`jarvis/state/${path}`, current);
          // }
        });
      }

      lastState = appState;
    }

    return result;
  };

  return persistMiddleware;
};
