import { reducer as app } from '@/state/slices/app';
import { reducer as assets } from '@/state/slices/assets';
import { reducer as exchange } from '@/state/slices/exchange';
import { reducer as theme } from '@/state/slices/theme';
import { reducer as transactions } from '@/state/slices/transactions';
import { reducer as wallet } from '@/state/slices/wallet';
import { combineReducers } from '@reduxjs/toolkit';

export const reducer = combineReducers({
  theme,
  app,
  assets,
  exchange,
  wallet,
  transactions,
});

export type RootState = ReturnType<typeof reducer>;
