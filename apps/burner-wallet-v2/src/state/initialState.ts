import { burnerWalletAssets } from '@/data/assets';
import { Transaction } from '@/data/transactions';
import { cache } from '@jarvis-network/app-toolkit';
import { StringAmount } from '@jarvis-network/core-utils/dist/base/big-number';
import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';
import { AddressOn } from '@jarvis-network/core-utils/dist/eth/address';
import { ExchangeSynthereumToken } from '@jarvis-network/synthereum-config/dist/supported/all-synthereum-pairs';
import { ThemeNameType } from '@jarvis-network/ui';

import { BurnerWalletAssets } from './slices/assets';

type Values = 'pay' | 'receive';

export interface WalletInfo {
  amount: StringAmount;
  nonce: number;
  decimals: number;
  name: string;
}

export interface Rate {
  rate: FPN;
}

export interface WhitespacePricePoint {
  time: string;
  history?: boolean;
}

export interface PricePoint {
  time: string; // in format YYYY-MM-DD
  open: number;
  high: number;
  low: number;
  close: number;
  history: boolean;
}

export type DataItem = PricePoint | WhitespacePricePoint;

export interface PricePointsMap {
  [pair: string]: PricePoint[];
}

export const DEFAULT_PAY_ASSET: ExchangeSynthereumToken = 'BUSD';
export const DEFAULT_RECEIVE_ASSET: ExchangeSynthereumToken = 'jEUR';

export type Days = 1 | 7 | 30;

export interface State {
  theme: ThemeNameType;
  app: {
    isExchangeLoaded: boolean;
    isWindowLoaded: boolean;
    isUnsupportedNetworkModalVisible: boolean;
    privateKey: string;
    mobileTab: number;
    networkId: number;
    address: AddressOn<'polygon'> | undefined;
  };
  assets: {
    list: BurnerWalletAssets;
  };
  exchange: {
    // pay/receive are stored as string to allow incomplete input fills while
    // typing ie. "1." (mind the dot) - forcing a number instantly will cause
    // dot to be removed as typed
    // these values should be casted/converted when needed
    pay: string;
    receive: string;
    base: Values;
    payAsset: ExchangeSynthereumToken | null;
    receiveAsset: ExchangeSynthereumToken | null;
    invertRateInfo: boolean;
    chooseAssetActive: Values | null;
    chartDays: Days;
  };
  wallet: {
    [key in ExchangeSynthereumToken]?: WalletInfo;
  };

  transactions: {
    list: Transaction[];
    loaded: boolean;
    payload: any;
    approveTx?: {
      blockHash: string;
      blockNumber: number;
      transactionHash: string;
    };
  };
}

export const initialAppState: State = {
  theme: cache.get<ThemeNameType | null>('jarvis/state/theme') || 'light',
  app: {
    isWindowLoaded: false,
    privateKey: cache.get<string | null>('jarvis/state/app.privateKey') || '',
    isExchangeLoaded: false, // will become false on user sign in
    isUnsupportedNetworkModalVisible: false,
    mobileTab: 1,
    networkId: 0,
    address: '0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee' as AddressOn<'polygon'>,
  },

  assets: {
    list: burnerWalletAssets,
  },
  wallet: {},
  transactions: {
    list: [],
    loaded: false,
    payload: {},
  },
  exchange: {
    pay: '0',
    receive: '0',
    base: 'pay',
    payAsset:
      cache.get<ExchangeSynthereumToken | null>(
        'jarvis/state/exchange.payAsset',
      ) || DEFAULT_PAY_ASSET,
    receiveAsset:
      cache.get<ExchangeSynthereumToken | null>(
        'jarvis/state/exchange.receiveAsset',
      ) || DEFAULT_RECEIVE_ASSET,
    invertRateInfo: false,
    chooseAssetActive: null,
    chartDays: cache.get<Days | null>('jarvis/state/exchange.chartDays') || 7,
  },
};

export type AssetType = Values;
