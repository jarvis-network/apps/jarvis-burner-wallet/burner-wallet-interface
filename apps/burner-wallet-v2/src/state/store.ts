import { initialAppState, State } from '@/state/initialState';
import { createPersistMiddleware } from '@/state/persist';
import { reducer } from '@/state/reducer';
import { dependencies } from '@jarvis-network/app-toolkit/dist/core-context';
import {
  createBurnerRealmAgentEpic,
  loadTransactionEpic,
} from '@jarvis-network/synthereum-ts/dist/core/realms/burner-wallet/epic';
import { assetsEpic } from '@jarvis-network/synthereum-ts/dist/epics/assets-burner';
import { realmBurnerEpic } from '@jarvis-network/synthereum-ts/dist/epics/core-burner';
import { priceFeedEpic } from '@jarvis-network/synthereum-ts/dist/epics/price-feed';
import { ReduxAction } from '@jarvis-network/synthereum-ts/dist/epics/types';
import { walletBurnerEpic } from '@jarvis-network/synthereum-ts/dist/epics/wallet-burner';
import { configureStore, getDefaultMiddleware, Store } from '@reduxjs/toolkit';
import { useMemo } from 'react';
import { combineEpics, createEpicMiddleware } from 'redux-observable';

let cachedStore: Store | undefined;
const epicMiddleware = createEpicMiddleware<ReduxAction, ReduxAction, State>({
  dependencies,
});
function initStore(preloadedState: State = initialAppState) {
  const middleware = [
    ...getDefaultMiddleware(),
    epicMiddleware,
    createPersistMiddleware([
      'theme',
      'exchange.payAsset',
      'exchange.receiveAsset',
      'app.privateKey',
    ]),
  ];

  // If you are going to load preloaded state from serialized data somewhere
  // here, make sure to convert all needed values from strings to BN
  const store = configureStore({ reducer, preloadedState, middleware });

  return store;
}

export const initializeStore = (
  preloadedState: State,
): ReturnType<typeof initStore> => {
  let store = cachedStore ?? initStore(preloadedState);
  const rootEpic = combineEpics(
    realmBurnerEpic,
    priceFeedEpic,
    walletBurnerEpic,
    assetsEpic,
    createBurnerRealmAgentEpic('CALL_MINT'),
    createBurnerRealmAgentEpic('CALL_APPROVE'),
    createBurnerRealmAgentEpic('CALL_EXCHANGE'),
    createBurnerRealmAgentEpic('CALL_SEND'),
    loadTransactionEpic(),
  );

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && cachedStore) {
    store = initStore({
      ...cachedStore.getState(),
      ...preloadedState,
    });
    // Reset the current store
    cachedStore = undefined;
  }

  // For SSG and SSR always create a new store
  if (typeof window === 'undefined') return store;

  // Create the store once in the client
  if (!cachedStore) cachedStore = store;
  epicMiddleware.run(rootEpic);
  return store;
};

export function useStore(state: State): ReturnType<typeof initializeStore> {
  return useMemo(() => initializeStore(state), [state]);
}
