import { useBehaviorSubject } from '@jarvis-network/app-toolkit/dist/useBehaviorSubject';
import { Address } from '@jarvis-network/core-utils/dist/eth/address';

import { useEffect, useState } from 'react';

import { useCoreObservables } from '../context/CoreObservablesContext';

const noop = () => undefined;

export const usePrettyName = (address: Address | null) => {
  const [name, setName] = useState<string | null>(null);
  const ens = useBehaviorSubject(useCoreObservables().ens$);

  useEffect(() => {
    if (!ens || !address) return;

    let cancelled = false;

    setName(null);
    ens
      .prettyName(address)
      .then(resolvedName => {
        if (!cancelled) {
          setName(resolvedName);
        }
      })
      .catch(noop);

    return () => {
      cancelled = true;
      setName(null);
    };
  }, [ens, address]);

  return name;
};
