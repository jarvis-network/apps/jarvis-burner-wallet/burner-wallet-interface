import React, { createContext, useContext } from 'react';

import { BehaviorSubject } from 'rxjs';
import type Web3 from 'web3';
import { ENSHelper } from '@jarvis-network/app-toolkit/dist/ens';

import { useConstant } from '@jarvis-network/app-toolkit/dist//useConstant';

type Context = {
  web3$: BehaviorSubject<Web3 | null>;
  ens$: BehaviorSubject<ENSHelper | null>;

  networkId$: BehaviorSubject<number>;
};

const CoreObservablesContext = createContext<Context | null>(null);

export const CoreObservablesContextProvider: React.FC<{ value: Context }> = ({
  value,
  children,
}) => (
  <CoreObservablesContext.Provider value={value}>
    {children}
  </CoreObservablesContext.Provider>
);

export function useCoreObservables(): Context {
  const value = useContext(CoreObservablesContext);
  if (!value) throw new Error('CoreObservablesContext not provided');
  return value;
}

export function useSubjects(): Context {
  return useConstant(() => ({
    web3$: new BehaviorSubject<Web3 | null>(null),
    ens$: new BehaviorSubject<ENSHelper | null>(null),
    networkId$: new BehaviorSubject<number>(0),
  }));
}
