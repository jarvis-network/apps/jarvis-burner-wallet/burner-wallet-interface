export interface IEIP712Domain {
  name: string;
  version: string;
  salt: string;
  verifyingContract: string;
}
export interface MetaTransactionMessage extends IEIP712Domain {
  nonce: number;
  from: string;
  functionSignature: string;
}
export const getMetaTransactionTypedData = ({
  name,
  version,
  salt,
  verifyingContract,
  nonce,
  from,
  functionSignature,
}: MetaTransactionMessage) => ({
  types: {
    EIP712Domain: [
      {
        name: 'name',
        type: 'string',
      },
      {
        name: 'version',
        type: 'string',
      },
      {
        name: 'verifyingContract',
        type: 'address',
      },
      {
        name: 'salt',
        type: 'bytes32',
      },
    ],
    MetaTransaction: [
      { name: 'nonce', type: 'uint256' },
      { name: 'from', type: 'address' },
      { name: 'functionSignature', type: 'bytes' },
    ],
  },
  primaryType: 'MetaTransaction',
  domain: {
    name,
    version,
    salt,
    verifyingContract,
  },
  message: {
    from,
    functionSignature,
    nonce,
  },
});
