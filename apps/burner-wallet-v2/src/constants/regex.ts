const Regex = {
  address: '^(?:0x)?[0-9a-f]{40}$',
  ens:
    '[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&/=]*)?',
  pk: '^(?:https?:\\/\\/[-a-z.]+\\/pk#)?((?:0x)?[0-9a-f]{64})$',
} as const;

export default Regex;
export type RegexKeys = keyof typeof Regex;
