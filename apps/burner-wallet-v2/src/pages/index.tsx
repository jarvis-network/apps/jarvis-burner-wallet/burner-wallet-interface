import React, { useEffect, useState } from 'react';
import { StickyHeader } from '@/components/header/StickyHeader';
import { OnMobile, styled } from '@jarvis-network/ui';
import { WelcomePage } from '@/components/welcome';
import { HomePage } from '@/components/home';
import { ScannerPage } from '@/components/scanner';
import { useDispatch } from 'react-redux';
import { setWindowLoaded } from '@/state/slices/app';
import { useReduxSelector } from '@/state/useReduxSelector';

export const Mobile = styled(OnMobile)`
  width: 100%;
`;

export const Rotate = styled.div`
  display: none;

  @media screen and (max-device-width: 1080px) and (orientation: landscape) {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 9;
    background: ${props => props.theme.background.secondary};
    color: ${props => props.theme.text.secondary};
    font-size: ${props => props.theme.font.sizes.l};
    display: flex;
    justify-content: center;
    align-items: center;
  }

  > span {
    font-size: ${props => props.theme.font.sizes.xxl};
    padding-right: 5px;
  }
`;

export function Index() {
  const dispatch = useDispatch();

  const [privateKey, setPrivatekey] = useState('');
  const [showScanner, setShowScanner] = useState(false);
  const [showHomePage, setShowHomePage] = useState(false);
  const privateKeyState = useReduxSelector(state => state.app.privateKey);
  useEffect(() => {
    if (privateKey && privateKey.length > 10) {
      dispatch({ type: 'app/setPrivatekey', payload: privateKey });
      dispatch({
        type: 'INIT_CORE_CONTEXT',
        payload: {
          privateKey,
          priceFeedNode: 'https://bsc-dataseed2.binance.org/',
          appNode: 'https://bsc-dataseed2.binance.org/',
        },
      });
      dispatch({ type: 'GET_MARKET_LIST' });

      dispatch({
        type: 'GET_WALLET_BALANCE',
      });

      setShowHomePage(true);
    }
    if (privateKeyState && privateKeyState.length > 10) {
      dispatch({
        type: 'INIT_CORE_CONTEXT',
        payload: {
          privateKey: privateKeyState,
          priceFeedNode: 'https://bsc-dataseed2.binance.org/',
          appNode: 'https://bsc-dataseed2.binance.org/',
        },
      });
      dispatch({ type: 'GET_MARKET_LIST' });

      dispatch({
        type: 'GET_WALLET_BALANCE',
      });

      setShowHomePage(true);
    }
  }, [privateKey, privateKeyState]);
  useEffect(() => {
    function handleLoad() {
      setTimeout(() => dispatch(setWindowLoaded(true)), 250);
      window.removeEventListener('load', handleLoad);
    }

    window.addEventListener('load', handleLoad);
  }, []);
  return (
    <Mobile>
      {showHomePage ? (
        <StickyHeader>
          <HomePage />
        </StickyHeader>
      ) : showScanner ? (
        <ScannerPage
          onClose={() => {
            setShowScanner(false);
          }}
          onScan={(data: string) => {
            if (data && data.toString().indexOf('0x') === -1) {
              setPrivatekey(`0x${data}`);
            } else if (data && data.toString()) {
              setPrivatekey(data);
            }
          }}
        />
      ) : (
        <StickyHeader>
          <WelcomePage scanWalletHandler={() => setShowScanner(true)} />
        </StickyHeader>
      )}
      <Rotate>
        <span>↺</span> Rotate your device to portrait mode
      </Rotate>
    </Mobile>
  );
}

export default Index;
