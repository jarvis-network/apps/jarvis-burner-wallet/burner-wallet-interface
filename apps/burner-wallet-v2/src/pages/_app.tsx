import React from 'react';
import { AppProps } from 'next/app';
import Head from 'next/head';
import {
  BackgroundPreloader,
  NotificationsProvider,
  styled,
  useIsMounted,
} from '@jarvis-network/ui';
import { useStore } from '@/state/store';
import { AppThemeProvider } from '@/components/AppThemeProvider';
import { Provider as StateProvider } from 'react-redux';
import './_app.scss';
import { backgroundList } from '@/data/backgrounds';

const MainWrapper = styled.div`
  height: 100%;
  width: 100vw;
  background: ${props => props.theme.background.primary};
  color: ${props => props.theme.text.primary};
`;
function CustomApp({ Component, pageProps }: AppProps) {
  const store = useStore(pageProps.initialReduxState);

  const isMounted = useIsMounted();
  if (!isMounted) return null;

  return (
    <>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0, viewport-fit=cover"
        />
      </Head>
      <StateProvider store={store}>
        <AppThemeProvider>
          <NotificationsProvider>
            <BackgroundPreloader backgrounds={backgroundList} />
            <MainWrapper>
              <Component {...pageProps} />
            </MainWrapper>
          </NotificationsProvider>
        </AppThemeProvider>
      </StateProvider>
    </>
  );
}

export default CustomApp;
